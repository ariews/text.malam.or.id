---
title: "About"
date: "2005-07-10T06:34:00+07:00"
---

![image0](/images/50567f6af0a1450567f8b07c7719436_244022453655_1279147_n.jpg)

## Arie Windu Subagja?

-   Ya ini nama yang di kasi sama ortu gw
-   anak kedua dari 3 bersaudara (kk=Indra && adek=fitrie(rani)).
-   gw berbintang scorpio.
-   gw lulusan SD CIPANAS III Sukabumi,
-   SMPN 2 Sukabumi,
-   SMUNSA Sukabumi

## Links

-   [Facebook](https://www.facebook.com/ariews)
-   [Github](https://github.com/ariews)
-   [Gitlab](https://gitlab.com/ariews)
-   [BitBucket](https://bitbucket.org/ariews)
-   [Last.fm](http://www.last.fm/user/ariews)

## Hobby?

-   Denger musik
-   Ngomik

## Family

-   [Vera Lindasari](https://www.facebook.com/bunda.mizani) :\* (Wife)
-   Arya M. Subagja (Son)
-   Yaqzan B. Subagja (Son)
-	Aisyah Shanum Almasyira Subagja (Daughter)

## OS ?

{{< image src="/images/arch-2024-05-12_09-11.png" alt="Arch Linux on Intel NUC NUC11TNKi5" >}}

-   <strike>PC (Ubuntu 20.04)</strike>, Intel NUC NUC11TNKi5 <strike>Debian 12 Bookworm</strike> Arch Linux
-   <strike>Odroid XU4 (Arch Linux)</strike>
-	Laptop (MacBook Pro (Retina, 15-inch, Late 2013))
-   Server (<strike>Slackware</strike>, <strike>RedHat</strike>, Ubuntu, Debian, Arch Linux)
-   Windows (rarely)

## Bahasa

-   [Go](https://golang.org/)
-   [PHP](http://www.php.net)

## Kerja? Ya! gw kerja

Saat ini gwa kerja sebagai Backend Web Programmer. Untuk pengalaman kerja, bisa dilihat di [CV](https://docs.google.com/document/d/e/2PACX-1vRn_zCRMoqadKdzlK8fuWatA5RVsZ3Iz6tqUgeKPGJrEtJohNMyyyXK9a3_l_4P4QvxbMiEOy8wfo4P/pub).

## Lainnya?

-   [Google](http://goo.gl/dVcPzL)
