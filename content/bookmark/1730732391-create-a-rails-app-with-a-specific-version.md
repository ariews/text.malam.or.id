---
title: Create a Rails App With a Specific Version
date: 2024-11-04T21:59:51+07:00
source: http://web.archive.org/web/20231123064045/http://craiccomputing.blogspot.com/2008/06/using-older-versions-of-rails.html
tags:
  - Bookmark
---
```text
$ rails _5.2.3_ new appname
```
