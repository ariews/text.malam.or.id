---
title: Mpv Keyboard Shortcuts
date: 2024-04-24T13:19:56+07:00
source: https://www.maketecheasier.com/cheatsheet/mpv-keyboard-shortcuts/
tags:
  - Bookmark
---
<table>
    <thead>
        <tr>
            <th><strong>Shortcut</strong></th>
            <th><strong>Function</strong></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2"><strong>Timeline Seeking</strong></td>
        </tr>
        <tr>
            <td>Left Arrow</td>
            <td>Go back 5 seconds in the media.</td>
        </tr>
        <tr>
            <td>Right Arrow</td>
            <td>Go forward 5 seconds in the media.</td>
        </tr>
        <tr>
            <td>Down Arrow</td>
            <td>Go back 1 minute in the media.</td>
        </tr>
        <tr>
            <td>Up Arrow</td>
            <td>Go forward 1 minute in the media.</td>
        </tr>
        <tr>
            <td>Shift + Page Up</td>
            <td>Go forward 10 minutes in the media.</td>
        </tr>
        <tr>
            <td>Shift + Page Down</td>
            <td>Go back 10 minutes in the media.</td>
        </tr>
        <tr>
            <td>Page Up</td>
            <td>Go to the beginning of the next chapter.</td>
        </tr>
        <tr>
            <td>Page Down</td>
            <td>Go to the beginning of the previous chapter.</td>
        </tr>
        <tr>
            <td>Ctrl + Left Arrow</td>
            <td>Go to the previous frame with subtitles.</td>
        </tr>
        <tr>
            <td>Ctrl + Right Arrow</td>
            <td>Go to the next frame with subtitles.</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Subtitle Manipulation</strong></td>
        </tr>
        <tr>
            <td>V</td>
            <td>Either display or hide subtitles for the currently playing media.</td>
        </tr>
        <tr>
            <td>Shift + J</td>
            <td>Select the previous subtitle file available.</td>
        </tr>
        <tr>
            <td>J</td>
            <td>Select the next subtitle file available.</td>
        </tr>
        <tr>
            <td>Ctrl + Shift + Left</td>
            <td>Synchronize the subtitle with the previous frame.</td>
        </tr>
        <tr>
            <td>Ctrl + Shift + Right</td>
            <td>Synchronize the subtitle with the next frame.</td>
        </tr>
        <tr>
            <td>Z</td>
            <td>Increase the subtitle delay by 100 milliseconds.</td>
        </tr>
        <tr>
            <td>Shift + Z</td>
            <td>Decrease the subtitle delay by 100 milliseconds.</td>
        </tr>
        <tr>
            <td>Shift + G</td>
            <td>Increase the subtitle font by 10%.</td>
        </tr>
        <tr>
            <td>Shift + F</td>
            <td>Decrease the subtitle font by 10%.</td>
        </tr>
        <tr>
            <td>R</td>
            <td>Shift the subtitle placement in the video upwards.</td>
        </tr>
        <tr>
            <td>Shift + R</td>
            <td>Shift the subtitle placement in the video downwards.</td>
        </tr>
        <tr>
            <td>U</td>
            <td>Switch the currently active subtitle to a compatible SubStation Alpha format.</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Playback Controls</strong></td>
        </tr>
        <tr>
            <td>[</td>
            <td>Slow the media playback down by 10%.</td>
        </tr>
        <tr>
            <td>]</td>
            <td>Speed the media playback up by 10%.</td>
        </tr>
        <tr>
            <td>Shift + [</td>
            <td>Drop the playback speed by 50%.</td>
        </tr>
        <tr>
            <td>Shift + ]</td>
            <td>Increase the playback speed by 50%.</td>
        </tr>
        <tr>
            <td>L</td>
            <td>Mark the current position as a playback loop.</td>
        </tr>
        <tr>
            <td>Shift + L</td>
            <td>Create an infinite loop between two marked points.</td>
        </tr>
        <tr>
            <td>Backspace</td>
            <td>Return the media playback speed back to 100%.</td>
        </tr>
        <tr>
            <td>/ or 9</td>
            <td>Decrease the media volume by 1 point.</td>
        </tr>
        <tr>
            <td>* or 0</td>
            <td>Increase the media volume by 1 point.</td>
        </tr>
        <tr>
            <td>M</td>
            <td>Mute the sound of the currently playing media.</td>
        </tr>
        <tr>
            <td>Shift + `</td>
            <td>Go through the available video tracks in the media file.</td>
        </tr>
        <tr>
            <td>Shift + 3</td>
            <td>Go through the available audio tracks in the media file.</td>
        </tr>
        <tr>
            <td>S</td>
            <td>Take the current video frame and save it as a screenshot.</td>
        </tr>
        <tr>
            <td>Shift + S</td>
            <td>Take a screenshot of the current frame and remove all subtitles.</td>
        </tr>
        <tr>
            <td>Ctrl + S</td>
            <td>Take a screenshot of the current player window.</td>
        </tr>
        <tr>
            <td>Ctrl +</td>
            <td>Increase the audio delay by 100 milliseconds.</td>
        </tr>
        <tr>
            <td>Ctrl –</td>
            <td>Decrease the audio delay by 100 milliseconds.</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Picture Controls</strong></td>
        </tr>
        <tr>
            <td>Shift + A</td>
            <td>Force a different aspect ratio for the currently playing media.</td>
        </tr>
        <tr>
            <td>D</td>
            <td>Toggle the realtime video deinterlacer.</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Decrease the contrast of the video.</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Increase the contrast of the video.</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Decrease the brightness of the video.</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Increase the brightness of the video.</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Decrease the gamma of the video.</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Increase the gamma of the video.</td>
        </tr>
        <tr>
            <td>7</td>
            <td>Decrease the saturation of the video.</td>
        </tr>
        <tr>
            <td>8</td>
            <td>Increase the saturation of the video.</td>
        </tr>
        <tr>
            <td>Ctrl + H</td>
            <td>Enable Hardware Decoding Support.</td>
        </tr>
        <tr>
            <td>Shift + W</td>
            <td>Increase the picture cropping for the currently playing media.</td>
        </tr>
        <tr>
            <td>W</td>
            <td>Decrease the picture cropping for the currently playing media.</td>
        </tr>
        <tr>
            <td>Alt + Up Arrow</td>
            <td>Move the picture in the window upwards.</td>
        </tr>
        <tr>
            <td>Alt + Down Arrow</td>
            <td>Move the picture in the window downwards.</td>
        </tr>
        <tr>
            <td>Alt + Left Arrow</td>
            <td>Move the picture in the window leftwards.</td>
        </tr>
        <tr>
            <td>Alt + Right Arrow</td>
            <td>Move the picture in the window rightwards.</td>
        </tr>
        <tr>
            <td>Alt +</td>
            <td>Zoom into the currently playing media.</td>
        </tr>
        <tr>
            <td>Alt –</td>
            <td>Zoom out of the currently playing media.</td>
        </tr>
        <tr>
            <td>Alt + Backspace</td>
            <td>Restore the original position and zoom of the picture.</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Player Controls</strong></td>
        </tr>
        <tr>
            <td>&gt; or Enter</td>
            <td>Play the next file in a playlist.</td>
        </tr>
        <tr>
            <td>&lt;</td>
            <td>Play the previous file in a playlist.</td>
        </tr>
        <tr>
            <td>P or Space</td>
            <td>Toggle the player’s pause function.</td>
        </tr>
        <tr>
            <td>.</td>
            <td>Pause the file and display the next frame.</td>
        </tr>
        <tr>
            <td>,</td>
            <td>Pause the file and display the previous frame.</td>
        </tr>
        <tr>
            <td>F</td>
            <td>Turn the player into a fullscreen window.</td>
        </tr>
        <tr>
            <td>Esc</td>
            <td>Turn the player back to its original size.</td>
        </tr>
        <tr>
            <td>T</td>
            <td>Force the player to stay on top of other windows.</td>
        </tr>
        <tr>
            <td>O or Shift + P</td>
            <td>Display the current progress bar.</td>
        </tr>
        <tr>
            <td>I or Shift + I</td>
            <td>Print the technical details of the currently playing file in the screen.</td>
        </tr>
        <tr>
            <td>`</td>
            <td>Pause the file and display the previous frame.</td>
        </tr>
        <tr>
            <td>Q</td>
            <td>Stop the currently playing file and exit the player.</td>
        </tr>
        <tr>
            <td>Shift + Q</td>
            <td>Stop the currently playing file, save its playback position then exit the player.</td>
        </tr>
        <tr>
            <td colspan="2"><strong>Mouse-Specific Controls</strong></td>
        </tr>
        <tr>
            <td>Right Click</td>
            <td>Toggle the pause function.</td>
        </tr>
        <tr>
            <td>Double Left Click</td>
            <td>Turn the player into a fullscreen window.</td>
        </tr>
        <tr>
            <td>Scroll Up</td>
            <td>Go forward 10 seconds in the media.</td>
        </tr>
        <tr>
            <td>Scroll Down</td>
            <td>Go backwards 10 seconds in the media.</td>
        </tr>
    </tbody>
</table>
