---
title: "Set Environment Value to .desktop File"
date: 2024-05-12T08:12:44+07:00
source: https://askubuntu.com/a/144971
tags:
  - Bookmark
---
You can add an environment variable to an application by editing its 
`.desktop` file. For example, to run "digiKam" with the environment variable
`APPMENU_DISPLAY_BOTH=1`, find the corresponding `digikam.desktop` file and 
add the setting of the variable, via the env command, to the entry "Exec"
