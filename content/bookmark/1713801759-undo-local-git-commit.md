---
title: "Undo Local Git Commit"
date: 2024-04-22T23:02:39+07:00
source: https://stackoverflow.com/a/927386
tags:
  - Bookmark
---
I accidentally committed the wrong files to Git, but didn't push the commit to the server yet.

How do I undo those commits from the local repository?

```text
$ git commit -m "Something terribly misguided" # (0: Your Accident)
$ git reset HEAD~                              # (1)
[ edit files as necessary ]                    # (2)
$ git add .                                    # (3)
$ git commit -c ORIG_HEAD                      # (4)
```
