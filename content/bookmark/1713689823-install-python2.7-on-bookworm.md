---
title: "Installing Python 2.7 on Debian 12 (Bookworm)"
date: 2024-04-21T15:57:04+07:00
source: https://www.fadedbee.com/2024/01/18/installing-python2-on-debian-12-bookworm/
tags: 
    - Bookmark
---
There is no Python 2 or Python 2.7 package in Debian 12 (Bookworm).

You can successfully install it from the Debian 11 (Bullseye) packages, but it 
has several dependencies which are also not provided by Debian 12 (Bookworm).
