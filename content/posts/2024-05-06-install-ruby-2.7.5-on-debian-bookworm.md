---
title: Install Ruby 2.7.5 on Debian Bookworm
date: 2024-05-06T05:34:56+07:00
toc: false
images:
tags:
  - Ruby 2.7.5
  - Rails 6.0.4.4
  - Debian
  - Learn English
---
I am not familiar with Ruby on Rails, but my colleague has a problem installing 
it on their server. So I tried to help her to achieve that. 

Their server is freshly installed Ubuntu 22.04 Jammy, but mine is Debian 12 Bookworm.
I think there would be no problem in this case.

> Disclaimer:
> This issue has been solved in the [latest version of rbenv](#latest-version)

## Objective
1. Ruby `2.7.5`, install using `rbenv`.
2. Rails `6.0.4.4`
3. Nginx + Phusion Passenger

## Preparation
```text
$ sudo apt update
$ sudo apt install wget curl git
$ sudo apt install rbenv
$ echo '`eval "$(rbenv init -)"`' >> ~/.bashrc
# relogin shell
```

From rbenv documentation, to install ruby what I need to do is type 
`rbenv install VERSION`. From that, I'm going straight forward by executing 
`rbenv install 2.7.5`, and done!.

Oh no! Unexpectedly, this command does not work.

```text
$ rbenv install 2.7.5
Downloading ruby-2.7.5.tar.bz2...
-> https://cache.ruby-lang.org/pub/ruby/2.7/ruby-2.7.5.tar.bz2
Installing ruby-2.7.5...

BUILD FAILED (Debian 12 using ruby-build 20220426)

Inspect or clean up the working tree at /tmp/ruby-build.20240505204602.3062.0O9P0L
Results logged to /tmp/ruby-build.20240505204602.3062.log
...
```

In the last 15 lines of `/tmp/ruby-build.20240505204602.3062.log`,

```text
...
installing default gems from lib:   /home/arie/.rbenv/versions/2.7.5/lib/ruby/gems/2.7.0 (build_info, cache, doc, extensions, gems, specifications)
                                    benchmark 0.1.0
/tmp/ruby-build.20240505204602.3062.0O9P0L/ruby-2.7.5/lib/rubygems/core_ext/kernel_require.rb:83:in `require': cannot load such file -- openssl (LoadError)
...
```

Even though I have OpenSSL installed on the server
```text
$ apt-cache show openssl
Package: openssl
Version: 3.0.11-1~deb12u2
Installed-Size: 2291
Maintainer: Debian OpenSSL Team <pkg-openssl-devel@alioth-lists.debian.net>
Architecture: amd64
Depends: libc6 (>= 2.34), libssl3 (>= 3.0.9)
Suggests: ca-certificates
...
```

I am not sure what happened. OpenSSL is installed, but the installer is unable to locate it.

## Install OpenSSL 1.1.1

After googling, I found a discussion on rbenv ruby-build issue page
(https://github.com/rbenv/ruby-build/discussions/1940#discussioncomment-2663209).

Follow `nielsonrolim` guide to solve openssl case.

```text
$ sudo apt install build-essential checkinstall zlib1g-dev
$ wget https://www.openssl.org/source/openssl-1.1.1q.tar.gz
$ tar zxf openssl-1.1.1q.tar.gz
$ cd openssl-1.1.1q
$ ./config --prefix=/opt/openssl-1.1.1q --openssldir=/opt/openssl-1.1.1q shared zlib
$ make
$ sudo make install
$ sudo rm -rf /opt/openssl-1.1.1q/certs
$ sudo ln -s /etc/ssl/certs /opt/openssl-1.1.1q
```

## Install Ruby 2.7.5

After OpenSSL 1.1.1 installed, try again to install ruby 2.7.5 using rbenv.

```text
$ RUBY_CONFIGURE_OPTS=--with-openssl-dir=/opt/openssl-1.1.1q rbenv install 2.7.5
$ rbenv local 2.7.5
```

## Install Rails 6.0.4.4
```text
$ gem install nokogiri -v 1.15.6
$ gem install sqlite3 -v 1.4.4
$ gem install rails --version 6.0.4.4
```

## Create Blog
```text
$ sudo mkdir -p /sites
$ sudo chown -R arie: /sites
$ cd /sites
$ rails new blog
```

edit `Gemfile` to use `sqlite3` version `1.4`. find `gem 'sqlite3', '~> 1.4'`
replace with `gem 'sqlite3', '1.4'`.

## Install nvm
```text
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
$ nvm install --lts
$ nvm install -g yarn
```

## Test Running
```text
$ cd /sites/blog
$ rails webpacker:install
$ bin/rails server
```

## Nginx + Phusion Passenger
```text
$ sudo apt update
$ sudo apt install nginx
$ sudo sudo apt-get install -y dirmngr gnupg apt-transport-https ca-certificates
$ curl https://oss-binaries.phusionpassenger.com/auto-software-signing-gpg-key.txt \
    | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/phusion.gpg >/dev/null
$ echo "deb https://oss-binaries.phusionpassenger.com/apt/passenger bookworm main" \
    | sudo tee -a /etc/apt/sources.list.d/passenger.list
$ sudo apt update
$ sudo apt-get install -y libnginx-mod-http-passenger
$ if [ ! -f /etc/nginx/modules-enabled/50-mod-http-passenger.conf ]; then
    sudo ln -s /usr/share/nginx/modules-available/mod-http-passenger.load /etc/nginx/modules-enabled/50-mod-http-passenger.conf
fi
$ sudo ls /etc/nginx/conf.d/mod-http-passenger.conf
$ sudo rm /etc/nginx/sites-enabled/default
```

create a config site.
```text
server {
	listen 80 default_server;
	server_name _;
	passenger_enabled on;
	passenger_app_env development;
	passenger_ruby /home/arie/.rbenv/shims/ruby;
	root /sites/blog/public;
}
```

restart nginx
```text
$ sudo systemctl restart nginx
```

<h2 id="latest-version">Latest version rbenv</h2>

To install Ruby 2.7.5 using latest rbenv

```text
$ sudo apt install curl git
$ sudo apt install build-essential zlib1g-dev
$ git clone https://github.com/rbenv/rbenv.git ~/.rbenv
$ ~/.rbenv/bin/rbenv init
$ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
# re-login shell
$ rbenv install 2.7.5
```
