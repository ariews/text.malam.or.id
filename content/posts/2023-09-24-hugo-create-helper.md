---
title: Hugo Create Helper
date: 2023-09-24T20:37:07+07:00
tags:
  - Hugo
  - Script
---

Di `hugo`, _entry_ baru itu dibuat di `console` dengan menggunakan perintah
`hugo new content filename.md`

misalnya:

```text
$ hugo new content filename.md
Content "/path/to/blog/content/filename.md" created
```

Dan nanti kita akan dibuatkan file baru bernama `filename.md` di`content/posts` 
folder. Abis itu, kita tinggal edit deh filenya.

Tapi hal ini mungkin akan bermasalah dikedepannya, sebab ada kemungkinan kita
akan membuat entry yang lain dengan nama file yang sama. Hal ini bisa kita atasi
dengan membuat nama file yang berbeda disetiap saat kita membuat entry baru.
Misalnya: `hugo new content my-other-blog-title.md`.

Untuk saya pribadi, saya lebih suka menambahkan tanggal sebagai _prefix_ pada 
nama filenya, seperti: `hugo new content 2023-09-23-new-blog.md`. Ini akan 
membuat file entry yang kita buat lebih rapih tersusun berdasarkan tanggal 
pembuatan.

![Directory Structure](/images/hugo-directory-structure.png)

Tapi karena hugo tidak membedakan antara `page` dan `posts`, dan akan sulit bagi
kita untuk membedakan antara mana entry untuk post dan mana entry untuk page. 
Untuk lebih mengorganisir file entry, kita bisa menambahkan nama folder sebagai
prefix untuk memisahkan antara post dan page, misalnya: `hugo new content 
posts/2023-09-23-new-blog.md` untuk post, dan `hugo new content 
page/contact-me.md` untuk halaman page. Berbeda dengan post, kita tidak 
memerlukan tanggal pada file entry, karena biasanya page hanya mempunyai
beberapa file saja. Sedangkan pada post bisa berisi puluhan atau ratusan file
(bahkan mungkin ribuan).

Buat saya, agak 'pegel' juga kalau harus menambahkan tanggal setiap membuat
entry baru. Karena itu saya mencoba untuk membuat sebuah _helper_ yang berguna
untuk menambakan tanggal dan folder pada file yang kita buat. misalnya seperti 
ini:

```text
$ ./create gaspol blog baru nih
Content "/path/to/blog/content/posts/2023-09-24-gaspol-blog-baru-nih.md" created
```

Helper ini membantu kita untuk menambahkan tanggal secara otomatis, dan 
menempatkan file kita di folder posts, dan juga secara otomatis mengubah nama
file kita menggunakan [kebab-case](https://www.theserverside.com/definition/Kebab-case)
format.

Berikut adalah cuplikan file helper `create` yang saya buat.

```bash
#!/usr/bin/env bash

set -e

# 5 functions below are stolen from casbab
# https://github.com/vandot/casbab

detect() {
  # Return normalized string, all lower and with spaces as separators
  arg=( "${@:-}" )
  helper=""
  for i in '_' '-' ' '; do
    if [[ ${arg[*]} == *$i* ]]; then
      helper="yes"
      echo "${arg[*]}" | tr '[:upper:]' '[:lower:]' | tr -s "${i}" ' '
    fi
  done
  # If '_','-' and ' ' are not used as a separator try by case
  if [[ -z $helper ]] && [[ ${arg[0]} != '' ]]; then
    dif_case "${@:-}" | tr '[:upper:]' '[:lower:]'
  fi
}

check_case() {
  # Check char case
  if [[ ${1:-} == [[:upper:]] ]]; then
    echo 0
  elif [[ ${1:-} == [[:lower:]] ]]; then
    echo 1
  fi
}

dif_case() {
  # Parse string char by char
  arg=("${@:-}")
  helper=""
  for ((i = 0; i < ${#arg}; i++)); do
    if [[ $i == 0 ]]; then
      helper="${arg:$i:1}"
    elif ([[ $(check_case "${arg:$i:1}") == 0 ]] && [[ $(check_case "${arg:((i + 1)):1}") == 1 ]]) || ([[ $(check_case "${arg:$i:1}") == 0 ]] && [[ $(check_case "${arg:((i - 1)):1}") == 1 ]]); then
      helper="${helper} ${arg:$i:1}"
    elif [[ $i == [^a-zA-Z] ]] || [[ $(check_case "${arg:$i:1}") == $(check_case "${arg:((i - 1)):1}") ]] || ([[ $(check_case "${arg:$i:1}") == 1 ]] && [[ $(check_case "${arg:((i - 1)):1}") == 0 ]]); then
      helper="${helper}${arg:$i:1}"
    else
      echo "Unknown character: ${arg:$i:1}"
      exit 1
    fi
  done
  echo "${helper}"
}

replace() {
  # Read from stdin and replace ' ' with $1
  cat | tr -s ' ' "${1}"
}

kebab() {
  detect "${@:-}" | replace '-'
}

ENTRY="posts/$(date +'%Y-%m-%d')-$(kebab "$@").md"
HUGO=$(which hugo)

$HUGO new content "$ENTRY"
```
