---
title: "Hello again"
date: 2023-09-23T20:33:06+07:00
tags:
  - myself
  - weblog
---

Haha, bingung cari judul, mohon maklum karena telah sekian lama tidak ngeblog dikarenakan hal ini-dan-itu.

Salah satu alasan utama adalah kesibukan sehari-hari (alesan klasik hehe), juga malesnya mindahin semua
[blog lama]({{< ref "archives" >}}) ke sini.

Sudah beberapa kali blog ini berpindah-pindah `host` dan `engine`, mulai dari wordpress, drupal, pelican dan sekarang 
menggunakan Hugo. Untuk host, dari server gratisan, berbayar dan 
[sekarang Gitlab]({{< ref path="2019-11-05-pindah-host-ke-gitlab.md" >}})
_(gratisan juga, hehe)_.

Banyak cerita kenapa harus berpindah, dan yang terakhir ini adalah konversi dari (balik lagi) format 
[reStructuredText](https://en.wikipedia.org/wiki/ReStructuredText) ke [Markdown](https://en.wikipedia.org/wiki/Markdown).
Gak banyak sih, hanya 458 _files_. Tadinya bikin _script_ buat otomatis konversi, tapi kadang ada beberapa yang
gak sesuai, yang akhirnya di ubah semi-otomatis. Untuk _header_ manual, tapi untuk kontennya menggunakan
[pandoc](https://pandoc.org/).

Tapi sekarang sudah selesai, kecuali mungkin masih ada _deadlink_ karena _linknya_ sudah pada ganti,
dan gak semudah pas pake wordpress/drupal.

Kedepannya mungkin akan nulis yang berhubungan sama pekerjaan (PHP, Golang, Linux, etc) daripada nulis tentang hal-hal
pribadi. Dan karena alesan ini, nanti bakal ada acara _bersih-bersih_ buat hapus blog yang sifatnya pribadi dan 
gak jelas :).

Sekian dulu, sampe ketemu ditulisan selanjutnya.
