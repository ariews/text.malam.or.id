---
title: "QEMU: network 'default' is not active"
date: 2024-06-02T00:01:10+07:00
tags:
  - QEMU
  - Learn English
---
I encounter this error when trying to power up the VM.

```text
Error starting domain: Requested operation is not valid: network 'default' is not active

Traceback (most recent call last):
  File "/usr/share/virt-manager/virtManager/asyncjob.py", line 72, in cb_wrapper
    callback(asyncjob, *args, **kwargs)
  File "/usr/share/virt-manager/virtManager/asyncjob.py", line 108, in tmpcb
    callback(*args, **kwargs)
  File "/usr/share/virt-manager/virtManager/object/libvirtobject.py", line 57, in newfn
    ret = fn(self, *args, **kwargs)
          ^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/usr/share/virt-manager/virtManager/object/domain.py", line 1402, in startup
    self._backend.create()
  File "/usr/lib/python3.12/site-packages/libvirt.py", line 1379, in create
    raise libvirtError('virDomainCreate() failed')
libvirt.libvirtError: Requested operation is not valid: network 'default' is not active
```
I didn't enable the network to start automatically, so this happened.

To solve this, we need to execute command `sudo virsh net-start --network default`.

```text
$ sudo virsh net-start --network default
Network default started
```
