---
title: "Menyusun Kategori dan Tag"
date: 2024-03-05T02:51:22+07:00
tags:
  - Hugo
---

Baru \*ngeh\* ternyata tampilan _tags_ tidak berurutan, terkadang angka dulu
yang duluan ditampilan, terkadang abjad dulu.

Bukan hal yang besar sih, cuman \*gatel\* aja liatnya, pengen ngoprek aja :)

Karena kita menggunakan Hugo, jadi gak seperti aplikasi yang menggunakan 
_database_, kita gak bisa melakukan _sort_ langsung via _database query_.

Yang paling mudah adalah dengan melakukan perubahan pada _templatenya_. Misalnya
seperti ini.

Berikut adalah [partial untuk tag template](https://github.com/rhazdon/hugo-theme-hello-friend-ng/blob/master/layouts/partials/tags.html)
di _theme_ yg saya gunakan saat ini.

```html
{{ with . }}
    <p>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag meta-icon"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7" y2="7"></line></svg>

        {{ range . -}}
            <span class="tag"><a href="{{ "tags/" | absLangURL }}{{ . | urlize }}/">{{.}}</a></span>
        {{ end }}
    </p>
{{ end }}
```

Agar tags bisa kita tampilan secara berurutan, kita bisa mengganti 
`{{ range . -}}` menjadi `{{ range (sort .) -}}`. 

Kira-kira menjadi seperti di bawah ini:

```html
{{ with . }}
    <p>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag meta-icon"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7" y2="7"></line></svg>

        {{ range (sort . ) -}}
            <span class="tag"><a href="{{ "tags/" | absLangURL }}{{ . | urlize }}/">{{.}}</a></span>
        {{ end }}
    </p>
{{ end }}
```
