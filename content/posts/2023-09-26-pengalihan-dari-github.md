---
title: "Pengalihan Dari Github"
date: 2023-09-26T20:48:25+07:00
tags:
  - github
  - weblog
---

Akhirnya semua [web lama](https://arie.malam.or.id) yang ada di Github selesai
diperbaharui. Semua link yang ada disana akan dialihkan ke alamat barunya di 
`text.malam.or.id`.

**Diperbaharui secara manual!**

Iya, semuanya. Karena source blog yang di sana sudah ntah dimana. Sudah coba 
cari ke beberapa harddrive cadangan juga gak ketemu. Ya sudahlah ya...

Sayangnya pengalihan ini dilakukan dari sisi _browser_ dan bukan dari sisi 
server, sehingga tidak diperolah status
[`301`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/301), mudah-mudahan _search engine_ bisa mendeteksi hal ini.

**UPDATE (`2023-10-21T21:41:00+07:00`)**

Sekarang menggunakan fasilitas **Forward to URL** di [freedns.afraid.org](https://freedns.afraid.org/).

```text
 ~ λ curl -v http://arie.malam.or.id
*   Trying 169.47.130.72:80...
* Connected to arie.malam.or.id (169.47.130.72) port 80 (#0)
> GET / HTTP/1.1
> Host: arie.malam.or.id
> User-Agent: curl/7.81.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 301 Moved Permanently
< Server: nginx/1.16.1
< Date: Sat, 21 Oct 2023 15:13:41 GMT
< Content-Type: text/html; charset=UTF-8
< Content-Length: 0
< Connection: keep-alive
< Cache-Control: public, max-age=15
< X-Abuse: URL redirection provided by freedns.afraid.org - please report any misuse of this service
< Location: http://text.malam.or.id
< 
* Connection #0 to host arie.malam.or.id left intact

```

Kita sudah mendapatkan status `301`, sudah dipastikan _search engine_ semacam 
Google bisa mendeteksi perubahan ini.

Sayangnya, hanya tidak support protocol `HTTPS`.
