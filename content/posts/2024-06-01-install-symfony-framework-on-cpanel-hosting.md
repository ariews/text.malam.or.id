---
title: Install Symfony framework on CPanel hosting
date: 2024-06-01T00:03:40+07:00
images:
tags:
  - CPanel
  - Symfony
  - Learn English
---

Yesterday, I ordered a shared hosting package for [malam.or.id](https://malam.or.id)
and planned to run a minimal Symfony project on it.

I needed to customize the Symfony folder structure 
because the hosting used CPanel and Apache as the web server.

CPanel served all html files under `public_html` directory and Symfony
uses `public` directory as entry point or front page.

So, all I need to do is tell Symfony that we will use `public_html` 
as front page instead of `public` directory.

```text
# new structure
root/
 ├─ public_html/
 │  └─ index.php
 └─ sfapp/
    ├─ bin/
    ├─ config/
    ├─ src/
    ├─ ...
    ├─ vendor/
    └─ .env
```

To achieve this, we need to follow the instructions in the [documentation](https://symfony.com/doc/current/configuration/override_dir_structure.html#override-web-dir).

1. Copy index.php from the `public` directory to `public_html` and change 
   the path to `autoload_runtime.php` file.  
   In my case, change to: `require_once dirname(__DIR__).'/sfapp/vendor/autoload_runtime.php';`
2. Add extra configuration to `composer.json`    

    ```text
    {
      "extra": {
        "...": "...",
        "public-dir": "../public_html"
      }
    }
    ```
3. Because the hosting use Apache as web server, add `symfony/apache-pack`
   to `composer`. This package will create a `.htaccess` file in 
   `public_html` directory.

Done! we ready to deploy Symfony app to the hosting.

Source code: https://gitlab.com/team-amorid/malam.or.id
