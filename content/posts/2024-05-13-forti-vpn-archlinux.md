---
title: "Connecting to FortiVPN On Arch Linux"
date: 2024-05-13T11:53:55+07:00
tags:
  - Arch Linux
  - Forti VPN
  - Learn English
---
I have been remotely working with a company that requires me to connect to 
their network using FortiVPN.

Previously, I was running [Debian Bookworm](https://wiki.debian.org/DebianBookworm)
and didn't have any issues with installing and using the FortiVPN client. 
But about 3 days ago, my storage crashed, fortunately all my work files 
are safe.

Since I had to reinstall the system, I decided to go with 
[Arch Linux](https://archlinux.org), another one of my favorite Linux distros.

The installation was smooth and had no hassle, and it's always fun to install 
Arch Linux. After everything is installed and set up, the last thing I need is 
a FortiVPN client to connect to the company network.

At first, I tried to install `fortivpn-client` (atm, version `7.0.7.0246-2`), 
no problem with the installation but unable to connect to the VPN. The same thing 
happened after I installed `forticlient` (version `7.2.2.0753-1`).

I tried to googling and search on the forum, but still unable to fix the problem.
Until I found [openfortivpn](https://github.com/adrienverge/openfortivpn). And the
installation was easy, `sudo pacman -S openfortivpn`.

From the manual, you can connect to Forti VPN with 
`sudo openfortivpn vpn-server-host:port --username=username`

```text
$ sudo openfortivpn vpn-server-host:port --username=username
VPN account password: (type your password here)
INFO:   Connected to gateway.
INFO:   Authenticated.
INFO:   Remote gateway has allocated a VPN.
Using interface ppp0
Connect: ppp0 <--> /dev/pts/2
INFO:   Got addresses: [10.212.134.10], ns [192.168.0.68, 192.168.0.71]
INFO:   Negotiation complete.
Cannot determine ethernet address for proxy ARP
local  IP address 10.212.134.10
remote IP address x.x.x.x
INFO:   Interface ppp0 is UP.
INFO:   Setting new routes...
INFO:   Adding VPN nameservers...
Dropped protocol specifier '.openfortivpn' from 'ppp0.openfortivpn'. Using 'ppp0' (ifindex=10).
INFO:   Tunnel is up and running.
```

We can also put our credential in `/etc/openfortivpn/config` file. Here's mine:

```text
### configuration file for openfortivpn, see man openfortivpn(1) ###

host = vpn.mycompany.com
port = 443
username = arie.w.subagja
password = __password__

trusted-cert = 3e86af11....
```

And we can connect to our vpn by typing: `sudo openfortivpn`.
