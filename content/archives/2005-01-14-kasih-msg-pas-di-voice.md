---
title: "Kasih msg pas di voice"
date: "2005-01-14T19:51:00+07:00"
tags:
  - Friends
  - mIRC
---

Lagi2 si HaZaMi minta script :p, skr dia minta nya.. *auto ngomong kalo
di kasi voice* :P

Sebenernya ini simple banget, beneran, tinggal bikin remote, masukin
event nya.

```text
on *:VOICE:*:{
  ; $chan = channel active
  ; $nick = dia yang kasih voice
  msg $chan makasih buat voice nya $nick
}
```

Awal nya seperti itu, tapi kalo di l iat lagi ada kejadian kalo bukan
kita yang di kasih voice, kita tetep ngasih msg ke cenel. Sekarang kita
ubah sedikit jadi:

```text
on *:VOICE:*:{
  ; pastikan yang di kasih voice itu adalah nick kita
  if ($2 == $me) {

    ; dan jangan lupa, nick yang ngasih voice
    ; adalah orang laen
    if ($nick != $me) {
      msg $chan makasih buat voice nya $nick
    }
  }
}
```

atau mungkin kita bisa nambahin variable laen, misal dengan isvoice.
Begitu simple dan mudah!
