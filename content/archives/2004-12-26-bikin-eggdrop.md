---
date: "2004-12-26T04:52:00+07:00"
title: "Bikin eggdrop"
tags:
    - Tutorial
---

Sebenernya sin dah banyak tutorial bikin
[eggdrop](http://eggheads.org), cuman.. ini karena temen gw blom
ngerti banget intsall eggdrop. ya udah deh yok kita bikin eggdrop,
caranya gampang koq

*Dari shell*

login ke shell, check apakah ada wget atau lynx di sana, whereis wget
atau whereis lynx, kalo gak ada berarti pake ftp atau dengan cvs.

- wget
    ```text
    wget ftp://ftp.eggheads.org/pub/eggdrop/source/eggdrop-latest.tar.gz
    ```
- lynx
    ```text
    lynx -dump \
        ftp://ftp.eggheads.org/pub/eggdrop/source/eggdrop-latest.tar.gz \
        > eggdrop-latest.tar.gz
    ```
- ftp
    ```text
    jz@jurigjarian:~$ ftp 
    ftp> o 
    (to) ftp.eggheads.org 
    Connected to ftp.eggheads.org. 
    220 FTP Server ready. 
    Name (ftp.eggheads.org:jz): anonymous
    Password: myemail@myhostmail.net 
    230-

    Welcome to ftp.eggheads.org

    [ -- welcome.msg -- ]

    230 Anonymous login ok, restrictions apply. 
    Remote system type is UNIX. 
    Using binary mode to transfer files. 
    ftp> cd pub/eggdrop/source
    250 CWD command successful.
    ftp> get eggdrop-latest.tar.gz
    ```
- cvs
    ```text
   cvs -d :pserver:anoncvs:anoncvs@cvs.eggheads.org:/usr/local/cvsroot -z9 co eggdrop1.7
    ```

*FTP ke dalam shell*

Kalo kita pake cara ini, maka kita mesti download eggdropnya ke komputer
kita dulu. eggdropnya bisa di download dari ftp.eggheads.org dengan cara
ftp. misal dengan ws_ftp atau ftp client yang lainnya. Kemudian kita
upload ke shell kita. Jadi dua kali kerja.

*Compile & install*

Setelah download eggdrop (masukin file eggdrop ke dalam shell) maka kita
perlu meng-configure dan meng-installnya. Pastikan ada 
[GCC & Compiler](http://gcc.gnu.org) dan [tcl](http://www.tcl.tk)

> Biasanya tiap shell account support, kecuali lo mo install egg di webhostingan. Heheh gak tau deh

Decompress file source (untuk yang download pake wget, Lynx sama FTP, buat yang pake CVS langsung ke no.2)

```text
tar -zxf eggdrop-latest.tar.gz
```

- Masuk ke folder `eggdrop1.6` atau folder `eggdrop1.7` buat yang pake CVS.
- Kita configure, tik: `./configure`
- Lalu ketik `make config`
- Selanjutnya kita compile dengan mengetikkan `make` atau `make static` atau `make sdebug`
- Setelah compile, kita install eggdrop nya. Caranya juga mudah koq, tik: `make install`
- Eggdrop dah beres kita install.

> catatan: langkah di atas akan membuat folder eggdrop di `$HOME` dir kita, 
> artinya kalo `$HOME` dir kita ada di `/home/ezie` maka kita menginstall 
> ke `/home/ezie/eggdrop`, kalo lo mau install di folder laen, kita tambahin 
> (langkah no 6) jadi: `make install DEST=/home/ezie/botgwa`. Maka kita akan 
> menginstall di folder `/home/ezie/botgwa`.

OK .. sekarang kita masuk ke folder `~/eggdrop` atau
`$HOME/eggdrop`. Di sana ada file yang namanya `eggdrop.conf` kita
edit.

> gak perlu di jelasin ya soal `eggdrop.conf` karena kalo kita buka file na, 
> di sana dah banyak penjelasan yang .. menurut gw KOMPLIT, banyak keterangannya, 
> cuman kalo lo mo rada cepet, lo bisa pake  konfig gw yang ada di 
> [pocongserv](http://www.malam.or.id/blog/352.14.html).

Seabis kita edit (pastikan pasang IP yang sesuai dengan shell lo). Kita
run eggdrop nya.

> Eh rie.. gmn sih ngerun eggdrop nya? koq gw tik `run eggdrop` kagak mo 
> jalan dia nya

Eheheh.. caranya bukan itu, tapi `./eggdrop -m NAMA-CONFIG-FILE` ya
misalnya aja nama file eggdrop lo itu `bot.config` jadi:
`./eggdrop -m bot.config`

Flag -m pada perintah run hanya di pake pada saat pertama kali
ngejalankan bot, selanjutnya gak usah di pake.
