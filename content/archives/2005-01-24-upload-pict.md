---
title: "upload pict!"
date: "2005-01-24T13:15:00+07:00"
tags:
    - Tools
    - Tutorial
---

[jeZz](http://itzjuztme.blogspot.com), temen gw nih nanya, *"rie gmn
cara upload pict ke blog?"*, yang dia maksud dengan kata 'blog' adalah
[blogger](http://www.blogger.com).

Setau gw, blogger gak nerima hosting images, tapi mungkin kita bisa
upload pict di tempat lain, misal
[geocities](http://geocities.yahoo.com), atau yang lainnya. Itu dulu!
cuman dah berapa lama ini ada semacam penambahan services buat yang
ngeblog di blogger. Salah satunya adalah [Hello
BloggerBot](http://www.hello.com/help/blogger.php), jess dah download
itu, cuman keknya blum ngerti cara pakenya. Jadi dia uninstall lagi tuh
bloggerbot nya.

Setelah test uji coba *\[gw juga blom pernah nyoba sebelomna\]*,
sebenernya gampang aja sih, yang perlu kita lakukan adalah:

1.  download [hello blogerbot](https://secure.hello.com/download.php)
    dari [hello](http://www.hello.com).
2.  signup kalo lo blom punya ID di sana.
3.  install, dan run.
4.  chat dengan blogger bot, tunggu sampe dia online. nanti kita seperti
    browsing ke blogger, dan disuruh login.
5.  setelah itu, klik send **pictures**. akan di beri pilihan
    menggunakan [picasa](http://www.picasa.com) *\[dl lagi kalo blom
    ada\]*, atau dengan explorer.
6.  pilih pictures yang akan kita upload.
7.  Send *\[pastikan*\* kita send kepada bloggerbot\]\*.
8.  Isikan *caption* di sebelah kanan bawah *\[chat dengan
    bloggerbot\]*.
9.  klik publish! dan tunggu beberapa saat.

**Result:** <http://sulap-sulip.blogspot.com/>

**Sebagai Tips:**

-   convert jenis images ke dalam format JPG/JPEG *\[bisa pake picasa
    atau ACDsee atau yang laennya\]*.
-   jangan lupa edit post setelah nge-publish pict. karena pict yang di
    upload pake bloggerbot akan menjadi postingan baru di blogger.
    **REPUBLISH** postingan yang dibuat oleh bloggerbot

**Alternatif Lain**

-   Selain bloggerbot, kita bisa juga menggunakan
    [flickr](http://www.flickr.com), <http://members.imagehost.biz> atau
    <http://www.imageshack.us> buat tempat hosting images kita.
-   liat [hello bloggerbot
    forum](http://forums.picasa.com/viewforum.php?f=15) kalo mo nanya
    soal bloggerbot.
