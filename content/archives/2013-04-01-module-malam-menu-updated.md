---
date: 2013-04-01T09:10:00Z
tags:
    - Kerjaan
    - Kohana
    - Kohana 3.2
    - Kohana modules
    - Git
title: "Module malam-menu updated :) "
---
Akhirnya, meski dengan males-malesan, update si [module
menu](https://github.com/ariews/malam-menu) beres juga :)
