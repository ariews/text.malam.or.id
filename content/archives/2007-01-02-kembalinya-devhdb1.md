---
title: "kembalinya /dev/hdb1"
date: "2007-01-02T23:03:00+07:00"
---

Setelah semaleman ngerjain module, gw test lagi recover data di hdd
kedua gw ini. Kalo gw inget2 gw punya
[miniPE](http://www.google.com/search?q=miniPE), kenapa miniPE?
karena gw blom nemu tool buat recover yg keren2 di Linux (kalo lo ada yg
tau, kasih tau gw pls).

Gw coba recover data pake r-studio, tus backup data2 yg penting aja (menurut gw). 
Selanjutnya gw wipe.

[![alt text](/images/50296438ebd20capture_01022007_133147_th.jpg)](http://arie.malam.or.id/images/uploads/capture_01022007_133147.jpg)
/dev/hdb lagi di wipe pake paragon

Gw print screen, tapi bingung paste di mana? biasanya abis pijit print screen gw 
buka ms paint kalo di ms-win, skr gak tau di mana, untung saja ada XnView, jadi 
gw bisa capture image.

[![XnView: Capture (thumb)](/images/502964397474dcapture_01022007_133234_th.jpg)](http://arie.malam.or.id/images/uploads/capture_01022007_133234.jpg)
XnView

Dan terselamatkanlah data2 gw yg kemaren sempet hilang dari peredaran :)

[![Data gw (thumb)](/images/50296439ee830capture_01022007_133831_th.jpg)](http://arie.malam.or.id/images/uploads/capture_01022007_133831.jpg)
Data gw

Sebenernya ada banyak yg bisa di selamatkan, tapi setelah gw pikir2, banyak 
file2 *.deb. Skr gw jadi bisa lebih konsen ngerjain module2 
[xaraya](<http://www.xaraya.com>).
