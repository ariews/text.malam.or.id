---
title: "Lowongan Gawe!!"
date: "2005-03-26T10:01:00+07:00"
---

LOWONGAN GAWE!!

--&gt; pertama

Job Vacancy, PT. Indosat, Tbk

1.  Customer Service
2.  Center Officer
3.  Tele Collection
4.  Direct Collection
5.  Revenue Assurance
6.  Cashier Gallery
7.  Account Management
8.  Promotion & Communication

Valid Until 31 Des 2005

Company Name

PT. Indosat, Tbk

Requirement

-   P Pendidikan Min D3
-   P IPK minimal 2.75 (skala 4)
-   P Usia 20 s.d 30 tahun
-   P Familiar dengan area Jabotabek (lebih disukai)

Alamat Lamaran di kirimkan dengan menyertakan CV, pas photo 4 X 6
berwarna 1 lembar, transkrip nilai dan foto copy ijazah ke alamat:

DEPUTY GM OUTSOURCING SUB DIVISI OUTSOURCING KOPINDOSAT d/a INDOSAT
THAMRIN Gedung BDN Lantai 14

Jl. MH. Thamrin No. 5 Jakarta Pusat 10340

--&gt; kedua

NISP Officer Development Program (NODP-2005)

Valid Until 31 Des 2005

Company Name

PT. Bank Nisp, Tbk

Requirement Lulusan S1 & S2 (semua jurusan) min IPK 2.75 dalam skala
4.00 bagi yang pernah bekerja, pengalaman kerja maksimum 2 tahun
Memiliki kegiatan ekstra kurikuler secara aktif

Menguasai bahasa asing/bahasa daerah merupakan nilai tambah

Alamat Kirimkan surat lamaran, CV, Fotocopy transkrip nilai terakhir dan
pas photo tidak diperlukan lampiran lainnya) serta cantumkan kode
lamaran (NODP-2005) di sudut kiri atas amplop ditujukan kepada:

Recruitment Section-PT. Bank NISP, Tbk Wisma Kosgoro Lt. 4 Jl. M.H.
Thamrin Kav 53

Jakarta Pusat

E-mail: <recruitment@banknisp.com>
