---
title: "plugin wpftp"
date: "2005-09-05T13:59:00+07:00"
tags:
  - Tools
---

![alt text](/images/5029642d1ee67heart.gif)

Kadang kita pengen upload pict atau file laennya, hanya saja space
hosting gak cukup. Kita punya hosting laennya. Tapi sialnya kita mesti
2x kerja.

1.  upload ke hosting cadangan
2.  pasang write.

Untuk space yang kecil (kek punya gw ini), gw bikin [plugin
kecil](/blog/files/wpftp.phps) buat wp ini, dasarnya adalah dari
[ftp.php](/blog/files/ftp.phps).

Keterbatasan waktu untuk menyelesaikan plugin ini terbatas, sehingga
plugin ini masih beta.
