---
title: "sapa tau berguna"
date: "2005-01-18T12:49:00+07:00"
tags:
    - mIRC
---

kadang ada orang yang lag banget saat ceting, sampe2 dia bilang:
`!ping me` atau `!ping NICK` buat ngeping si NICK yg lag.

```text
; 1. EVENTS

; TEXT
; saat ada yang bilang '!ping' di channel
On *:TEXT:!ping*:#:{

  ; variable flood biar kita gak flood
  if (%c.flood != 1) {

    ; pastikan yang bilang bukan kita
    if ($nick != $me) {

      ; bikin variable '%p.chan' sebagai channel active
      set %p.chan #

      ; siapa yang akan di PING? ada di channel apa gak?
      if ( ($2 != $null) && ($2 ison $chan) ) {

        ; selain itu, pastikan pula yang kita ping bukan channel
        if ($chr(35) !isin $2) {

          ; pinging
          CTCP $2 PING
        }
      }
      else {
        if ( ($2 == me) || ($2 == $null) ) {

          ; pinging
          CTCP $nick PING
        }
      }

      ; bikin sebuah variable untuk keperluan laen
      set %c.flood 1
      .timer 1 20 { unset %c.flood }
    }
  }
  else { halt }
}

; ACTION
; sama aja kek yang di atas, hanya saja ini action
; atau /me atau describe
On *:ACTION:!ping*:#:{
  if (%c.flood != 1) {
    if ($nick != $me) {
      set %p.chan #
      if ( ($2 != $null) && ($2 ison $chan) ) { if ($chr(35) !isin $2) { CTCP $2 PING } }
      else { if ( ($2 == me) || ($2 == $null) ) { CTCP $nick PING } }
      set %c.flood 1 | .timer 1 20 { unset %c.flood }
  } } | else { halt }
}

; pada saat kita mendapat replay dari ctcp
On *:CTCPREPLY:*:{

  ; pastikan tidak ada char(35) atau # (indikasi nama channel)
  if ($chr(35) !isin %cnick) {

    ; pastikan juga bahwa ctcp reply ini adalah 'PING'
    if (PING isin $1) {

      ; lalu kita kalkulasikan ping reply ini
      ; dalam format 'DETIK MENIT JAM HARI ...'
      ; dan jadikan hasilnya sebagai variable baru
      %ctcp.reply.ping = $ctime - $2
      set %ctcp.reply PING REPLY: $duration($calc(%ctcp.reply.ping))
    }

    ; sekarang send output ke channel (%p.chan) dan selesai
    $_sprtc(%p.chan,%cnick $+ $chr(44) ping reply %ctcp.reply)
    halt
  }
}

; 2. ALIAS

; alias send_ping_reply_to_channel
alias _sprtc {

  ; send ke channel
  msg $1 $2-

  ; unset variable yang udah di buat, untuk keperluan yang laen
  unset %ctcp.*
  unset %cnick
  unset %p.*
}

; alias ctcp
alias ctcp {

  ; variable ini kita gunakan nanti
  ; sebagai nick yang akan kita ping
  set %cnick $1

  ctcp %cnick $2
}
```

copy dan paste di remote (ALT+R)
