---
title: "dari pito"
date: "2004-11-06T17:45:00+07:00"
tags:
    - Email
---

Dijamin nggak ngerjain. Kalian bakal sebel,lucu, penasaran, pas banget!

> Bacalah dan lihat apakah ini sesuai untukmu.
>
> Dalai Lama

Menarik banget. Hanya empat pertanyaan dan jawabannya akan mengejutkan!
Jangan curang dengan mengintip terlebih dahulu jawabannya. Otak kita itu
seperti parasut. Bekerja dengan baik jika dalam keadaan terbuka.

Dan ini menyenangkan banget untuk dilakukan, asal instruksinya diikutin.
Ucapkan keinginanmu sebelum memulai tes!

**Perhatian !!! Jawablah pertanyaan berikut sesuai dengan pilihan hati
kamu sendiri.**

Hanya terdapat 4 pertanyaan dan jika kamu ngintip semua sebelum kamu
selesein, kamu gak akan dapet jawaban jujur tentang diri kamu.

Arahkan ke bawah secara perlahan, jawablah semua tes secara berurutan
dan jujur. Jangan ngintip pertanyaan nomor berikut jika belum njawab
jawaban yang di atasnya. Pake pensil dan kertas untuk menulis jawaban
karena kamu pasti perlu banget kalo kamu pengen tau jawaban yang jujur
tentang diri kamu.

Semua jawaban akan menceritakan banyak hal tentang diri kamu sendiri.
Jangan takut, ini hanya tes personality Dalai Lama ... :))

PERSONALITY TES:

Ingat tulis jawabannya dengan pensil dan kertas yang sudah kamu
sediakan.

Pertanyaan #1

    Urutkan lima hewan di bawah ini yang menurut Anda bisa mewakili diri Anda(semua harus dipilih namun diurut berdasarkan prioritas pilihan).

1.  Sapi/Cow
2.  Macan/Tiger
3.  Kambing/Sheep
4.  Kuda/Horse
5.  Babi/Pig

Pertanyaan #2

    Tuliskan kalimat yang menjelaskan tentang hal dibawah ini menurut kamu (Contoh Hujan -- Kalimat yang ada dibenak saya adalah: Menyegarkan dan penuh berkah)

1.  Anjing/Dog
2.  Kucing/Cat
3.  Tikus/Rat
4.  Kopi/Coffe
5.  Laut/Sea

Pertanyaan #3

    Pikirkan seseorang yang juga mengenal kamu dan memiliki arti penting buat kamu. Dimana kamu bisa menghubungkannya dengan warna di bawah ini. Jika mendengar warna di bawah ini, siapa orang yang langsung teringat bagi kamu (Jangan mengulang jawabannya, jawaban pertama kamu adalah yang digunakan). Masing-masing warna dijawab hanya dengan menyebut satu nama orang atau teman dekat yang memiliki arti.

1.  Kuning/Yellow
2.  Orange
3.  Merah/Red
4.  Putih/White
5.  Hijau/Green

Pertanyaan #4

    Selanjutnya Tuliskan Angka favorit dan hari favorit kamu. Ucapkan keinginan yang benar-benar kamu inginkan!

Lihat jawaban dari pertanyaan kamu di bawah ini. (Sebelumnya ucapkan
sekali lagi keinginan Anda)

**JAWABAN**

Pertanyaan # 1:

    Hal ini akan menjawab prioritas hidup kamu, mana yang diutamakan:

1.  Sapi/Cow berarti Karir
2.  Macan/tiger berarti Harga Diri
3.  Kambing/Sheep berarti Cinta
4.  Kuda/Horse berarti Keluarga
5.  Babi/Pig berarti Uang/Kekayaan

Pertanyaan #2:

1.  Deskripsi kamu tentang Anjing/Dog merupakan gambaran DIRI kamu
    SENDIRI
2.  Deskripsi kamu tentang Kucing/Cat merupakan gambaran Sifat
    Pasangan kamu.
3.  Deskripsi kamu tentang Tikus/Rat merupakan gambaran Sifat Musuh
    kamu.
4.  Deskripsi kamu tentang Kopi/Coffe merupakan jawaban kamu jika
    ditanya Makna Seks.
5.  Deskripsi kamu tentang Laut/Sea merupakan gambaran ke hidupan
    diri kamu sendiri.

Pertanyaan #3:

1.  Kuning/Yellow adalah seseorang yang tidak akan pernah kamu
    lupakan.
2.  Orange adalah seseorang yang Anda anggap sebagai sahabat sejati
    kamu.
3.  Merah/Red adalah seseorang yang sangat kamu cintai!
4.  Putih/White adalah Seseorang yang hatinya merupakan kembaran
    hati kamu (your Twin Soul)
5.  Hijau/Green adalah seseorang yang akan kamu ingat untuk
    selama-lamanya

Pertanyaan #4:

    Kamu harus mengirmkan Pesan ini sebanyak angka favorit kamu dan keinginanmu akan tercapai pada hari favorit kamu.
