---
date: "2004-12-26T06:16:00+07:00"
title: "I May Know The Word"
tags:
    - Lyrics
---

I may know the word  
But not say it  
I may know the truth  
But not face it  
I may hear a sound  
A whisper sacred and profound  
But turn my head  
Indifferent  

I may know the word  
But not say it  
I may love the fruit  
But not taste it  
I may know the way  
To comfort and to sooth  
A worried face  
But fold my hands  
Indifferent

If I'm on my knees  
I'm begging now  
If I'm on my knees  
Groping in the dark  
I'd be prying for deliverance  
From the night into the day

But it's all grey here  
It's all grey to me

I may know the word  
But not say it  
This may be the time  
But I might waste it  
This may be the hour  
Something move me  
Someone prove me wrong  
Before night comes  
With indifference

If I'm on my knees  
I'm begging now  
If I'm on my knees  
Groping in the dark  
I'd be prying for deliverance  
From the night into the day

But it's all grey here  
But it's all grey to me

I recognize the walls inside  
I recognize them all  
I've paced between them  
Chasing demons down  
Until they fall  
In fitful sleep  
Enough to keep their strength  
Enough to crawl  
Into my head  
With tangled threads  
They riddle me to solve

Again and again and agan

_By Natalie Merchant_
