---
title: "upload pict. 2"
date: "2005-01-26T00:43:00+07:00"
tags:
    - Tools
    - Tutorial
---

Susah bener cara upload poto yang free alias grentongan aka gratis,
kecuali lo ada budget, baru lo bisa nyantey!

Ngeliat file [image
rotator](http://www.spunge.org/~ezie/test/rotator.phps) (PHP Script) gw
jadi kepikiran buat upload pict di tempat laen, tapi gw ambil pict na di
blog ini. mungkin ya sama aja kek gw upload di hosting A tus gw ambil di
hosting B. kan gampang tinggal tik:

`<img src="http://hosting_a/gambar.gif" alt="" />`

di hosting A itu ada kebijakan “[no
hotlinking](http://www.100webspace.com/news/archive/hotlinking_protection_activated.html)”
ya sih.. gw juga suka itu dan jujur.. setuju abieeeess.. cuman
masalahnya kalo gw sendiri yang liat pictna gmn? dan bukan orang laen.
gw mau pict gw di sana, tapi gw load disini, tapi gw juga gak mau di
ambil sama orang gitu aja.

Terus gw baca2 file rotator tadi, edit dikit.. dan bikin file upload
sendiri. hasilnya seperti ini

![linuxtshirt-10.gif](/images/502963ef9927dlinuxtshirt-1.gif)

gw upload pake php, liatnya juga. dan buat yang mo ikutan upload gambar
di sana, kasih tau gw ok? tar gw kasih caranya.
