---
title: "Karena cinta tidak selalu harus berwujud 'bunga'"
date: "2003-10-24T12:04:00+07:00"
---

Suami saya adalah seorang insinyur, saya mencintai sifatnya yang alami
dan saya menyukai perasaan hangat yang muncul di hati saya ketika saya
Tiga tahun dalam masa perkenalan, dan dua tahun dalam masa pernikahan,
saya harus akui, bahwa saya mulai merasa lelah, alasan-2 saya
mencintainya dulu telah berubah menjadi sesuatu yang menjemukan.

Saya seorang wanita yang sentimentil dan benar-2 sensitif serta
berperasaan halus. Saya merindukan saat-saat romantis seperti seorang
anak yang menginginkan permen. Tetapi semua itu tidak pernah saya
dapatkan.

Suami saya jauh berbeda dari yang saya harapkan. Rasa sensitif-nya
kurang. Dan ketidakmampuannya dalam menciptakan suasana yang romantis
dalam pernikahan kami telah mementahkan semua harapan saya akan cinta
yang ideal.

Suatu hari, saya beranikan diri untuk mengatakan keputusan saya
kepadanya, bahwa saya menginginkan perceraian. "Mengapa?", dia bertanya
dengan terkejut. "Saya lelah, kamu tidak pernah bisa memberikan cinta
yang saya inginkan"

Dia terdiam dan termenung sepanjang malam di depan komputernya, tampak
seolah-olah sedang mengerjakan sesuatu, padahal tidak.

Kekecewaan saya semakin bertambah, seorang pria yang bahkan tidak dapat
mengekspresikan perasaannya, apalagi yang bisa saya harapkan darinya?
Dan akhirnya dia bertanya, "Apa yang dapat saya lakukan untuk merubah
pikiranmu?".

Saya menatap matanya dalam-dalam dan menjawab dengan pelan, "Saya punya
pertanyaan, jika kau dapat menemukan jawabannya di dalam hati saya, saya
akan merubah pikiran saya: Seandainya, saya menyukai setangkai bunga
indah yang ada di tebing gunung dan kita berdua tahu jika kamu memanjat
gunung itu, kamu akan mati. Apakah kamu akan melakukannya untuk saya?"

Dia termenung dan akhirnya berkata, "Saya akan memberikan jawabannya
besok." Hati saya langsung gundah mendengar responnya.

Keesokan paginya, dia tidak ada dirumah, dan saya menemukan selembar
kertas dengan oret-2an tangannya dibawah sebuah gelas yang berisi susu
hangat yang bertuliskan....

"Sayang, saya tidak akan mengambil bunga itu untukmu, tetapi ijinkan
saya untuk menjelaskan alasannya." Kalimat pertama ini menghancurkan
hati saya. Saya melanjutkan untuk membacanya.

"Kamu bisa mengetik di komputer dan selalu mengacaukan program di PC-nya
dan akhirnya menangis di depan monitor, saya harus memberikan jari-2
saya supaya bisa membantumu dan memperbaiki programnya."

"Kamu selalu lupa membawa kunci rumah ketika kamu keluar rumah, dan saya
harus memberikan kaki saya supaya bisa mendobrak pintu, dan membukakan
pintu untukmu ketika pulang.".

"Kamu suka jalan-2 ke luar kota tetapi selalu nyasar di tempat-tempat
baru yang kamu kunjungi, saya harus menunggu di rumah agar bisa
memberikan mata saya untuk mengarahkanmu."

"Kamu selalu pegal-2 pada waktu 'teman baikmu' datang setiap bulannya,
dan saya harus memberikan tangan saya untuk memijat kakimu yang pegal."

"Kamu senang diam di rumah, dan saya selalu kuatir kamu akan menjadi
'aneh'. Dan harus membelikan sesuatu yang dapat menghiburmu di rumah
atau meminjamkan lidahku untuk menceritakan hal-hal lucu yang aku
alami."

"Kamu selalu menatap komputermu, membaca buku dan itu tidak baik untuk
kesehatan matamu, saya harus menjaga mata saya agar ketika kita tua
nanti, saya masih dapat menolong mengguntingkan kukumu dan mencabuti
ubanmu."

"Tanganku akan memegang tanganmu, membimbingmu menelusuri pantai,
menikmati matahari pagi dan pasir yang indah. Menceritakan warna-2 bunga
yang bersinar dan indah seperti cantiknya wajahmu".

"Tetapi sayangku, saya tidak akan mengambil bunga itu untuk mati.
Karena, saya tidak sanggup melihat air matamu mengalir menangisi
kematianku."

"Sayangku, saya tahu, ada banyak orang yang bisa mencintaimu lebih dari
saya mencintaimu."

"Untuk itu sayang, jika semua yang telah diberikan tanganku, kakiku,
mataku, tidak cukup bagimu.Aku tidak bisa menahan dirimu mencari tangan,
kaki, dan mata lain yang dapat membahagiakanmu."

Air mata saya jatuh ke atas tulisannya dan membuat tintanya menjadi
kabur, tetapi saya tetap berusaha untuk membacanya. "Dan sekarang,
sayangku, kamu telah selasai membaca jawaban saya. Jika kamu puas dengan
semua jawaban ini, dan tetap menginginkanku untuk tinggal di rumah ini,
tolong bukakan pintu rumah kita, saya sekarang sedang berdiri disana
menunggu jawabanmu."

"Jika kamu tidak puas, sayangku, biarkan aku masuk untuk membereskan
barang-barangku, dan aku tidak akan mempersulit hidupmu. Percayalah,
bahagiaku bila kau bahagia.".

Saya segera berlari membuka pintu dan melihatnya berdiri di depan pintu
dengan wajah penasaran sambil tangannya memegang susu dan roti
kesukaanku.

Oh, kini saya tahu, tidak ada orang yang pernah mencintai saya lebih
dari dia mencintaiku.

Itulah cinta, di saat kita merasa cinta itu telah berangsur-angsur
hilang dari hati kita karena kita merasa dia tidak dapat memberikan
cinta dalam wujud yang kita inginkan, maka cinta itu sesungguhnya telah
hadir dalam wujud lain yang tidak pernah kita bayangkan

sebelumnya.

Seringkali yang kita butuhkan adalah memahami wujud cinta dari pasangan
kita, dan bukan mengharapkan wujud tertentu.

Karena cinta tidak selalu harus berwujud "bunga"
