---
title: "gw hari ini"
date: "2005-07-04T20:39:00+07:00"
---

[worth](http://blogshares.com/user.php?id=28645) gw cuman sampe
5-persen-koma-sekian-sekian saja. hanya sedikit kemajuannya. beban
bertambah berat untuk mempertahankan rank/atau bahkan naek.

Tapi tenang ini baru awal bulan!!!

Ada beberapa email (termasuk spam :D) yang masuk ke emailku
(@yahoo<sup>1</sup>) maklum email gratisan :P, salah satu email itu
adalah dari Pak Hanny (pemilik tadulako.co.id), tempat gw beli hostingan
buat malam.or.id ini. Isinya meminta kesediaan gw menjadi moderator di
[forumnya](http://tadulako.co.id/forum/).

Dan gw di kasih hadiah berupa sebuah paket reseller, \[padahal gw
berencana ambil reseller bulan november ini\], gpp sekalian gw belajar
pake whm, jadi ntar ada persiapan.

Dan lalu gw skr jadi reseller juga jadi seorang moderator.

Menurut gw, apa yang di berikan oleh pak hanny terlalu berlebihan,
soalna apa yang gw lakukan di sana hanya sekedar mengisi waktu gw yang
kosong, membagi beberapa pengetahuan kecil (yang mungkin sangat berguna
bagi teman2 yang lainnya).

Tertulis pada whm baru (dan yang pertama kali) gw: ezieweb.com. Apa yang
gw mesti lakuin? apakah gw beli domain ezieweb.com dulu lalu gw pasang
di sana, atau apakah ezieweb.com itu termasuk ke dalam ‘hadiah’ yang Pak
Hanny berikan?

Oh ya, btw gw masih belajar bikin gambar, kemaren sempet kejebol captha
gw dengan sejumlah komentar spam. Juga, gw bakalan (dan memang
HARUS:keknya sihhh) belajar di bidang Grafis Design.

Wordpress:

ada yang baru gw perhatikan pada wp1.6 ini, Simplenya tulisan pada file
`.htaccess` gak seperti biasanya.

```text
# BEGIN WordPress
<ifmodule mod_rewrite.c>
RewriteEngine On
RewriteBase /blog/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /blog/index.php
</ifmodule>

# END WordPress
```

karena begitu simple, gw jadi pengen ganti permalink nya menjadi:
`/%post_id%.%day%.html`…

Hal ini sama sekali berbeda dengan permalink dengan wp yang lama. Karena
hal ini, maka halaman [archives](/blog/archives.php) di tiadakan
(kemungkinan di cari jalan keluarnya, atau di hack ulang).

<sup>1</sup>) sebenernya di @yahoo cuman untuk akses saja, email aslinya
ada di tempat laen.
