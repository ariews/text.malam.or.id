--- 
date: "2014-12-10T22:15:00+07:00"
title: "Monyet Lancar Jaya!"
tags:
    - Monkey
    - Debian
    - Nginx
    - Squeeze
---

Ok, udah dicoba beberapa hal. Nyoba balik ke nginx, makan memory lebih
gede, ya gak gitu jauh sih, tapi lumayan untuk VPS gw yang lowmem, juga
lumayan nambah kerjaan buat gw yg lagi males nyeting, soalnya kudu milih
cginya nih mau pake apaan. Soalnya secara bawaan nginx gak ngedukung
CGI.

Akhirnya nyoba downgrade pake [monyet yang ada di
squeeze](https://packages.debian.org/squeeze/monkey), dan lancar jaya
;). Autoblogged bisa jalan lagi, juga memory yg kepake cuman dikit.
Huuuuraaayyy!!

Struktur folder dan file yg gw pake kek gini:

```text
/var/www/arie.malam.or.id/
  |- cgi <-- tempat naro Perl script buat autoblogged
  |- public <-- ouput dari Pelican, juga jadi document root
                untuk web
```

Isi Perl script yg gw pake kira2 seperti ini:

```perl
#!/usr/bin/perl

use Net::IP::Match::Bin;

print "Content-type:text/plain\n\n";

my $ipm = Net::IP::Match::Bin->new();

# http://goo.gl/4ZNMGs
$ipm->add(['131.103.20.167', '131.103.20.165',
           '131.103.20.168', '131.103.20.166',
           '131.103.20.169', '131.103.20.170']);

if (($ENV{'REQUEST_METHOD'} eq 'POST') &&
    ($ipm->match_ip($ENV{'REMOTE_ADDR'}) != undef)) {
    exec('/path/to/bash/script.sh');
    print 'Executed!';
}
```

Hanya untuk menjalankan script yg sebenernya sih, yaitu si shell script,
yg tugasnya adalah check update dari blog repo (termasuk plugins, themes
dan tentu aja content), lalu menjalankan pelican di virtualenv.

ps: postingan ini merupakan test untuk autoblogged.
