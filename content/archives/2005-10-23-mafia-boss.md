---
title: "mafia boss"
date: "2005-10-23T23:39:00+07:00"
---

![The Mafia Boss](/images/50296433a3452themafiaboss.jpg)

Beda sama [thecrims](http://www.thecrims.com) yang mesti milih 1
karakter (bussinesman, hitman, robber, gangster atau pimp), di
themafiaboss kita menjadi diri kita sendiri, seorang bos mafia.

Ada banyak perbedaan pada keduanya. Selain yng disebut di atas, juga ada
beberapa yg jadi perbedaan ([lihat lebih detil tentang
themafia](http://themafiaboss.com/guide.htm)).

> You wanna be a real Mafia Don like Al Capone, John Gotti and have all
> the little mafiosos "Kiss Your Ring ... or Die", then you are at the
> right place!

Dari perbedaan2 yang ada. Paling menonjol adalah:

-   Di Thecrims kita membutuhkan stamina, kalo stamina ini habis kita
    bisa mengisi kembali dengan drugs. Respect, strenght, intellegence,
    charisma, dan tolerance akan bertambah. Selama kita bisa mengisi
    kembali stamina, kita bisa melakukan operasi (perampokan,
    penyerangan dan lain2) secara terus menerus tanpa henti.
-   Di themafiaboss, kita membutuhkan 'turns'. Jika habis maka kita
    harus menunggu sampe tersedia kembali.

btw, gw kemaren bisa juga dapat blackjack, sayang euy duitna dikit.

![blackjack!](/images/5029643424d32blackjack.jpg)
