---
title: "HappyBirthday!"
date: "2005-04-15T20:23:00+07:00"
tags:
  - Friends
---

![ultah.jpg](http://x3.freeserverhost.net/seepicts.php?img=/ultah.jpg)
via
[stef](http://www.pepperdesign.gr/blog/2005/04/15/happy-birthday/):
Happy birthday kesh!

hari ini gw abis ngajarin stef, dia pengen masukin halaman "about me"
dan "contact page". untuk yang "about me" kita bisa bikin melalui admin
panel, lalu kita bikin page (Write -&gt; Write Page) kita kasih title:
**about me** dan post slug about. itu dah beres, dan gak ada masalah.

Masalah muncul ketika gw ngasih tau cara bikin halaman kontak "contact
page", udah inggris gw blepotan, buka kamus juga,... jadinya berapa kali
stef ngelakuin kesalahan yang disebabkan oleh bahsa gw yang kacau.
terakhir ge ngomong. gw aja deh yang login ke sana, biar gw yang benerin
:P (di translate sama gw biar lo ngerti :D)

Setelah gw masuk ke cpanelnya [pepper](http://www.pepperdesign.gr), gw
benerin filenya. huM... file nya sih bener, cuman cara nyatuinnya yang
gak bener, ini yang bikin layout blog jadi ancooooooorrr :P. btw,
contact page yang punya gw, bisa lo liat di:
[sini](http://ezie.spunge.org/files/contact.txt).

Lalu yang terakhir adalag: pasang plugins. stef pengen plugins mood,
yang ada musicnya. gw pertama kalo gak ngerti, plugin yang gmn? ya gw
sarankan aja biar stef cari2 plugins dia di
[codex/Plugins](http://codex.wordpress.org/Plugins), gak taunya dia dah
dapat, cuman yang dia bingung: gmn cara masangnya, itu aja.

plugins nya adalah:
[myMooMus](http://darkestbeforedawn.co.uk/index.php?p=510)
