---
title: "belajar lagi :)"
date: "2006-08-30T18:58:00+07:00"
tags:
    - Eggdrop
---

```tcl
!/usr/bin/tclsh

set vara "Variable A"

proc getvara {} {
    global vara
    puts $vara
}
# result: Variable A

proc getvara2 {} {
    set vara "Variable B"
    puts $::vara
}
#result: Variable A

proc getvara3 {} {
    set ::vara "Variable B"
    puts $::vara
}
#result: Variable B
```

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
