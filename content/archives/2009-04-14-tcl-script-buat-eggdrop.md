--- 
date: "2009-04-14T02:17:00+07:00"
title: "Tcl script buat eggdrop"
tags:
    - Uncategorized
---

Yaa.. gw mulai kembali nulis2 tcl script buat
[eggdrop](http://eggheads.org) beberapa waktu lalu. Tapi yang sekarang
gw tulis itu script buat bot di [channel #sukabumi](irc://irc.dal.net/sukabumi), gw berencana, membuat script
itu menjadi 3 bagian.

1.  Master, yang mengolah, mengatur script utama. Bot yang menggunakan
    Master ini gak mesti chanop. Master yang akan bertanggung jawab
    menambah badwords, spammer, inviter, flooder, dsb, lalu mengirimkan
    listnya ke Slave.
2.  Slave, dia yang mengeksekusi inviter, spammer, flooder, jadi mesti
    chanop.
3.  Spy, dia yg nyari tau soal spammer, mencatat user host nya, lalu
    mengirimkan ke Master.

Semuanya masih dalam rancangan (yahh emang sudah berjalan sih
penulisannya).
