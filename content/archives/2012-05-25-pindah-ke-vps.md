--- 
date: "2012-05-25T00:00:00+07:00"
title: "Pindah ke VPS"
tags:
    - VPS
    - Pindah Hosting
---

Udah berapa lama ini, hostingan gw ada di bluehost. Tapi, karena sesuatu
hal, gw pindah ke vps murah. Lagian blog ini bukan web yg punya traffic
tinggi, jadi gak perlu yg bagus2 deh.

Tadinya sih gak mau pindah, cuman karena gw blum sanggup buat bayar 2
hostingan, gw putusin untuk pilih 1 aja.

Si VPS ini bisa gw otak atik seenak gw juga, gak perlu ribet2 masukin
tiket kalo gw pengen install aplikasi yg gw mau.

Selain itu (semua kerjaan gw) gw host di sini, pake git & gitosis.
