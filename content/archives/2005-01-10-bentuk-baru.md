---
title: "Bentuk Baru"
date: "2005-01-10T12:59:00+07:00"
---

Cerita singkat di Sukabumi.

Ketika gw balik, gw naek mobil dinas, hehehe nyokap ke bandung bawa
mobil dinas, katanya mo ada kerjaan, jadi ya sekalian ikut. Mayan irit
ongkos :p. Sebelom balik, gwa nagnter nyokap dulu yang ada belanjaan
kantor, dia mo beli kain buat keperluan di Rumas Sakit.

Sampe di rumah sekitar jam 8 malem, langsung benerin kompi ade yang RAM
na agak2 error, soalna ada 3 RAM yang di pasarng, 64M sama 2 biji 128M
(adek gw beli RAM baru, katanya gak bisa kedetek sama CPU nya, sialnya
yang 128M). Menurut pengalaman gw **ciee**, itu bisa di akalin sama
pindah2in posisinya aja di slots memory. Dipindah slot sana sini, di
ganti, single, juga masih gak bisa, aneh...

Akhirnya di balikin lagi ke posisi lama (2 RAM lama tanpa ada tambahan
RAM yang baru). Ya udahlah, akhirnya itu RAM baru emang gak kedetek di
kompi nya, jadi rencanya di jual aja. 128M = 150rebu SDRAM.

Setelah bongkar pasang kompi, kita pada ngobrol di ruang tengah,
ceritanya besok (kemaren minggu berarti) si dede (anak dari tante gw
yang paling kecil) mau di sunat. Kasian juga, seabis di sunat dia
kliatan nahan sakit terus, meringis gak ada abisnya. Semoga cepet sembuh
ya de dan **Selamat menempuh Bentuk Baru** :P.

Nah sekarang gw dah di bandung lagih. Selama ada di Sukabumi, yang ada
di pikiran gw itu ya:

-   Buru2 ke bandung, supaya cepet2 ketemu ochie. gw kangen banget sama
    dia.
-   Cepet tulis blog :P

Selamat datang di bandung rie :P.
