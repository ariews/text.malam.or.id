---
title: "Mother's Love"
date: "2004-11-19T04:48:00+07:00"
---

Tak ada yang menyamai seorang ibu. Ibu memberi kita hidup, mencintai
kita, merawat kita, dan menginginkan yang terbaik bagi kita, berapa pun
umur kita. Mereka tak pernah berhenti menjadi ibu kita.Tak ada cara yang
bisa menjelaskan anugerah yang mereka berikan kepada kita.

Dan tak ada pengalaman lain di dunia yang bisa dibandingkan dengan
menjadi seorang ibu. Mengandung, melahirkan, dan baik bagi ibu angkat
maupun ibu kandung, melihat wajah bayi mereka untuk pertama kalinya,
peristiwa-peristiwa yang sangat bermakna ini hanyalah awal dari sebuah
peran yang sangat mengesankan dan unik, yaitu yang kita sebut peran
seorang ibu.

Siapa lagi yang harus mengatur semua pekerjaan yang diperlukan untuk
membesarkan sebuah keluarga di zaman ini? Kapan saja, seorang ibu bisa
diharuskan menjadi pencari nafkah, pemberi makan, supir, pencuci piring,
pembantu, sekretaris, perawat, penegak hukum, tukang belanja, dan
sebagainya. Kadang seorang ibu harus melakukan semua pekerjaan ini
sekaligus! Jelas bahwa kaum ibu adalah pelaku multiguna yang pertama.

Karena para ibu memnpunyai tempat yang begitu istimewa dalam hati kita,
maka kami memfokuskan isi makalah ini penuh dengan cinta seorang ibu
(Mother's Love).
