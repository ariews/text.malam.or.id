--- 
date: "2014-11-12T01:29:00+07:00"
title: "Menggunakan Monkey!"
tags:
    - Monkey
    - Debian
    - Nginx
---

Beberapa hari ini, gw lagi mikirin gimana caranya supaya VPS gw, yg
memorynya cuman 256M bisa punya memory tambahan. Selama ini gw pake
[nginx](http://nginx.org), dan karena VPS gw memorynya rada terbatas, gw
install dengan:

```text
$ sudo apt-get install nginx-light
```

Belum yakin sih, `nginx-light` ini nginx yg lebih ringan atau gak, jika
dibandingin sama `nginx` atau bahkan `nginx-extra` atau `nginx-full`,
gak ngerti juga. Mungkin karena lebih ringan, jadinya pake nama light,
haha sok tau nih.

Nah, si nginx ini emang make memory kecil, jadi masih bisa lah untuk
handle web, soalnya juga pengunjung blog ini cuman web spider aja :)

Cuman karena gw juga ada aplikasi yang jalan dibelakang (background),
yang juga butuh [MySQL](http://mysql.com) jadi mesti mikir2 lagi. _MySQL_
udah gw setting pake setingan sederhana sih, cuman aktifin `MyISAM` aja,
`InnoDB` udah gw disable, tapi teteepppp makan memory.

Juga karena web ini cuman buat serve static HTML, gw jadi mikir2 untuk
mengganti dengan yg lebih _light_ lagi, nyari2 di uum gugel,
akhirnya nemuin [monyet](http://monkey-project.com).

Gw gak (atau belum) nemuin paket debiannya, jadi masih install manual.

Begitu nyala, memory yg dipake cuman 500~650KiB, gak sampe 1MiB, kalo
nginx masih bisa sampe 1.2MiB.

Yahhh lumayan ah, tapi gw belom berani buat jadiin si monyet ini sebagai
web server utama di web yang banyak trafficnya.

Masih ujicoba :)

**UPDATE** *(2014-11-12T13:41:00+07:00)*

Ternyata sampe 1MiB juga tuh, catatannya ini bukan web yang sibuk, jadi
pengen tau gmn kalo di web sibuk ya.

```text
root@arie:~# ./ps_mem.py | awk '/monkey|Program/'
 Private  +   Shared  =  RAM used   Program
840.0 KiB + 186.0 KiB =   1.0 MiB   monkey: server
```

Tambahannya, si monyet blom support redirect per URL keknya, atau gw yg
blom tau gmn setingannya. Karena dulunya pake
[Kohana](http://kohanaframework.org), ada banyak URL yang pengen gw
redirect ke URL baru.
