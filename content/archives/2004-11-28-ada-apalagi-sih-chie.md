---
title: "Ada apalagi sih chie?"
date: "2004-11-28T15:47:00+07:00"
---

Pagi tadi gw emang bagun siang.. bukan karena gw begadang.. cuman karena HARI INI 
ADALAH MINGGU! Gw mau agak santay tadinya.. sampe ce gw (Ochie) 
[sms](http://en.wikipedia.org/wiki/Short_message_service).. dan kita saling 
sms-an.. *yeah.. walo gw masih rada2 ngantuk.. atau mongkin gak nyadar apa yang 
lagi gw tik di sms, maap kalo sms na lama.. otak masih lag hehe*

Sekitar jam (lupa lagi) Ochie tefon gw.. katanya minta jebolin ip yang dia yakin 
nyalah gunain kepercayaan, sebut aja si X, tus minta gw bangun, ya emang gw 
ngerasa dah waktunya bangun sih.. jadi gak usah di suru juga gw emang mo bangun. 
Sebelom gw OL, gw mandi dolo.. tus pesen makan ke pak moes, biasa lah paling 
juga roti bakar atau mie, soalna hari minggu warung nasi pada tutup.

Abis makan gw OL, dan ngobrol bentar.. n minta 
[IP](http://en.wikipedia.org/wiki/Internet_Protocol) yang katanya mo di jebolin..

> Sebelom lebih jauh lagi.. gw ceritain aja persoalan: kenapa sih Ochie minta gw
> jebolin IP si X
> Karena si X ini di rasa nyalah gunain kepercayaan dia, atau temennya. Mestinya 
> dia narok bot scramble di #samarinda [allnet](http://www.allnetwork.org), 
> [mirc](http://www.mirc.co.uk) bot aja koq sebenernya, tapi bukan itu yang jadi 
> masalah.. **KEPERCAYAAN**. Jadi dia kesel kali ya sama si X.
>
> Ya udah.. skr.. dia pengen tuh gw masuk ke kompinya si X tus hapus file mirc 
> script (bot scramblenya). Simpel tapi rumit!

Ok.. gw butuh [nmap](http://www.insecure.org/nmap/), gw butuh buat liat port apa 
aja yang di buka.. and bisa ngambil kesimpulan apa aja nanti yang bisa di masukin..
*scan dan scan*

Dan scan itu lama… gw jadi banyak diem daripada ngobrol sama Ochie. Sampe siangan, 
gw masih masih banyak diem, tus pas ngomong.. gw malah minta diri dulu buat beli 
roti, gw laper banget soalna, lom makan!. Gw pergi pesen roti bakar, tus makan 
roti, tadinya gw mau makan roti ya sambil ngobrol lah.. gak enak juga tadi 
banyak diem. But, pas gw sampe di depan kompi, kompina gi di pake sama temennya 
[asep](http://www.sindikat.org). So gw sambil duduk liatin si 
[eddie](http://ediotz.sourceforge.net/tutorial/) maen 
[mineswepeer](http://en.wikipedia.org/wiki/Minesweeper_%28computer_game%29) aja.
Sampe roti abis.. baru temennya asep itu pergi, soalna gw bukain kompi 1 biar 
dia maen di sana.

Pas gw balik lagi ke [irc](http://en.wikipedia.org/wiki/Irc), ochie dah idle 
lama.. sekitar 30 menitan. Sambil nunggu gw liat2 cenel malam, tus ada ngobrol 
sama temen2. Ngobrol biasa aja sih.. ketawa2, becanda2, then.. ke cenel gw lagi 
(gak mau gw sebut nama cenel na soalna +sp :P, kesananya di bilang CSP), karena 
Ochie dah balik lagi, gw tanya *dari mana?*, *disini aja* gitu jawabnya… karena 
gw gak liat dia (lokasi kita jauhan, gw di bdg, dia di bjm) asumsi gw dia 
berarti liat cenel & tau gw ngobrol sama anak2 cenel malam.

Tapi kita koq ngobrol jadi kaku, gw berusaha ngejauhin cenel malam & anak2 
dulu kalo ada dia, buakn apa2 soalna gw mau ngobrol aja sama Ochie, tapi –ya
gw juga sih yang banyak diem tadinya– Ochie jga banyak diem, pas gw ngomong: 
*papa sambil ngobrol di malam ya ma*, *iya gpp koq pa.. silahkan* –Ochie sayang,
yang papa maksud itu sambil ngobrol di sana disini juga ngobrol.

Tapi Ochie malah jadi diem pas gw bilang gitu, terus terang, gw jadi –gmn yah– 
diem gak enak.. pengen ngobrol juga yang di ajak ngobrol diem, jadi gw ngobrol 
aja di malam (lagi2). Akhirnya Ochie DC dari irc, gw gak tau.. apakah quit atau
emang dc, doalna dia pake [psybnc](http://www.psychoid.lam3rz.de).

Ya udah.. gw ngobrol lagi sama anak2, termasuk 
[Stefie](http://www.19-10.net/blog) (Stef-1) di private message (pv). Gak jauh2 
sih kalo ngobrol sama stef, paling juga ngomongin 
[web](http://en.wikipedia.org/wiki/World_Wide_Web), dan emang iya.. dia 
ngomongin soal web 
[virtual bartender](http://www.virtualbartender.beer.com/beer_usa.htm). Saat 
itu Ochie balik online, dan obrolan gw sama stef pun keputus…

Balik lgi ke CSP tadi, Ochie bilang: *papa terusin aja dulu ngobrolnya* dan 
diem.. gw jadi serba salah ngobrol di CSP, kalo ngobrol, Ochie na diem.. gw 
ilang bahan obrolan tapi pengen ngobrol (ilang bahan obrolan setiap Ochie 
bilang: *ya udah papa terusin aja dulu*, padahal gw dah berenti ngobrol), 
kalo gw ngobrol sama temen, gw serba salah juga, karena gw emang pengen ngobrol 
sama Ochie.

Akhirnya Ochie bener2 quit.. gak lama dari itu gw juga kuit.. Gw pergi tidur aja.

Sekitar jam 8 malem, Ochie tefon.. dan gw OL lagi. Di cenel, kita ngomongin.. 
“ada apa sih?”. Iya ada apa? kenapa gw gini, dan kenapa Ochie gitu. tapi masih 
banyak diem :(. Sampe Ochie ngomong kalo dia mo ngurangin OL. Terus terang gw 
gak setuju, tapi asal dia seneng gw iya2 aja, gw gak setuju karena gw butuh dia!

Gw selalu bingung, gak tau mo ngapain kalo keadaan kek gini, terus terang gw 
serba salah. Ngomong salah.. diem malah tambah salah. ngomong.. juga itu2 juga,
diem bikin kamu bete. Mesti gmn sih sayang?
