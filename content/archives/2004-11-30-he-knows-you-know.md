---
title: "He Knows You Know"
date: "2004-11-30T03:56:00+07:00"
tags:
    - Lyrics
---

![Script](http://img72.exs.cx/img72/4724/marillion-script1.jpg)

Light switch, yellow fever, crawling up your bathroom wall Singing
psychedelic praises to the depths of a china bowl You’ve got venom in
your stomach, you’ve got poison in your head You should have listened to
the priest at the confession When he offered you the sacred bread He
knows, you know, he knows, you know

He knows, you know, but he’s got problems

Fast feed, crystal fever, swarming through a fractured mind Chilling
needles freeze emotion, the blind shall lead the blind You’ve got venom
in you stomach, you’ve got poison in your head When your conscience
whispered, the vein lines stiffened

You were walking with the dead

He knows, you know, he knows, you know, he knows, you know He’s got
experience, he’s got experience, he knows, you know

But he’s got problems, problems, problems

He knows… slash wrist, scarlet fever, crawled under your bathroom door
Pumping arteries ooze their problems through the gap that the razor tore
You’ve got venom in your stomach, you’ve got poison in your head You
should have listened to your analyst’s questions

When you lay on his leather bed

He knows, you know, he knows, you know

H he knows, you know, but he’s got problems

Blank eyes, purple fever, streaming through the frosted pane You learned
your lesson far to late from the links in a chemist chain You’ve got
venom in your stomach, you’ve got poison in your head You should have
stayed at home and talked with father

Listen to the lies he fed

He knows, you know, he knows, you know, He knows, you know, but he’s got
problems He knows, you know, he knows, you know, he knows, you know He’s
got experience, he’s got experience, he knows, you know

You know, you know, you know

—\| by Mariilion, Script for a Jester tears
