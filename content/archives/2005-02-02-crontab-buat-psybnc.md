---
title: "crontab buat psybnc??"
date: "2005-02-02T09:24:00+07:00"
tags:
  - Tutorial
---

kemaren si joko.. skr si beny yang nanyain bikin crontab buat
[psybnc](http://www.psycoid.lam3rz.de), susah?? gak eperti
[eggdrop](http://eggheads.org) yang tinggal pake `autobotchk`.
sebenernya sama aja sih, kalo kita liat di folder psybnc, ada file yang
namanya `psybncchk` cuman.. kalo file ini gak masukin crontab otomatis,
itu cuman buat ngecek aja. crontabnya sendiri bisa kita buat manual.

misalnya dengan ngetik: `crontab -e`, lalu isikan entry seperti berikut:

```text
0,5,10,15,20,25,30,35,40,45,50,55 * * * * /home/myhome/psybnc/psybncchk >/dev/null 2>&1
```

ganti `PATH` di atas dengan PATH ke folder psybnc kamu.
