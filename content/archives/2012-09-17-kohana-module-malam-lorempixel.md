--- 
date: "2012-09-17T20:56:00+07:00"
title: "Kohana module malam-lorempixel"
tags:
    - Kohana
    - Kohana module
    - Lorempixel
---

[malam-lorempixel](https://github.com/ariews/malam-lorempixel) dibuat
pas ketika gw butuh dummy image, gw yakin ada banyak service yang
nyediain dummy image, tapi gw begitu nemu langsung pake aja, males
ngebanding2in, hehe.

> Kenapa ga pake dummy image biasa sih? bikin dummy terus save..  
> terus bla bla bla...

Iya.. awalnya sih begitu.. cuman sejak pindah ke pagodabox, spacewebnya
gak sebesar kalo lo sewa hosting. Jadi daripada nambah space, kan gak
ada salahnya pake yang on-the-fly aja :)

Cara pake si lorempixel pun gampang aja, setelah kita download (atau
dijadiin submodule), aktifkan di bootstrap.php, seperti ini:

Terus? definisikan `$width` sama `$height` , untuk menambahkan text,
gunakan method `text()`.

mudahkan? ^^
