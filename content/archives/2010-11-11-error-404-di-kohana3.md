--- 
date: "2010-11-11T12:52:00+07:00"
title: "Error 404 di Kohana 3"
tags:
    - Kohana
    - Kohana 3.2
    - Tutorial
---

Ternyata gampang banget membuat [error
404](http://en.wikipedia.org/wiki/HTTP_404) di Kohana 3. Yang kita perlu
cuman `catch all` semua request yang ga ada di `Route`.

Bikin route seperti ini:

```php
Route::set( 'catchall', '<path>',
     array( 'controller'    => 'catchall',
            'path'          => '.+'))
    ->defaults(array(
            'controller'    => 'catchall',
            'action'        => 'error',
    ));
```

Tempatkan route tersebut di paling bawah route kita.

Lalu buat Controller_Catchall, save di controller/catchall.php, isi
dari controller tersebut seperti berikut:

```php
class Controller_Catchall extends Controller_Template
{
    public $template = 'error404';

    public function action_error()
    {
        $this->request->status = 404;
    }
}
```

Kemudian kita buat view `error404` tersebut, isinya cuman ngasih tau
kalo halaman yang di panggil itu ga ada. Setelah dibikin, maka semua
request yang ga match sama Route, akan di catch sama Controller ini.

source: [Unofficial Kohana 3.0
Wiki](http://kerkness.ca/wiki/doku.php?id=routing:creating_a_custom_404_page)
