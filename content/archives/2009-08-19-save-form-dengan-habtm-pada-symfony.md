--- 
date: "2009-08-19T16:50:00+07:00"
title: "Save form dengan HABTM pada Symfony"
tags:
    - Symfony
    - Kerjaan
---

Sempet kebingungan saat melakukan saving di form yang ada relasi habtm
di symfony. Karena gak otomatis ke-save.

Misal aja untuk database [ini](http://pastie.org/583709), dengan class
[berikut](http://pastie.org/583708), setelah gugling, dan baca2 forum,
akhirnya dapat [jawaban di
forum](http://forum.symfony-project.org/index.php/m/83032/#msg_82985).

Dari sini, gw override method `doSave()` di `TripForm` class menjadi:

```php
public function doSave($con = null)
{
  parent:doSave($con);

  // save table relasi habtm disini, misal untuk kasus gw:

  if ($con == null)
  {
    $con = $this->getConnection();
  }
  $PK = $this->object->getPrimaryKey();

  if (isset($this->widgetSchema['cities']))
  {
    $c  = new Criteria();
    $c->add(TripCitiesPeer::TRIP_ID, $PK);
    TripCitiesPeer::doDelete($c, $con);
    foreach ($this->getValue('cities') as $k => $city_id)
    {
      $TripRel = new TripCities();
      $TripRel->setTripId($PK);
      $TripRel->setCityId($city_id);
      $TripRel->save();
    }
  }
}
```
