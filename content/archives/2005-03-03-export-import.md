---
title: "export import"
date: "2005-03-03T00:18:00+07:00"
tags:
  - Friends
  - Tutorial
---

Kayak nama bank aja.. BANK EXIM, atau kegiatan ekonomi. Tapi buka itu
yang mau gw bahas, ini jawaban pertanyaan
[jess](http://itzjuztme.blogspot.com) (dah pindah dia ke
[pacar.org](http://pacar.org)), yang nanya soal "gimana sih cara
mindahin blog dari [blogger](http://www.blogger.com) ke
[wp](http://wordpress.org)?".

Gini jess ([gw tau belakangan kalo yang nanya itu temen dari kakak na
jess](http://www.pacar.org/2005/02/24/51#comment-13)), sebenernya ada
banyak tutorial soal ini, misalna aja yang dari [tutorial
resmi](http://codex.wordpress.org/Importing_from_other_blogging_software#Importing_from_Blogger)
wordpress, atau yang dari
[bluechronicles](http://tutorials.bluechronicles.net/index.php?p=4)
(malah di sana komplit sama gambar2).
