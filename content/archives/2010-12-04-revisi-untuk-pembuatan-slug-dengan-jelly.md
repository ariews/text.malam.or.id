--- 
date: "2010-12-04T00:38:00+07:00"
title: "Revisi untuk pembuatan slug dengan jelly"
tags:
    - Kohana
    - Kohana 3.2
    - Jelly
    - Bugs
    - Kohana 3
    - Kohana Jelly
---

Pada postingan sebelumnya yang berjudul [Membuat Permalink Otomatis
dengan Jelly]({{< ref path="2010-11-23-membuat-permalink-otomatis-dengan-jelly.md">}}),
gw nulis sebuah file baru yaitu `field/slug.php`.

Class itu berjalan seperti yang diharapkan, tapi memakan banyak waktu.
Hal ini dikarenakan setiap mau save, kita bakal ngecek semua slug. :(
maap yahh...

Ok, kita *fix* skr.

menjadi:

Kita ga perlu cek lagi kalo cuman edit. Postingan ini sebagai testnya.
