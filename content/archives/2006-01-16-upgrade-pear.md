---
title: "upgrade pear"
date: "2006-01-16T03:36:00+07:00"
tags:
    - Tutorial
---

Kemaren2 iseng upgrade php ke 4.4.1, lalu iseng liat list pear packages,
karena masih versi 1.3.6 pear, tus upgrade.

```text
Installed packages:
==================================
Package              Version State
Archive_Tar          1.1     stable
Console_Getopt       1.2     stable
HTML_Template_IT     1.1     stable
Net_UserAgent_Detect 2.0.1   stable
PEAR                 1.3.6   stable
XML_RPC              1.4.0   stable
```

`# pear upgrade-all`

beres.. jadi 1.4.6, tapi gak tau kenapa ada yg error :((

skr ada iseng yg laen.. gmn kalo di hostingan ini pear nya masih blom
upgrade? *ini gak mesti di upgarde koq, cuman iseng aja*

cari2 di [manual](http://pear.php.net/manual/en/)... akhirnya ketemu:

> In order to install a local copy of PEAR through ftp, you must have an
> ftp client that can set permissions on upload. First, create a
> directory that is NOT in your publicly accessible web space, and name
> it anything you'd like. Set it to be readable and writable by any user
> (permissions 0777). Then, download a copy of <http://go-pear.org/> and
> save it as go-pear.php. Upload go-pear.php to a subdirectory of your
> publicly accessible web space. It is a very good idea to
> password-protect the directory containing go-pear.php with an
> .htaccess file.
>
> Next, browse to the go-pear.php file. If your web address is
> <http://www.example.com/>, you have placed go-pear.php in
> public_html/install/go-pear.php, and <http://www.example.com/> will
> browse to public_html/, then browse to
> <http://www.example.com/install/go-pear.php>. Follow the installation
> prompts...

Kelar deh...
