---
date: "2004-12-05T06:05:00+07:00"
title: "VirtualHost"
---

Pengen belajar bikin
[VirtualHost](http://httpd.apache.org/docs-2.0/vhosts/) (VH), kayak
[ezie.spunge.org](http://ezie.spunge.org).. tapi ternyata blom cukup
ilmunya si sayah :) [Apache 1.3](http://httpd.apache.org/docs/) sih
bisa, tapi ini [Apache 2.0](http://httpd.apache.org/docs-2.0/), jadi
blom ngerti.

config VH sayah kek ginih:

```text
### Section 3: Virtual Hosts
## Use name-based virtual hosting.
#
NameVirtualHost *

<VirtualHost wick.ezie.info:80>

    ServerAdmin root@localhost
    DocumentRoot /var/www/virtual/wick/html
    ServerName wick.ezie.info
    ErrorLog logs/error_log
    TransferLog logs/access_log

    ScriptAlias /cgi-bin/ "/var/www/virtual/wick/cgi-bin/"

    <Directory "/var/www/virtual/wick/cgi-bin">
        AllowOverride None
        Options None
        Order allow,deny
        Allow from all
    </Directory>

    Alias /icons/ "/var/www/icons/"

    <Directory "/var/www/icons">
        Options Indexes MultiViews
        AllowOverride None
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>
```

ada yang salah? atau komentar?
