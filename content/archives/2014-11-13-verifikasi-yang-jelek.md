--- 
date: "2014-11-13T15:03:00+07:00"
title: "Verifikasi yang jelek"
---

Kemaren2 gw nemu sebuah PHP _library_. Untuk lebih memahami
maksud dan cara kerjanya, gw baca tuh codenya. Di dalamnya terdapat
beberapa baris code untuk memverifikasi kepemilikan codenya (ini bukan
free _library_, kita harus beli baru dapat file-filenya.

Di bagian inialisasi, si class ini melakukan request ke server pembuat
code disertai data email dan transaksi id pembelian sebagai acuan. Jadi
gak mungkin kita isi dengan sembarang data.

Format URL yang diminta seperti ini:

```text
http://server/verify?mail=email@domain.tld&trid=ID-TRANSAKSI
```

Sampai sini sih gak terlalu menarik. Di baris selanjutnya, hasil dari
request tersebut diproses.

```php
try {
    $result = validate($url);
    $data   = json_decode($result, TRUE);

    if ($data['status'] == 'valid') {
        // ...
    }
} catch (Exception $exp) {
    // ...
}
```

Masih normal sih, sampai gw nyoba ngirim dengan email yang ngasal dan
transaksi id yg asal pula, hasilnya tetep aja valid.

Hasil dari request tersebut kira-kira seperti ini:

```json
{"status":"valid", ... }
```

Ini baru menarik, karena input seperti apapun hasilnya tetep hasilnya
_valid_, _library_ seperti ini dengan mudah
dibikin [NULLED](http://en.wikipedia.org/wiki/Copyright_infringement),
kita bisa _bypass code_ diatas, ganti aja jadi seperti ini:

```php
try {
    // $result = validate($url);
    // $data   = json_decode($result, TRUE);

    $data = array(
        'status' => 'valid',
        // ...
    );

    if ($data['status'] == 'valid') {
        // ...
    }
} catch (Exception $exp) {
    // ...
}
```

Jadi gak perlu melakukan verifikasi online, langsung aja
_bypass_, script kita langsung verified deh.
