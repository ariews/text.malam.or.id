---
date: "2004-12-06T12:40:00+07:00"
title: "Hits, mp3 dan slashdot"
---

Ternyata [perpindahan]({{< ref path="2004-12-03-move.md">}}) ini
masih kurang banyak orang tau. Banyak orang yang masih mengakses [alamat
lama](http://www.spunge.org/~ezie/jz/).

![Hits](http://img72.exs.cx/img72/1456/g6jhits2.gif)

Karena kebanyakan mencari dari mesin pencari kek
[google](http://www.google.com), [yahoo](http://search.yahoo.com),
[msn search](http://search.msn.com/) (sama yang
[beta](http://beta.search.msn.com/)\ nya),
[hotbot](http://www.hotbot.com),
[atltavista](http://www.altavista.com/) dan yang lainnya.

mp3

dari [betanews](http://www.betanews.com/): mp3 mengalami perubahan
dalam bentuk ukuran filenya yang akan semakin [menciut alias makin
kecil](http://www.betanews.com/article/MP3_Goes_51_Channel/1102131014)

Bahkan sampe setengah dari umuran mp3 sekarang ini. duh kalo sekarang
HDD gw 40G bisa muat brp file mp3 ya?

Oh ya tadi pagi gw mo surf ke [slashdot](http://slashdot.org) eh..
malah nemu tampilan kalo gw kena BAN!!

err!!

katanya ip gw di pake yang gak2.. flood. script.. dll dll. padahal cuman
browsing aja koq... ketika di baca lebih teliti.. ternyata di sana ada
tertera IP yang di ban, 62.xx.xx.xx, bukan ip gw lah.. ip gw kan
202.143.xx.xx.

ternyata proxy, bekas yang semalem kali.

![banned from slashdot](/images/502963e1ac4a2f7dbannedfromslashdo.jpg)
