---
title: "Something to ponder about"
date: "2005-06-11T14:48:00+07:00"
tags:
  - Email
---

![butt_051.gif](http://x3.freeserverhost.net/seepicts.php?img=/butt_051.gif)

Tuhan yang Mahabaik memberi kita ikan
![ATT122.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT122.gif),
tetapi kita harus mengail untuk mendapatkannya
![ATT233.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT233.gif).
Demikian juga Jika kamu terus menunggu
![ATT344.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT344.gif)
waktu yang tepat, mungkin kamu tidak akan pernah mulai.

Mulailah sekarang... mulailah di mana kamu berada sekarang dengan apa
adanya. Jangan pernah pikirkan
![ATT455.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT455.gif)
kenapa kita memilih seseorang untuk dicintai
![ATT566.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT566.gif),
tapi sadarilah bahwa cintalah
![ATT677.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT677.gif)
yang memilih kita untuk mencintainya.

Perkawinan
![ATT788.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT788.gif)
memang memiliki banyak kesusahan, tetapi kehidupan lajang
![ATT899.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT899.gif)
tidak memiliki kesenangan
![ATT91010.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT91010.gif).
Buka mata kamu lebar-lebar
![ATTA1111.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATTA1111.gif)
sebelum menikah, dan biarkan mata kamu setengah terpejam sesudahnya
![ATTB1212.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATTB1212.gif).
Menikahi wanita atau pria karena kecantikannya
![ATTC1313.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATTC1313.gif)
atau ketampanannya
![ATTD1414.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATTD1414.gif)
sama seperti membeli rumah
![ATTE1515.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATTE1515.gif)
lapisan catnya. Harta
![ATTF1616.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATTF1616.gif)
milik yang paling berharga bagi seorang pria di dunia ini adalah... hati
![ATT101717.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT101717.gif)
seorang wanita...

Begitu juga
![ATT111818.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT111818.gif)
Persahabatan, persahabatan adalah 1 jiwa dalam 2 raga, Persahabatan
sejati layaknya kesehatan, nilainya baru kita sadari setelah kita
kehilangannya
![ATT121919.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT121919.gif).
Seorang sahabat adalah yang dapat mendengarkan lagu didalam hatimu dan
akan menyanyikan kembali tatkala kau lupa akan bait-baitnya
![ATT132020.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT132020.gif).
Sahabat adalah tangan Tuhan untuk menjaga Kita.

Rasa hormat tidak selalu membawa kepada persahabatan, tapi Jangan pernah
menyesal untuk bertemu dengan orang lain... tapi menyesalah
![ATT142121.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT142121.gif)
jika orang itu menyesal bertemu dengan kamu. Bertemanlah
![ATT152222.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT152222.gif)
dengan orang yang suka membela kebenaran. Dialah hiasan
![ATT162323.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT162323.gif)
dikala kamu senang
![ATT172424.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT172424.gif)
dan perisai
![ATT182525.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT182525.gif)
diwaktu kamu susah
![ATT192626.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT192626.gif).

Namun kamu tidak akan pernah memiliki seorang teman
![ATT1A2727.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT1A2727.gif),
jika kamu mengharapkan seseorang tanpa kesalahan. Karena semua manusia
itu baik
![ATT1B2828.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT1B2828.gif)
kalau kamu bisa melihat kebaikannya dan menyenangkan kalau kamu bisa
melihat keunikannya
![ATT1C2929.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT1C2929.gif)
tapi semua manusia itu akan buruk
![ATT1D3030.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT1D3030.gif)
dan membosankan kalau kamu tidak bisa melihat keduanya.

Begitu juga Kebijakan, Kebijakan itu seperti cairan, kegunaannya
terletak pada penerapan yang benar, orang pintar bisa gagal karena ia
memikirkan terlalu banyak hal, sedangkan orang bodoh sering kali
berhasil dengan melakukan tindakan tepat.

Dan Kebijakan sejati tidak datang dari pikiran kita saja
![ATT1E3131.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT1E3131.gif),
tetapi juga berdasarkan pada perasaan dan fakta. Tak seorang pun
sempurna. Mereka yang mau belajar
![ATT1F3232.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT1F3232.gif)
dari kesalahan adalah bijak. Menyedihkan melihat orang berkeras bahwa
mereka benar meskipun terbukti salah. Apa yang berada di belakang kita
dan apa yang berada di depan kita adalah perkara kecil berbanding dengan
apa yang berada di dalam kita.

Kamu tak bisa mengubah masa lalu... tetapi dapat menghancurkan masa kini
dengan mengkhawatirkan masa depan
![ATT203333.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT203333.gif).

Bila Kamu mengisi hati
![ATT213434.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT213434.gif)
kamu ... dengan penyesalan untuk masa lalu dan kekhawatiran
![ATT223535.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT223535.gif)
untuk masa depan, Kamu tak memiliki hari ini untuk kamu syukuri. Jika
kamu berpikir
![ATT233636.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT233636.gif)
tentang hari kemarin tanpa rasa penyesalan dan hari esok tanpa rasa
takut, berarti kamu sudah berada dijalan yang benar menuju sukses.

![ATT243737.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT243737.gif)

Thanks
![ATT253838.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT253838.gif)
& Salam
![ATT263939.gif](http://x3.freeserverhost.net/seepicts.php?img=/ATT263939.gif)

**note**: ini dari milis smansa1sukabumi
