---
title: "Perkenankanlah Aku MencintaiMu Semampuku"
date: "2003-10-30T11:42:00+07:00"
tags:
    - Email
---

## Dari Dodi

Tuhanku, Aku masih ingat, saat pertama dulu aku belajar mencintaiMu?
Lembar demi lembar kitab kupelajari? Untai demi untai kata para ustadz
kuresapi? Tentang cinta para nabi Tentang kasih para sahabat Tentang
mahabbah para sufi Tentang kerinduan para syuhada Lalu kutanam di jiwa
dalam-dalam Kutumbuhkan dalam mimpi-mimpi dan idealisme yang mengawang
di awan? Tapi Rabbii, Berbilang detik, menit, jam, hari, pekan, bulan
dan kemudian tahun berlalu? Aku berusaha mencintaiMu dengan cinta yang
paling utama, tapi? Aku masih juga tak menemukan cinta tertinggi
untukMu? Aku makin merasakan gelisahku membadai? Dalam cita yang
mengawang Sedang kakiku mengambang, tiada menjejak bumi? Hingga aku
terhempas dalam jurang

Dan kegelapan?

Wahai Ilahi, Kemudian berbilang detik, menit, jam, hari, pekan, bulan
dan tahun berlalu? Aku mencoba merangkak, menggapai permukaan bumi dan
menegakkan jiwaku kembali Menatap, memohon dan menghibaMu: Allahu
Rahiim, Ilaahi Rabbii, Perkenankanlah aku mencintaiMu, Semampuku Allahu
Rahmaan, Ilaahi Rabii Perkenankanlah aku mencintaiMu Sebisaku

Dengan segala kelemahanku

Ilaahi, Aku tak sanggup mencintaiMu Dengan kesabaran menanggung derita
Umpama Nabi Ayyub, Musa, Isa hingga Al musthafa Karena itu izinkan aku
mencintaiMu Melalui keluh kesah pengaduanku padaMu Atas derita batin dan
jasadku

Atas sakit dan ketakutanku

Rabbii,

Aku tak sanggup mencintaiMu seperti Abu bakar, yang menyedekahkan
seluruh hartanya dan hanya meninggalkan Engkau dan RasulMu bagi diri dan
keluarga. Atau layaknya Umar yang menyerahkan separo harta demi jihad.
Atau Utsman yang menyerahkan 1000 ekor kuda untuk syiarkan dienMu.
Izinkan aku mencintaiMu, melalui seratus-dua ratus perak yang terulur
pada tangan-tangan kecil di perempatan jalan, pada wanita-wanita tua
yang menadahkan tangan di pojok-pojok jembatan. Pada makanan?makanan
sederhana yang terkirim ke handai taulan.

Ilaahi, aku tak sanggup mencintaiMu dengan khusyuknya shalat salah
seorang shahabat NabiMu hingga tiada terasa anak panah musuh terhunjam
di kakinya. Karena itu Ya Allah, perkenankanlah aku tertatih menggapai
cintaMu, dalam shalat yang coba kudirikan terbata-bata, meski ingatan
kadang melayang ke berbagai permasalahan dunia.

Robbii, aku tak dapat beribadah ala para sufi dan rahib, yang
membaktikan seluruh malamnya untuk bercinta denganMu. Maka izinkanlah
aku untuk mencintaimu dalam satu-dua rekaat lailku. Dalam satu dua
sunnah nafilahMu. Dalam desah napas kepasrahan tidurku.

Yaa, Maha Rahmaan,

Aku tak sanggup mencintaiMu bagai para al hafidz dan hafidzah, yang
menuntaskan kalamMu dalam satu putaran malam. Perkenankanlah aku
mencintaiMu, melalui selembar dua lembar tilawah harianku. Lewat
lantunan seayat dua ayat hafalanku.

Yaa Rahiim

Aku tak sanggup mencintaiMu semisal Sumayyah, yang mempersembahkan jiwa
demi tegaknya DienMu. Seandai para syuhada, yang menjual dirinya dalam
jihadnya bagiMu. Maka perkenankanlah aku mencintaiMu dengan
mempersembahkan sedikit bakti dan pengorbanan untuk dakwahMu. Maka
izinkanlah aku mencintaiMu dengan sedikit pengajaran bagi tumbuhnya
generasi baru.

Allahu Kariim, aku tak sanggup mencintaiMu di atas segalanya, bagai
Ibrahim yang rela tinggalkan putra dan zaujahnya, dan patuh mengorbankan
pemuda biji matanya. Maka izinkanlah aku mencintaiMu di dalam segalanya.
Izinkan aku mencintaiMu dengan mencintai keluargaku, dengan mencintai
sahabat-sahabatku, dengan mencintai manusia dan alam semesta.

Allaahu Rahmaanurrahiim, Ilaahi Rabbii

Perkenankanlah aku mencintaiMu semampuku. Agar cinta itu mengalun dalam
jiwa. Agar cinta ini mengalir di sepanjang nadiku.

note: gak tau dodi dapat dri mana
