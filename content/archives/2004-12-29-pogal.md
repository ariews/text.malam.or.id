---
date: "2004-12-29T08:53:00+07:00"
title: "POGAL"
tags:
    - Humor
    - Friends
    - Email
---

[pogal](http://malam.or.id/pogal.php) (apaan tuh?) adalah poto
gallery, apa aja.. yang ada di kompi client, poto anak2.. yang nurut gw
bagus :).

Pogal ini di bikin karena ada plugin yang baguuuuuuuuuusssss dari
[flickr
pugins](http://www.worrad.com/archives/2004/11/30/flickr-gallery-wp-plugin)
nya [ray](http://www.worrad.com). Flickr plugin ini pake
[API](http://flickr.com/services/api/misc.api_keys.html) dari
[flickr](http://www.flickr.com)

Dulu pernah make tapi [nyatanya
gagal]({{< ref path="2004-12-04-flickr-gallery.md">}}) lalu
setelah menghubungi ray lewat comment, gw di suruh UP ke versi
[0.6](http://www.worrad.com/archives/2004/11/30/flickr-gallery-wp-plugin).
Gw berharap banyak bisa make, tapi nyatanya [masih error
juga](http://tadulako.co.id/forum/viewtopic.php?t=170).

Hingga 2 hari lalu gw UP wp nya ke
[1.5-alpha-6](http://wordpress.org/nightly/) dari cvs. Karena masih
penasaran, coba2 brows ke pogal. Eh ternyata nongol dia,... potonya!!!

Asli deh bangus banget.. HIGHLY RECOMMENDED

**UPDATE**: [pogal](http://ezie.spunge.org/pogal.php/)
