--- 
date: "2011-11-28T15:17:00+07:00"
title: "Ngeblog lagi dengan Kohana 3.2"
tags:
    - Kohana
    - Kohana 3.2
---

Setelah sekian lama, gw akhirnya coba pake [Kohana
3.2](http://kohanaframework.org/), komentar temen-temen semuanya
[dipindahin](http://disqus.com/admin/tools/import/platform/generic/) ke
[Disqus](http://disqus.com/).

Ada banyak yang mau gw posting di sini, terutama soal perjalanan blog gw
ini tentunya :)
