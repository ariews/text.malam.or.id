---
title: "carpe diem"
date: "2004-06-07T20:55:00+07:00"
---

Apakah butiran itu -- yang tertiup badai

akan kembali ke tempat semula?

Kita pernah seperti lingkaran setan, berjalan di jalan kita, lalu
membelok kembali lagi, dan

berbelok lagi. Sungguh melelahkan.

Di Buya s, kita bertemu.

Lingkaran ini mesti terputus. Aku,

kamu juga mereka sudah tau.

Seperti kerikil tadi, yang ada di dasar sungai terdalam. Berharap
menjadi besar.

Berharap ...

Dalam kesunyian jiwa: "Aku akan kembali" katanya dalam hati. "Aku akan
kembali" dia berucap.

"Aku akan kembali" teriaknya.
