--- 
date: "2017-05-16T22:52:44+07:00"
title: "Pelican to Hexo"
tags:
    - Pelican
    - Hexo
    - reStructuredText
---

Setelah bertahun-tahun ngeblog pake [Pelican]({{< ref path="2014-11-07-ke-pelican.md">}}),
itu pun banyak idle-nya, sebab gak nyempetin buat nulis.

Nah sekarang isengn pengen ngidupin lagi, tapi sayangnya, Pelican udah
lama gak aktif developmenya. *Ehh ternyata bukan karena gak aktif, tapi
pelican gwnya aja yg gak bener, selalu error `JINJA2_EXTENSIONS` gitu,
dan gak beres-beres masalahnya*.

Setelah nyari sana-sini, akhirnya pake [Hexo](http://hexo.io), tapi
karena pas pake Pelican gw pake format `reStucturedText`, jadi ga bisa
langsung ganti engine, kudu converting dulu.

Berikut format lama gw:

```text
====
test
====

:date: 2003-08-21 09:50
:tags: Myself
:category: Blog
:slug: posts/read/1/test

ezie :p~
```

setelah diupdated menjadi:

```text
----------------------------
title: test
date: 2003-08-21 09:50
tags:
  - Myself
----------------------------

ezie :p~
```

Lebih terlihat bedanya antara meta sama content.

Cuman sayangnyan, masih belum puas untuk settingan _:slug:_ atau _permalink_.

Kudunya sih bisa di`override`, tapi nyatanya ga :| , misal `config.yml`:

```text
permalink: :year/:month/:day/:title/
```

lalu file .rst kita, kasih tambahan:

```text
----------------------------
title: test
date: 2003-08-21 09:50
tags:
  - Myself
permalink: you-rock
----------------------------

ezie :p~
```

Harusnya permalink jadi:

```text
/you-rock/
```

Kenyataannya gak, permalink malah jadi:

```text
/203/08/21/you-rock
```

Ini gw yg belum tau atau emang kek gitu?

Karena gak bisa, maka semua disqus comment yg sebelumnya jadi ilang,
pasti ada cara settingan yg pas buat update permalink..

Juga untuk tags, gak bisa pake (atau gw belom tau caranya)
comma-separated, kudu dilist seperti di atas.
