--- 
date: "2010-11-14T01:48:00+07:00"
title: "Relation has_one dengan Jelly ORM"
tags:
    - Kohana 3.2
    - Jelly ORM
    - Tutorial
    - Kohana 3
---

Kita telah belajar membuat class untuk `Model_Post` di postingan
sebelumnya, sekarang kita mau belajar membuat relasi has_one dengan
`Jelly`.

Oh ya.. contoh2 berikut ini adalah migrasi dari ORM yang [udah gw post
sebelumnya]({{< ref path="2009-06-11-kohana-orm-2.md">}}).

`has_one`, post `has_one` status

kita tulis untuk Model_Status

```php
// classes/model/status.php
class Model_Status extends Jelly_Model {
  public static function initialize(Jelly_Meta $meta) {
    $meta
      ->fields(array(
        'id' => new Field_Primary,
        'name' => new Field_String(array(
          'unique' => true,
        ))
      ));
  }
}
```

lalu untuk `Model_Post`, mirip sepertya yang sebelumnya, hanya saja ada
yg berubah.

```php
// classes/model/post
class Model_Post extends Jelly_Model
{
  public static function initialize(Jelly_Meta $meta)
  {
    $meta->fields(array(
      'id' => new Field_Primary,
      'title' => new Field_String,
      'status' => new Field_HasOne,
    ));
  }
}
```

Relasi `has_one` dibentuk saat kita initialisasi.

Kita lihat statement `'status' => new Field_HasOne`. `status` adalah
alias, bukan nama field, begitu juga dengan `id` dan `title`. `Jelly`
melihat jenis fieldnya, karena `Field_HasOne` maka `Jelly` akan merujuk ke
`Model_Status`, dan secara default, akan dianggap menggunakan field:
`alias_id`, berarti pada table posts harus ada `status_id`, dan id pada
table statuses.

Sekarang post sudah mempunyai relasi `has_one`.

```php
$post = Jelly::factory('post');
$post->title:'new Title';
$post->status = Jelly::factory('status', 1);
$post->save();
```
