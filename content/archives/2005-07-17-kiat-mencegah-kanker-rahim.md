---
title: "Kiat mencegah Kanker Rahim"
date: "2005-07-17T00:41:00+07:00"
---

![kids.jpg](http://x3.freeserverhost.net/seepicts.php?img=/kids.jpg)

Banyak cara, misalnya, tak terlalu sering mencuci vagina dengan
antiseptik, apalagi tanpa indikasi dan saran dokter. Jangan pula
menaburkan talk di vagina. Bisa juga dengan diet rendah lemak. Waduh!
Kengerianlah yang langsung terbayang begitu mendengar kata kanker rahim.

Kita tahu penyakit ganas ini menduduki peringkat atas sebagai pembawa
kematian. Tapi, tak perlu khawatir bila sejak awal kita sudah melakukan
pencegahan. Karena justru, menurut dr.Nasdaldy, SpOG, , pencegahan
menjadi bagian terpenting dari risiko kanker. "Caranya dengan mencegah
terpaparnya substansi yang menyebabkan risiko terjadinya kanker
tersebut," tandasnya. Yang terjadi di sini justru sebaliknya, masih
banyak wanita yang enggan memeriksakan diri ke dokter kandungan, kendati
sudah memiliki berbagai keluhan. Padahal, jika dibiarkan kanker akan
semakin mengganas!

Jadi, yuk, kita ikuti sejumlah kiat mencegah kanker rahim yang
dipaparkan ahli kebidanan dan kandungan dari RS Kanker Darmais,
Jakartaini.

1.  **JAUHI ROKOK**

    Ini peringatan paling penting buat wanita perokok. Kecuali
    mengakibatkan penyakit pada paru-paru dan jantung, kandungan nikotin
    dalam rokok pun bisa mengakibatkan kanker serviks (leher rahim),
    lho. "Nikotinkan, mempermudah semua selaput lendir sel-sel tubuh
    bereaksi atau menjadi terangsang, baik pada mukosa tenggorokan,
    paru-paru, juga serviks. " Sayangnya tak diketahui pasti seberapa
    banyak jumlah nikotin dikonsumsi yang bisa menyebabkan kanker
    serviks. Tapi, mengapa harus ambil risiko, lebih baik tinggalkan
    segera rokok jika kita ingin terbebas dari kanker.

2.  **PENCUCIAN VAGINA**

    Sering, kan, kita melakukan pencucian vagina dengan obat-obatan
    antiseptik tertentu. Alasannya beragam, entah untuk "kosmetik" atau
    kesehatan. Padahal, kebiasaan mencuci vagina bisa menimbulkan kanker
    serviks, baik obat cuci vagina antiseptik maupun deodoran. "Douching
    atau cuci vagina menyebabkan iritasi di serviks.

    Nah, iritasi berlebihan dan terlalu sering akan merangsang
    terjadinya perubahan sel, yang akhirnya jadi kanker." Jadi,
    sebaiknya pencucian vagina dengan bahan-bahan kimia tak dilakukan
    secara rutin. "Kecuali bila ada indikasi, misalnya, infeksi yang
    memang memerlukan pencucian denganzat-zat kimia. Itu pun seharusnya
    atas saran dokter." Artinya, kita jangan sembarangan membeli
    obat-obatan pencuci vagina. "Terlebih lagi,pembersih tersebut
    umumnya akan membunuh kuman-kuman. Termasuk kuman Basillus doderlain
    di vagina yang memproduksi asam laktat untuk mempertahankan pH
    vagina."

    Kita tahu, bila pH enggak seimbang lagi di vagina, maka kuman lain,
    seperti jamur dan bakteri, bisa punya kesempatan hidup di tempat
    tersebut. Ini, kan, malah bisa menimbulkan penyakit-penyakit lain.

3.  **MENABURI TALK**

    Yang kerap terjadi lagi, saat daerah vagina gatal atau merah-merah,
    kita menaburkan talk di sekitarnya. Walah, ternyata itu bahaya.
    Pemakaian talk pada vagina wanita usia subur bisa memicu terjadi
    kanker ovarium (indung telur). Sebab di usia subur berarti sering
    ovulasi. Padahal bisa dipastikansaat ovulasi terjadi perlukaan di
    ovarium.

    Nah, bila partikel talk masuk akan menempel di atas luka tersebut.
    Akibatnya, kan, bisa merangsang bagian luka untuk berubah sifat jadi
    kanker." Karena itu sangat tidak dianjurkan memberi talk di daerah
    vagina. Karena dikhawatirkan serbuk talk terserap masuk ke dalam.
    Lama-lama akan bertumpuk dan mengendap menjadi benda asing yang bisa
    menyebabkan rangsangan sel menjadi kanker.

4.  **DIET RENDAH LEMAK**

    Penting diketahui, timbulnya kanker pun berkaitan erat dengan pola
    makan seseorang. Wanita yang banyak mengkonsumsi lemak akan jauh
    lebih berisiko terkena kanker endometrium (badan rahim). "Sebab
    lemak memproduksi hormon estrogen. Sementara endometrium yang sering
    terpapar hormon estrogen mudah berubah sifat menjadi kanker." Jadi,
    terang Nasdaldy, untuk mencegah timbulnya kanker endometrium,
    sebaiknya hindari mengkonsumsi makanan berlemak tinggi. "Makanlah
    makanan yang sehat dan segar.

    Jangan lupa untuk menjaga berat badan ideal agar tak terlalu gemuk."
    Tak heran, bila penderita kanker endometrium banyak terdapat di
    kota-kota besar negara maju. Sebab, umumnya mereka menganut pola
    makan tinggi lemak.

5.  **KEKURANGAN VITAMIN C**

    Pola hidup mengkonsumsi makanan tinggi lemak pun akan membuat orang
    tersebut melupakan zat-zat gizi lain, seperti beta karoten, vitamin
    C, dan asal folat. Padahal, kekurangan ketiga zat gizi ini bisa
    menyebabkan timbul kanker serviks. "Beta karoten, vitamin C, dan
    asam folat dapat memperbaiki atau memperkuat mukosa diserviks. Nah,
    jika kekurangan zat-zat gizi tersebut akan mempermudah rangsangan
    sel-sel mukosa tadi menjadi kanker." Beta karoten banyak terdapat
    dalam wortel, vitamin C terdapat dalam buah-buahan berwarna oranye,
    sedangkan asam folat terdapat dalam makanan hasil laut.

6.  **HUBUNGAN SEKS TERLALU DINI**

    Hubungan seks idealnya dilakukan setelah seorang wanita benar-benar
    matang. Ukuran kematangan bukan hanya dilihat dari ia sudah
    menstruasi atau belum, lo. Tapi juga bergantung pada kematangan
    sel-sel mukosa; yang terdapat di selaput kulit bagian dalam rongga
    tubuh. Umumnya sel-sel mukosa baru matang setelah wanita tersebut
    berusia 20 tahun ke atas. Jadi, seorang wanita yang menjalin
    hubungan seks pada usia remaja; paling rawan bila dilakukan di bawah
    usia 16 tahun. Hal ini berkaitan dengan kematangan sel-sel mukosa
    pada serviks si wanita. "Pada usia muda, sel-sel mukosa pada serviks
    belum matang. Artinya, masih rentan terhadap rangsangan sehingga tak
    siap menerima rangsangan dari luar. Termasuk zat-zat kimia yang
    dibawa sperma."

    Lain hal bila hubungan seks dilakukan kala usia sudah di atas 20
    tahun, dimana sel-sel mukosa tak lagi terlalu rentan terhadap
    perubahan. Nah, karena masih rentan, sel-sel mukosa bisa berubah
    sifat menjadi kanker. "Sifat sel, kan, selalu berubah setiap saat;
    mati dan tumbuh lagi.

    Karena ada rangsangan, bisa saja sel yang tumbuh lebih banyak dari
    sel yang mati, sehingga perubahannya tak seimbang lagi. Kelebihan
    sel ini akhirnya bisa berubah sifat menjadi sel kanker."

7.  **BERGANTI-GANTI PASANGAN**

    Bisa juga kanker serviks muncul pada wanita yang berganti-ganti
    pasangan seks. "Bila berhubungan seks hanya dengan pasangannya, dan
    pasangannya pun tak melakukan hubungan seks dengan orang lain, maka
    tidak akan mengakibatkan kanker serviks." Bila berganti-ganti
    pasangan, hal ini terkait dengan kemungkinan tertularnya penyakit
    kelamin, salah satunya Human Papilloma Virus (HPV). "Virus ini akan
    mengubah sel-sel di permukaan mukosa hingga membelah menjadi lebih
    banyak. Nah, bila terlalu banyak dan tidak sesuai dengan kebutuhan,
    tentu akan menjadi kanker."

8.  **TERLAMBAT MENIKAH**

    Sebaliknya wanita yang tidak atau terlambat menikah pun bisa
    berisiko terkena kanker ovarium dan kanker endometrium. Sebab,
    golongan wanita ini akan terus-menerus mengalami ovulasi tanpa jeda.
    "Jadi, rangsangan terhadap endometrium pun terjadi terus-menerus.
    Akibatnya bisa membuat sel-sel di endometrium berubah sifat jadi
    kanker."

    Risiko yang sama pun akan dihadapi wanita menikah yang tidak mau
    punya anak. Karena ia pun akan mengalami ovulasi terus-menerus.
    "Bila haid pertama terjadi di bawah usia 12 tahun, maka paparan
    ovulasinya berartiakan semakin panjang. Jadi, kemungkinan
    terkenakanker ovarium akan semakin besar."

    Nah,salah satu upaya pencegahannya tentu dengan menikah dan hamil.
    Atau bisa juga dilakukan dengan mengkonsumsi pil KB. Sebab
    penggunaan pil KB akan mempersempit peluang terjadinya ovulasi.
    "Bila sejak usia 15 tahun hingga 45 tahun dia terus menerus ovulasi,
    lantas 10 tahun ia ber-KB, maka masa ovulasinya lebih pendek
    dibandingkan terus-menerus, kan?" Hasil penelitian menunjukkan
    penggunaan pil KB sebagai alat kontrasepsi dapat menurunkan kejadian
    kanker ovarium sampai 50 persen.

9.  **PENGGUNAAN ESTROGEN**

    Risiko yang sama akan terjadi pada wanita yang terlambat menopause.
    "Karena rangsangan terhadap endometrium akan lebih lama, sehingga
    endometriumnya akan lebih sering terpapar estrogen. Jadi, sangat
    memungkinkan terjadi kanker." Tak heran bila wanita yang memakai
    estrogen tak terkontrol sangat memungkinkan terkena kanker. "Umumnya
    wanita yang telah menopause di negara maju menggunakan estrogen
    untuk mencegah osteroporosis dan serangan jantung."

    Namun, pemakaiannya sangat berisiko karena estrogen merangsang
    semakin menebalnya dinding endometrium dan merangsang sel-sel
    endometrium sehingga berubah sifat menjadi kanker. "Jadi, sebaiknya
    penggunaan hormon estrogen harus atas pengawasan dokter agar
    sekaligus juga diberikan zat antinya, sehingga tidak berkembang jadi
    kanker."

Nah, banyak hal ternyata yang bisa dilakukan agar tak "mengundang"
kanker datang ke tubuh kita. Tentu saja kita bisa memulainya dari
hal-hal kecil. Jangan tunda sampai esok!

dari milis smunsasukabumi
