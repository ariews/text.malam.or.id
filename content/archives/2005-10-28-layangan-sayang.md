---
title: "Layangan, sayang"
date: "2005-10-28T17:45:00+07:00"
---

![Layangan](/images/50296435e0d40layangan.jpg)

Bagaikan bermain layangan, hanya bisa memegang sedikit dari yang besar.

Bagaikan Bermain layangan, mengendalikannya tidak melihat tali yg kita
pegang.

Bermain layangan, terasa lebih berat pegangan kita.

Sayang jika kalah.

Bermain layangan, penuh kepercayaan antara mata, otak, hati dan tangan.

Jika kita kalah, akan terasa banyak pengorbanan.

Jangan sampai kalah sayang.
