--- 
date: "2009-03-27T00:39:00+07:00"
title: "Pink Floyd: Mother"
slug: "pink-floyd-mother"
tags:
    - Lyrics
---

Mother do you think they'll drop the bomb? Mother do you think they'll 
like this song? Mother do you think they'll try to break my balls?

![pink floyd, the wall](/images/5029644e0bae0pink_floyd_wall.png)

Ooh ah,

Mother should I build the wall? Mother should I run for President? 
Mother should I trust the government? Mother will they put me in 
the firing mine?

Ooh ah,

Is it just a waste of time

Hush now baby, baby, don't you cry. Mama's gonna make all your 
nightmares come true. Mama's gonna put all her fears into you.
Mama's gonna keep you right here under her wing. She won't let 
you fly, but she might let you sing. Mama's gonna keep baby cozy
and warm. Ooh baby, ooh baby, ooh baby.

Of course mama's gonna help build the wall

Mother do you think she's good enough? For me? Mother do you think 
she's dangerous. To me? Mother will she tear your little boy apart?
Ooh ah

Mother will she break my heart?

Hush now baby, baby don't you cry. Mama's gonna check out all your 
girlfriends for you. Mama won't let anyone dirty get through.
Mama's gonna wait up until you get in. Mama will always find out
where you've been. Mama's gonna keep baby healthy and clean.
Ooh baby, ooh baby, ooh baby

You'll always be baby to me

Mother, did it need to be so high?

[Pink Floyd, The Wall,
Mother](http://www.google.com/search?q=pink+floyd+the+wall+mother+lyrics)
