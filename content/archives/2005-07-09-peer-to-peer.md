---
title: "peer to peer"
date: "2005-07-09T00:49:00+07:00"
---

Bagi kalian yang menyukai program peer to peer chatand file sharing
(PrivaShare), sepertinya mesti menambah kewaspadaan atau di update
buru2.

Eh tapi keknya blom ada updatenya ya?

**DESCRIPTION:**

> basher13 has discovered a vulnerability in PrivaShare, which can be
> exploited by malicious people to cause a DoS (Denial of Service).
>
> The vulnerability is caused due to an error when processing malformed
> data received during a connection request. This can be exploited to
> crash the application.
> 
> The vulnerability has been confirmed in version 1.3. Prior versions may
> also be affected.

**SOLUTION:**

> Filter access so that only trusted IP addresses can connect to the
> service.
