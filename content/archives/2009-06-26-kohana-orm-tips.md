--- 
date: "2009-06-26T05:41:00+07:00"
title: "Kohana ORM Tips"
tags:
    - Tutorial
    - Kohana
---

Kalo kita pernah baca [manual kohana](http://docs.kohanaphp.com/), terus
kita liat ke bagian [ORM](http://docs.kohanaphp.com/libraries/orm), di
sana ada contoh seperti berikut:

```php
$user->add(ORM::factory('role', 'admin'));
```

nah, `ORM::factory('role', 'admin')`, mestinya kalo kita coba itu gak
bisa ya, karena ORM akan mencoba untuk mencari berdasarkan `unique_field`
nya (pada contoh sebelumnya, field id adalah unique).

Nah skr, gw akan coba trik ini.

Pada table posts yang lalu, kita coba tambahin field `slug`, sehingga
tabelnya menjadi:

| id | title     | status_id | slug    |
|---:|:----------|----------:|:--------|
| 1  | title #1  | 3         | title-1 |
| 2  | title #2  | 1         | title-2 |
| 3  | title #2  | 2         | title-3 |

Tentu dengan mudah kita bisa membuat `title-1` dan yang lainnya
menggunakan helper `url::title()`, tapi sebagai catatan, field slug ini
**harus unique** juga, sebagaimana field id.

Kemudian, pada `Post_Model` class, kita tambah sebuah method:

```php
// models/post.php
class PostModel extends ORM
{
  ...
  public function uniquekey($id)
  {
    if (!empty($id) AND isstring($id) AND !ctypedigit($id))
    {
      return 'slug';
    }
    return parent::uniquekey($id);
  }
  ...
}
```

perhatikan baik, nilai yang dikembalikan pada fungsi `unique_key` adalah
`slug` (nama field yang barusan kita tambah di table posts).

```php
public function blog($slug)
{
  $post = ORM::factory('post', $slug);
}
```

Sekarang kita bisa menggunakan `slug` ini sebagai id atau juga sebagai
url. misal kalo di blog gw ini menjadi `blog/kohana-orm-2.php`
