--- 
date: "2010-12-03T14:42:00+07:00"
title: "Kondisi 'BETWEEN' di Jelly"
tags:
    - Kohana
    - Jelly ORM
    - Tutorial
    - Database
    - Kohana 3
    - Kohana Jelly
---

Kadang kita memerlukan kondisi `BETWEEN` pada query ke database, misal
untuk melihat logs tanggal 3 dan tanggal 5. Dengan gampangnya pada MySQL
kita kasih perintah seperti ini:

Maka akan muncul activity log untuk tanggal terserbut.

> Tapi bagaimana melalukan hal itu dengan Jelly, gw koq ga ngeliat ada
> methhod `BETWEEN` yah?

Gak juga, kalo kita liat2 lagi, class `Jelly_Builder` itu merupakan
extends dari class `Database_Query_Builder`, jadi apapun yang kita tulis
setelah `Jelly::select('model')` merupakan method dari sana. Misal:
`Jelly::select('model')->where()`, nah `where` ini merupakan method dari
class `Database_Query_Builder_Where`.

Kalo di urut-urut classnya (dari extends), kita bisa liat ada method yg
namanya `compile_conditions`, lalu di sana ada statement yg mengatakan
bahwa kita bisa pake kondisi `BETWEEN` tadi, jadi emang ga bisa
`Jelly::select('model')->between()` (kecuali kita emang nambahin method
itu), tapi kita bisa dengan cara seperti ini:

Selamat Mencoba :)
