---
title: "Premium Member, horay!"
date: "2005-06-08T00:25:00+07:00"
---

Terus terang, gw gak nyangka kalo gw bisa menang, beruntung sekali!
Thanks [Laila\*](http://www.ladylaila.net/blog/) for the [great
mission](http://blogshares.com/missions.php?list=250).

Ada beberapa kabar lainnya.

Karena terkadang blog ini suka error pada koneksi mysql nya (seperti
yang di [bilang oleh
kusaeni](http://www.malam.or.id/blog/about/#comment-2640)).

Sebetulnya bukan error, tapi terputusnya koneksi mysql dari spunge ke
tadulako. HUm.. sekali lagi... ntar yang keberapa kali, kita pindah
hosting (kembali lagi ke malam.or.id). Ada beberapa perubahan disini,
yang pasti memindahkan file2 yang ada di spunge ke malam.or.id. Lalu
tulis ulang pada file `.htaccess` karena tadi sempet error.

seperti:

```text
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /blog/
RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^.*$ - [S=45]
....
```

tadinya:

```text
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^.*$ - [S=45]
....
```

Sebenernya gak perlu sih ganti `.htaccess`, tapi karena kita skr tidak
di direktori paling atas, maka di gantilah :)

![morid.png](http://x3.freeserverhost.net/seepicts.php?img=/morid.png)
Lalu ada kabar yang membuat gw deg2an, ada email dari root, bahwa space
di malam.or.id hampir habis (sudah terpakai sekitar 98%). Jika tidak di
lakukan penghapusan, maka akan di hapus secara asal oleh admin :roll:
padahal ini baru di upload [wp](http://wordpress.org)nya, kalian tau kan
berapa gede wp? kecil!

Selidik punya selidik, ternyata dari penggunaan email. Maka dengan
postingan ini gw ngasih tau kepada temen2 yang punya account email di
malam.or.id **segera pindahkan data anda, atau saya akan menghapusnya
secara asal!!**.

Kalo kemaren adalah hari ulang tahun ochie, maka sebentar lagi ulang
tahun jadian gw sama dia :D, gw berdoa semoga hubungan gw sama ochie
semakin erat, dan mesra (huhuyyyyyyyyyyyy -ala spontan!)

Sekali lagi gw mengucapkan banyak terima kasih kepada Ochie,
[Laila\*](http://blogshares.com/user.php?id=13413), [Seth
Dillingham](http://blogshares.com/user.php?id=9560) dan teman2 yang lain
(terutama **P** yang tidak mau di sebutkan namanya) atas dukungannya...

Horeeeeeee premiummmmm :D
