--- 
date: "2014-12-11T15:45:00+07:00"
title: "Balik lagi pake Nginx!"
tags:
    - Nginx
    - Monkey
    - Debian
    - uWSGI
---

Wuiidiihh ganti lagi! Sebenernya siapa yg galau sih? webservernya atau
gw? Hahahaha..

Ya ya.. kemaren gw emang udah sukses seting monyet beserta script buat
autoblogged. Tapi ternyata masih [ada
bugs](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=Monkey+HTTP),
terlebih soal
[DDoS](http://en.wikipedia.org/wiki/Denial-of-service_attack#Distributed_attack)
di versi lama [Monkey HTTP](http://monkey-project.com/).

Jadi ya udah lah, gw balik lagi pake nginx, beda memory 1mega doang
keknya gak terlalu signifikan. Dan lagi, nginx sudah menjadi [HTTP
Server](http://en.wikipedia.org/wiki/Web_server) favorit gw ;)

Terus buat `CGI`-nya, gw pake `uWSGI`, yg ternyata instalasinya relative
gampang. Gw ikutin cara [installasi uWSGI di
raspeberry](http://raspberrywebserver.com/cgiscripting/setting-up-nginx-and-uwsgi-for-cgi-scripting.html),
tapi dengan sedikit perbedaan.

Untuk install nginx, gw pake `apt-get` aja:

```text
# apt-get install nginx
# apt-get install uwsgi uwsgi-plugin-cgi
```

Lalu dibagian server config:

```text
location /cgi/ {
    autoindex       off;
    gzip            off;
    include         uwsgi_params;
    uwsgi_modifier1 9;
    uwsgi_pass      127.0.0.1:2014;
}
```

dan aplikasi untuk `uWSGI`:

```ini
[uwsgi]
    plugins = cgi
    socket = 127.0.0.1:2014
    cgi = /cgi=/var/www/arie.malam.or.id/cgi
    cgi-allowed-ext = .cgi
    cgi-allowed-ext = .pl
```

jadi deh, gw tinggal panggil
[scriptnya](http://arie.malam.or.id/cgi/hello.cgi) seperti ini:

```text
$ curl -v http://arie.malam.or.id/cgi/hello.cgi
* Hostname was NOT found in DNS cache
*   Trying 185.53.129.214...
*   Trying 2a02:2ca0:aaa::86c0:94de...
* Connected to arie.malam.or.id (185.53.129.214) port 80 (#0)
> GET /cgi/hello.cgi HTTP/1.1
> User-Agent: curl/7.35.0
> Host: arie.malam.or.id
> Accept: */*
>
< HTTP/1.1 200 OK
* Server nginx/1.6.2 is not blacklisted
< Server: nginx/1.6.2
< Date: Thu, 11 Dec 2014 08:59:59 GMT
< Content-Type: text/html
< Transfer-Encoding: chunked
< Connection: keep-alive
<
Hello, world!
* Connection #0 to host arie.malam.or.id left intact
```

Sukses!!
