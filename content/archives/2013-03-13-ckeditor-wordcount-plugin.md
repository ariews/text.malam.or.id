--- 
date: "2013-03-13T20:26:00+07:00"
title: "CKeditor Wordcount plugin"
tags:
    - Markdown
    - CKEditor
    - Javascript
---

Baru sekarang gw pake plugin
[wordcount](http://www.n7studios.co.uk/2010/03/01/ckeditor-word-count-plugin/)
buat [CKeditor](http://ckeditor.com/), si plugin ini digunakan buat
ngitung jumlah kata yang ditulis pada `textarea`, hanya saja pada
kondisi-kondisi tertentu, si plugin gak bekerja semestinya.

Dari code si plugin:

dia hanya akan membuat sebuah HTML string menjadi text biasa, dan
di-`split` berdasarkan spasi. Tab, dan garis baru tidak akan dihitung.

Nah, gw coba-coba benerin code tersebut, menjadi seperti ini:

Pertama-tama, gw akan coba ngeganti semua tab, garis baru dan spasi
menjadi spasi tunggal, lalu hilangkan HTML tag-nya dengan menggunakan
fungsi [.text()](http://api.jquery.com/text/), ternyata masih
menghasilkan beberapa spasi kosong, makanya gw perlu untuk --sekali
lagi-- mengganti menjadi spasi tunggal.

Lalu gw split berdasarkan spasi, dan gw hitung jumlahnya. Jadi dehh!!

Haha, gw tau, itu kode yang buruk! tapi berfungsi seperti apa yg gw
harapkan.
