---
title: "(mIRC) mOTFv3.dll"
date: "2006-10-01T19:30:00+07:00"
tags:
    - Tutorial
---

sebenernya udah lama....

```text
; mOTFv3.dll
; URL: http://mircscripts.org/comments.php?cid=169

alias mo.dll {
  ; mOTFv3.dll
  ; disini: $mircdirdllmotfv3.dll
  ; jadi kalo mirc ada di folder C:Program FilesmIRC
  ; maka file dll ada di: C:Program FilesmIRCdllmOTFv3.dll
  return $shortfn( [ $mircdirdll $+ mOTFv3.dll ] )
}

alias moload {
  ; alias load $mo.dll
  dll $mo.dll motfv Load
}

alias moconn {
  ; Sync $mo.dll saat kita connect ke IRC Server
  dll $mo.dll motfv Sync
}

On *:START:{
  ; load $mo.dll
  moload
}

RAW *:*:{
  var %raw.num = $right(00 $+ $numeric,3)
  ; sebetulnya sih On *:CONNECT:{}
  ; tapi karena On *:CONNECT:{} itu setelah selesai loading server,
  ; dan buka ketika kita benar2 konek ke server, di pilih RAW 001
  ; untuk Sync dengan $mo.dll
  if (%raw.num == 001) { moconn }
}

Ctcp ^*:MOTFV:{
  ; reply CTCP VERSION
  var %reply = electro HOMOK
  .ctcpreply $nick VERSION %reply
  haltdef
}
```

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
