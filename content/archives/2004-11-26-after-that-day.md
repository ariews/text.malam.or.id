---
title: "After That day!"
date: "2004-11-26T08:58:00+07:00"
---

Gw kemaren kan ujian CPNS yang di Lembaga Pemasyarakatan Sukamiskin
(LAPAS) itu, tanggal 24 sampe 25 Nove. Yang 24 sih ujian tulis.. lancar
aja.. masuk jam 7 sampe jam 12-an.

Nah yang maren.., yang 25.. ujian fisik, gak ganas sih.. cuman LARI,
KESEHATAN (check tatoo, tindik, kulit.. dan di telanjangin!!), PBB
(baris berbaris.. kek militer) dan WAWANCARA.

**Diluar dugaan**

Gw kira jarak tempuh test lari pendek aja, cuman 2 belokan aja.
nyatanya... 4 kali dari itu. duh mana jarang lari pula gw nya. napas
langsung *senen kemis*, capek.. pusing.. mana blom makan lagih. urutan
ke delapan :(, kata panitia yang *ngetest* sih yang di nilai adalah cara
berlari, bukan cepat atau lambatnya kita berlari.

Ada orang yang di sebalah gw bilang:

> Iya.. cara berlari, tapi kan kalo caranya gak bener kita gak bakalan
> bisa sampe tujuan dengan cepet

Tapi ada juga yang bilang:

> Ya gak juga sih, dia bisa aja bener larinya, orang yang pertama sampe
> di garis finish kebetulan aja staminanya bagus, (atau kuat) tapi kalo
> jaraknya lebih jauh.. kan bisa aja yang belakang sampe di tujuan lebih
> cepet

Yang laennya:

> Yang sampe tujuan adalah yang bagus!

Gak tau juga sih gw, lagi pula.. gw blom pernah belajar ILMU LARI, cuman
yang pasti..... kaki gw pegel2 abis itu, pala pusing, lemes.. gak konsen
jadinya.

**Silahkan buka baju anda**

Sesudah berlari2 (gw ngelakuinnya dengan terpaksa, gitu juga dengan yang
laen), kita semua di test kesehatan, kata yang *mmipin* test:

> Saya bukan tidak percaya dengan surat kesehatan yang anda semua
> lampirkan pada saat pendaftaran di DEPKEH & HAM, tapi saya hanya ingin
> membuktikan bahwa surat anda benar! Jadi... nanti di sana anda akan di
> liat sama dokter kita, apakah anda layak atau tidak?

Gw kira, kita cuman bakalan di liat. hidung dan telinga (untuk check
tindik) dan punggung (untuk check tatoo) aja, ternyata lebih dari
itu.Dokter menyuruh kita buat bugil2an.

Beuh...

**Baris berbaris & Wawancara**

Ini adalah *session* yang paling menjengkelkan, gmn gak? menghabiskan
**lebih dari 6 jam!!!**. kita hanya di test baris-berbaris, dan di akhir
test di tanya satu-persatu tentang diri kita.

> Anda pernah ikut beladiri? Tahun berapa anda lulus SMA? Akademik lain
> selain SMA? Sudah berkeluarga? Sekarang tinggal di mana? Sudah
> bekerja? Ikut PASKIBRA? Keahlian lain?

Itu hanya sebagian kecil pertanyaan yang di lontarkan petugas penanya.

Semua dah lewat, sekarang gw lagi nahan pegel di kaki yang belom ilang2
:(, gini deh karena gw jarang olahraga
