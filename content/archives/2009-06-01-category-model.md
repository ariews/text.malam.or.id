--- 
date: "2009-06-01T23:42:00+07:00"
title: "Category Model"
tags:
    - Tutorial
    - Kohana
---

Ok, kemaren coba2 bikin model buat category, kasusnya mungkin dah sering
kita temuin. Misal gini:

`X` mempunyai subcat `A`,`B`,`C`. `A` mempunyai subcat `A1`, `A2`. Lalu
`C` mempunyai sub `C2` dan `C4`[^1].

Pada model, kita pengen tau child dari `X`, kita bisa pake:

```php
ORM::factory('category', X)->find_child();
```

atau kita bisa juga memindahkan category menjadi subcategory yang laen.

```php
ORM::factory('category', A1)->move_to(C);
```

sehingga skr child dari `C` adalah `C2`, `C4` dan `A1`.

berikut adalah beberapa method yang tersedia:

-   `move_to($id)`
-   `level_up()`
-   `find_parent()`
-   `find_child()`
-   `is_my_child($id)`
-   `is_my_parent($id)`

Category Model ini menggunakan Table seperti berikut:

```sql
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) default NULL,
  `shortname` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `shortname` (`shortname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
```

[^1]:  `X`, `A`, `B`, `C`, `A1`, `A2`, `C2` dan `C4` adalah `id`
