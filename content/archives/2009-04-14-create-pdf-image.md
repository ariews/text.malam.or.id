--- 
date: "2009-04-14T23:14:00+07:00"
title: "Create PDF Image"
tags:
    - Tutorial
    - PHP
---

Kadang kita pengen nampilin file PDF dengan covernya, ada berbagai cara
untuk nampilin itu, misalnya kita buka pake PDF Reader, lalu ambil
gambar halaman depannya terus upload, link ke file PDFnya. Tapi itu
ribet kalo kita punya banyak file PDF.

Di PHP kita bisa menggunakan Imagick untuk itu.

```php
<?php

$PDF_File = '/home/arie/PDF/The_Ultimate_CSS_Reference_Feb_2008.pdf';

function imagic_test_save_cover($PDF_File)
{
    if (class_exists('Imagick')) {
        $dirname        = dirname($PDF_File);
        $filename       = pathinfo($PDF_File, PATHINFO_BASENAME);
        $cover          = '%s.jpg';
        $cover_magick   = new Imagick("{$PDF_File}[0]");
        $cover_magick->setImageFormat('jpg');
        $h              = (50*$cover_magick->getImageHeight())/100;
        $w              = (50*$cover_magick->getImageWidth())/100;
        $cover_magick->scaleimage($w, $h);
        $cfilename      = $dirname .'/' . sprintf($cover, $filename);
        if ($fp = @fopen($cfilename, 'w+')) {
            flock ($fp, LOCK_EX );
            fwrite($fp, $cover_magick);
            flock ($fp, LOCK_UN );
            fclose($fp);
        }
    }
}

imagic_test_save_cover($PDF_File);
```
