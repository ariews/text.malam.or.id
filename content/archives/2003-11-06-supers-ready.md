---
title: "super's ready"
date: "2003-11-06T01:33:00+07:00"
tags:
    - Lyrics
---

![image0](http://img51.exs.cx/img51/8779/fox_label.jpg)

## Lovers' leap

Walking across the sitting-room, i turn the television off. Sitting
beside you, i look into your eyes. As the sound of motor cars fades in
the night time, I swear i saw your face change, it didn't seem quite
right. ...and it's hello babe with your guardian eyes so blue

Hey my baby don't you know our love is true.

Coming closer with our eyes, a distance falls around our bodies. Out in
the garden, the moon seems very bright, Six saintly shrouded men move
across the lawn slowly. The seventh walks in front with a cross held
high in hand. ...and it's hey babe your supper's waiting for you.

Hey my baby, don't you know our love is true.

I've been so far from here, Far from your warm arms. It's good to feel
you again,

It's been a long long time. hasn't it?

## The guaranteed eternal sanctuary man

I know a farmer who looks after the farm. With water clear, he cares for
all his harvest.

I know a fireman who looks after the fire.

You, can't you see he's fooled you all. Yes, he's here again, can't you
see he's fooled you all. Share his peace, Sign the lease. He's a
supersonic scientist, He's the guaranteed eternal sanctuary man. Look,
look into my mouth he cries, And all the children lost down many paths,
I bet my life you'll walk inside Hand in hand, Gland in gland With a
spoonful of miracle, He's the guaranteed eternal sanctuary. We will rock
you, rock you little snake,

We will keep you snug and warm.

## Ikhnaton and itsacon and their band of merry men

Wearing feelings on our faces while our faces took a rest, We walked
across the fields to see the children of the west, But we saw a host of
dark skinned warriors Standing still below the ground,

Waiting for battle.

The fight's begun, they've been released. Killing foe for peace...bang,
bang, bang. bang, bang, bang... And they're giving me a wonderful
potion, 'cos i cannot contain my emotion. And even though i'm feeling
good,

Something tells me i'd better activate my prayer capsule.

Today's a day to celebrate, the foe have met their fate.

The order for rejoicing and dancing has come from our warlord.

![image1](http://img51.exs.cx/img51/6549/Gen-Fox.jpg)

## How dare i be so beautiful?

Wandering through the chaos the battle has left, We climb up a mountain
of human flesh, To a plateau of green grass, and green trees full of
life. A young figure sits still by a pool, He's been stamped "human
bacon" by some butchery tool. (he is you) Social security took care of
this lad. We watch in reverence, as narcissus is turned to a flower.

A flower?

## Willow farm

If you go down to willow farm, To look for butterflies, flutterbyes,
gutterflies Open your eyes, it's full of surprise, everyone lies, Like
the fox on the rocks, And the musical box. Oh, there's mum & dad, and
good and bad,

And everyone's happy to be here.

There's winston churchill dressed in drag, He used to be a british flag,
plastic bag, what a drag. The frog was a prince, the prince was a brick,
the brick was an egg, The egg was a bird. (fly away you sweet little
thing, they're hard on your tail) Hadn't you heard? (they're going to
change you into a human being!) Yes, we're happy as fish and gorgeous as
geese,

And wonderfully clean in the morning.

We've got everything, we're growing everything, We've got some in We've
got some out We've got some wild things floating about Everyone, we're
changing everyone, You name them all, We've had them here,

And the real stars are still to appear.

All change!

Feel your body melt, Mum to mud to mad to dad Dad diddley office, dad
diddley office,

You're all full of ball.

Dad to dam to dum to mum Mum diddley washing, mum diddley washing,

You're all full of ball.

Let me hear you lies, we're living this up to the eyes.
Ooee-ooee-ooee-oowaa

Momma i want you now.

And as you listen to my voice To look for hidden doors, tidy floors,
more applause. You've been here all the time, Like it or not, like what
you got, You're under the soil (the soil, the soil), Yes, deep in the
soil (the soil, the soil, the soil, the soil!). So we'll end with a
whistle and end with a bang

And all of us fit in our places.

## Apocalypse in 9/8 (co-starring the delicious talents of gabble
ratchet)

With the guards of magog, swarming around, The pied piper takes his
children underground. Dragons coming out of the sea, Shimmering silver
head of wisdom looking at me. He brings down the fire from the skies,
You can tell he's doing well by the look in human eyes. Better not
compromise.

It won't be easy.

666 is no longer alone, He's getting out the marrow in your back bone,
And the seven trumpets blowing sweet rock and roll, Gonna blow right
down inside your soul. Pythagoras with the looking glass reflects the
full moon,

In blood, he's writing the lyrics of a brand new tune.

And it's hey babe, with your guardian eyes so blue, Hey my baby, don't
you know our love is true, I've been so far from here, Far from your
loving arms,

Now i'm back again, and babe it's gonna work out fine.

## As sure as eggs is eggs (aching men's feet)

Can't you feel our souls ignite Shedding ever changing colours, in the
darkness of the fading night, Like the river joins the ocean, as the
germ in a seed grows

We have finally been freed to get back home.

There's an angel standing in the sun, and he's crying with a loud voice,
"this is the supper of the mighty one", Lord of lords, King of kings,
Has returned to lead his children home,

To take them to the new jerusalem.

-- genesis - foxtrot - 1972 --
