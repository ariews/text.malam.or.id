--- 
date: "2012-07-03T09:23:00+07:00"
title: "Image Upload API"
tags:
    - API
---

Uhm.. sekarang lagi test bikin image hosting service API. Saat kita
upload file (khususnya gambar) akan kita upload secara otomatis ke
sebuah server tersendiri.

> Kok mirip sama [imgurl](http://api.imgur.com/)?

Ho oh, emang mirip sama itu, tapi buat sekarang dibuat yang simple aja,
bakalan berkembang sih pastinya :) *amin*.

Contohnya seperti ini:

Output:

Sementara ini, meskipun sudah berfungsi dengan baik, tapi baru sampe
tahap staging, blom siap production.

Service ini digawangi sama Kohana, MySQL, Nginx, Linux :)

Hehe.. iya iya, gak pake Nginx juga bisa, mau pake Apache juga bisa, itu
pake nginx karena web server yang ada cuman itu. Gak cuman itu, jika
kita pake OS selain Linux juga bisa kok. Kalo dah kelar ntar di-set
publish deh.

Si MySQL dipake cuman buat setor daftar file yang udah diupload aja,
jadi saat nanti kita melakukan `DELETE` kita gak perlu check lagi ke
filesystem.

Buat sekarang, belom bisa melakukan `UPDATE`.

ps: `http` adalah [HTTPie](http://httpie.org) (HTTPie is a CLI HTTP
utility built out of frustration with existing tools. The goal is to
make CLI interaction with HTTP-based services as human-friendly as
possible.)
