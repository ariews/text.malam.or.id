---
title: "susah ngeprint"
date: "2005-08-17T14:51:00+07:00"
tags:
  - Warnet
  - Tutorial
---

"narik napas panjang"

Sejak beberapa hari lalu, printer di warnet ngadat, gw gak tau knp
sebabnya. setiap orang yg mo print mesti save di "My Documents" lalu
sama Operator di print. Ribet Gak sih? --jangan koment!

Tus terang, gw sih kadang pas lagi jaga kebanyakan diem di kompi yg
kosong, ngelakuin apa aja, apakah chatting, apakah bersih2 kompi (meski
gak segila pas masih pake windows), ngegambar pake Gimp (tepatnya:
belajar pake Gimp). blogshares, dan banyak lagi. Itu kerjaan kalo pas
jaga.

Tus sapa yang jaga di billing? [teh
ina](http://www.kaomifuxi.com/drupal-4.5.4/node/20), dia yg duduk di
billing, kalo ada masalah yang rada2 berat baru giliran gw. Kasian si
Teh ina, dia susah kalo ada yang ngeprint, kebayangd eh sama gw kalo pas
penuh sama yg ngeprint... mana skr unpar mulai aktif lagi (tgl 18
besok).

Gak ngerti gw knp jadi gak bisa ngeprint. Dulu sih bisa, kek pas
ngeprint dari LAN di windows (pake Samba) cuman emang jadi rada lama,
padahal disini printernya laserjet loh!

Keadaan kek gini bisa ngehambat kerjaan gw skr (ada kerjaan mesti
ngebikin web/doain kelar n sukses ya!). Selain itu, gw juga kasian ma
teh ina, apalagi hari2 kita disini selalu sepi, hinga tiap hari itu
bengong mulu, gak ada kegiatan yang bikin kita ngelupain masalah "BETE".

Gw dah coba beberapa kali, install, rei-nstall, delete, rea-dd. ARGH
tetep sama. SIAL!

Akhirnya gw baca2 manual CUPS. Praktek, dan berhasil!

**Caranya:**

Anggap printer itu di komputer **SERVER**, dengan IP: **168.192.0.1**,
nama printernya adalah: **HPLASER**. Lalu printer yang mau ngeprint
(LAN) adalah **MOPRINT** dengan IP: **168.192.0.2**. **Catet!**

**1. Login ke kompi SERVER**

buka file `/etc/cups/cupsd.conf` dengan editor. (gw pake vi) Cari baris
seperti berikut:

```text
<Location />
Order Deny,Allow
Deny From All
Allow From 127.0.0.1
</Location>
```

Add di bawah baris Allow From 127.0.0.1: Allow From 168.192.0.2. Jadi
kira2 seperti ini hasilna.

```text
<Location />
Order Deny,Allow
Deny From All
Allow From 127.0.0.1
Allow From 168.192.0.2
</Location>
```

restart cups dengan login root.

```text
# /etc/init.d/cupsys restart
```

Jika tidak ada error, kita lanjut ke langkah berikutnya.

**2. Sekarang login ke komputer MOPRINT**

Add printer Network dengan CUPS Printer (bukan Windows/Samba), isi URI
dengan `http://IPSERVER:631/printers/NAMAPRINTER` jadi seperti ini:

```text
http://168.192.0.1:631/printers/HPLASER
```

lalu test print. berikut adalah log dari console.

```text
I [17/Aug/2005:14:34:34 +0700] Full reload complete.
I [17/Aug/2005:14:42:55 +0700] Adding start banner page "none" to job 582.
I [17/Aug/2005:14:42:55 +0700] Adding end banner page "none" to job 582.
I [17/Aug/2005:14:42:55 +0700] Job 582 queued on 'HP_hp_LaserJet_1010' by 'pocong'.
```

**Catatan.**

-   `http://168.192.0.1:631/printers/HPLASER` bisa di buka di browser,
    biar jelas.
-   Test di lakukan pada-&gt; client: ubuntu, server: xandros
-   pada client xandros. pilih printer type: others. nanti akan ada
    pilihan ke Internet Printer Protocols (kalo gw gak salah eja)
