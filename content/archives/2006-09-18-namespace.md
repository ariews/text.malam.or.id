---
title: "namespace"
date: "2006-09-18T15:55:00+07:00"
tags:
    - Eggdrop
---

huaha gw lagi belajar namespace neh, biar gw semanget gw kasih contoh yg
asik2 :)

```text
$ tclsh
% cat > testdoang.tcl
namespace eval gwa {
    variable gwvar "EMANG!"
    proc keren {} {
        variable gwvar
        puts $gwvar
    }
}
% source testdoang.tcl
% gwa::keren
EMANG!
%
```

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
