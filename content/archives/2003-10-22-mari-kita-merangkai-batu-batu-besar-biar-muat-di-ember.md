---
title: "mari kita merangkai batu-batu besar biar muat di ember! (="
date: "2003-10-22T03:04:00+07:00"
tags:
    - Email
    - Friends
---

dari pito:

Manajemen Waktu

Suatu hari seorang dosen sedang memberi kuliah tentang manajemen waktu
pada para mahasiswa MBA. Dengan penuh semangat ia berdiri depan kelas
dan berkata, "Okay,sekarang waktunya untuk quiz."

Kemudian ia mengeluarkan sebuah ember kosong dan meletakkannya di meja.

Kemudian ia mengisi ember tersebut dengan batu sebesar sekepalan tangan.
Ia mengisi terus hingga tidak ada lagi batu yang cukup untuk dimasukkan
ke dalam ember.

Ia bertanya pada kelas, "Menurut kalian, apakah ember

ini telah penuh?" Semua mahasiswa serentak berkata,"Ya!" Dosen bertanya
kembali, "Sungguhkah demikian?" Kemudian, dari dalam meja ia
mengeluarkan sekantung kerikil kecil. Ia menuangkan kerikil-kerikil itu
ke dalam ember lalu mengocok-ngocok ember itu sehingga kerikil-kerikil
itu turun ke bawah mengisi celah-celah kosong di antara batu-batu.

Kemudian, sekali lagi ia bertanya pada kelas, "Nah, apakah sekarang
ember ini sudah penuh?" Kali ini para mahasiswa terdiam. Seseorang
menjawab, "Mungkin tidak." "Bagus sekali," sahut dosen.

Kemudian ia mengeluarkan sekantung pasir dan menuangkannya ke dalam
ember. Pasir itu berjatuhan mengisi celah-celah kosong antara batu dan
kerikil.

Sekali lagi, ia bertanya pada kelas, "Baiklah, apakah sekarang ember ini
sudah penuh?" "Belum!" sahut seluruh kelas. Sekali lagi ia
berkata,"Bagus. Bagus sekali."

Kemudian ia meraih sebotol air dan mulai menuangkan airnya ke dalam
ember sampai ke bibir ember. Lalu ia menoleh ke kelas dan bertanya,
"Tahukah kalian apa maksud illustrasi ini?"

Seorang mahasiswa dengan semangat mengacungkan jari dan berkata,
"Maksudnya adalah, tak peduli seberapa padat jadwal kita, bila kita mau
berusaha sekuat tenaga maka pasti kita bisa mengerjakannya.

"Oh, bukan," sahut dosen,"Bukan itu maksudnya

Kenyataan dari illustrasi mengajarkan pada kita bahwa bila anda tidak
memasukkan "batu besar" terlebih dahulu, maka anda tidak akan bisa
memasukkan semuanya."

"Apa yang dimaksud dengan "batu besar" dalam hidup anda?

Anak-anak anda; Pasangan anda; Pendidikan anda; Hal-hal yang penting
dalam hidup anda; mengajarkan sesuatu pada orang lain; Melakukan
pekerjaan yang kau cintai; Waktu untuk diri sendiri; Kesehatan anda;
Teman anda; atau semua yang berharga.

Ingatlah untuk selalu memasukkan "Batu Besar" pertama kali atau anda
akan kehilangan semuanya. Bila anda mengisinya dengan hal-hal kecil
(semacam kerikil dan pasir) maka hidup anda akan penuh dengan hal-hal
kecil yang merisaukan dan ini semestinya tidak perlu. Karena dengan
demikian anda tidak akan pernah memiliki waktu yang sesungguhnya anda
perlukan untuk hal-hal besar dan penting.

Oleh karena itu, setiap pagi atau malam, ketika akan merenungkan cerita
pendek ini, tanyalah pada diri anda sendiri : Apakah "Batu Besar" dalam
hidup saya? Lalu kerjakan itu pertama kali.
