---
title: "Perjalanan:Install Openkiosk di Xandros OC 3.0.1 #2"
slug: perjalanan_install_openkiosk_di_xandros_oc_3_0_1_2
date: "2005-06-21T22:47:00+07:00"
tags:
  - Warnet
  - Tutorial
---

Akhirnya install Qt beres juga, lama banget! makslum kompi 'heubeul' p2
450 128MB

Catatan tambahan, sebelom ini saya juga pernah melakukan instalasi yang serupa
tapi mungkin ada cara2 yang salah, pada saat yang lalu, saya sempat menginstall
KDE header library via Xandros Network.
Nama paket yang di install sebelumnya:

- gcc
- g++
- KDE Development Tools

3:49pm fiuh.. akhirnya beres :P

```text
The Qt library is now built in ./lib  
The Qt examples are built in the directories in ./examples  
The Qt tutorials are built in the directories in ./tutorial  

Enjoy!   - the Trolltech team 
```

```text
make install

tambahkan baris '/usr/local/qt/lib' ke file /etc/ld.so.cobf'
lalu jalankan:

ldconfig
```

QT SELESAI

Install NodeView 0.8.3

```text
cd ~/dl
tar -zxf nodeview-0.8.3.tar.gz
cd nodeview-0.8.3
./configure
make && make install

jalankan nodeview dari menu -> run

/usr/local/bin/nodeview

setting configuration
```

CLIENT

1.  Install BerkeleyDB (Lihat Installasi Server point 4.)
2.  Install Qt (lihat Installasi Server point 5.)
