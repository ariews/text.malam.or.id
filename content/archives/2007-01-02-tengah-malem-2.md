---
title: "Tengah malem"
date: "2007-01-02T01:13:00+07:00"
tags:
    - Myself
    - Desktop
    - Komputer
---

Stress gak tau mesti gmn!

Sejak malam sebelum ied, kepala gw dah puyeng, mikirin hdd gw gak tau
kenapa error.

![alt text](/images/50296437621b6screenshot-1.png)

ada yg bisa bantu kasih tau cara recover data, atau benerin?

Padahal isinya data penting gw semua, ya sih emang gak banyak, cuman
12G. Tapi buat gw penting banget, soalna itu arsip kerjaan gw yg
sekarang sama yg dulu. Ada juga di dalamnya beberapa script2 (atau file2
web) yg pernah gw buat. Yahh buat sekedar referensi di lain waktu.

Meski lagi pusing karena hdd gw ntah kenapa gak bisa di
[mount](http://www.die.net/doc/linux/man/man8/mount.8.html), dan juga
ada banyak kerjaan di [Malaka9](http://www.malaka9.com) (tempat gawe
skr). Gw masih sempet2nya benerin [tcl script](http://bot.to.md/13)
yg sempet di buat.

[![IMG: My desktop](/images/5029643803d8cscreenshot_th.png)](/uploads/screenshot.png)
