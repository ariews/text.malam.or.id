---
title: "3 jam!"
date: "2005-01-28T12:11:00+07:00"
---
Abis malem tadi bangun, gw tidur lagi sekitar jam 4 pagi, humpp.. susah
tidur.. gw gk bisa tidur dengan lampu menyala, mungkin gw bener2 tidur
pada jam2 4:30 pagi. dan terbang ke alam mimpi.. *\[cieeeee kek
dongeng2..\]*.

*\[Cerita anak SD\]* Dalam tidur gw bermimpi..

Gw liat di depan rumah gw ada buaya besar banget, gak tau sapa yang
punya, apakag nyokap, atau orang laen. Karena kita gak punya tempat yang
gede, maka buaya itu kita lepasin di kolam belakang, di kolam itu
sebelumnya banyak banget ikan mas, gurame. dan gede2!. Sekarang dah abis
di makan Buaya.

Suatu hari, gw sama oom mancing di sana, aneh! padahal ikan udah pada
abis, tus lagi.. gw dapat banyak ikan gede!

Sampe ke saat, oom gw mancing terlalu deket sama kolam itu. Dia
kepeleset, dan lo tau apa? dia di makan Buaya peliharaan keluarga...
tragis... lagi2 aneh.. dia masih senyum2 aja pas gw coba tarik dari
kolam.

*\[Cerita selesai\]*

gw bangun.. lagi dan minum air putih... seenggaknya gw cukup kaget
dengan mimpi "oom gw di makan buaya"

*\[another dream\]* Setelah agak tenang, gw tertidur lagi...

skr.. gw ngeliat pemandangan sebuah Cafe... ada banyak orang di sana,
nyanyi rame2 *\[gw gak inget dengan jelas gimana lirik lagunya\]*, tapi
ada seseorang yang memegang sebuah poto, dia duduk sendirian, gak kek di
pelem2 yang biasanya duduk sendiri di pojok, dia malah duduk di tengah2.

dia menjelaskan kenapa dia ada di sana, kenapa ada tulisan2 di poto yang
dia pegang (tulisannya gak jelas gw liat) dan kenapa gambarnya seperti
itu. Dia mengatakan pengen punya Rumah buat kluarga doang. dan orang itu
adalah bokap gwa!

*\[mimpi beres\]* Gw bangun bobo.. gak bisa tidur lagi.

Buat kalian, mungkin mimpi ini cuman cerita basi, gak berarti, omong
kosong, atau "aaaaaaaaaaaaaaaaaahhh bete dengernya".Tapi buat gw? nafas
gw langsung kek senen kamis.. kek orang abis marathon, kek keabisan
nafas... karena setaun lalu oom gw meninggal duluan, lalu gak selang
berapa lama (seminggu) babeh gw meninggal juga.

dan gw cuman bisa bengong... gw kangen babeh
