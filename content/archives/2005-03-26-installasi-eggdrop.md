---
title: "installasi eggdrop"
date: "2005-03-26T20:45:00+07:00"
tags:
  - Tutorial
---

Ini adalah bagian dari installasi eggdrop yang gw post [waktu
lalu](http://www.malam.or.id/blog/369.26.html), beberapa masalah kerap
di tanyakan temen gw saat mereka install eggdrop. Dan yang paling sering
adalah TCL.

Eggdrop bisa kita install kalo di system kita ada tclnya (shared library
dan header). Untuk menyelesaikan masalah ini mari kita install tcl di
system.

1.  Download source dan decompress

Pertama adalah download tcl nya. bisa kita dapat dari web
[sf.net](http://sf.net) di bagian [tcl
project](http://sourceforge.net/projects/tcl/).

Asumsikan bahwa kita mempunyai `wget` atau `lynx`, ini kita butuhkan
untuk mendownload source tcl yang akan kita install.

**wget**

```text
wget http://voxel.dl.sourceforge.net/sourceforge/tcl/tcl8.3.5-src.tar.gz
```

**lynx**

```text
lynx -source http://voxel.dl.sourceforge.net/sourceforge/tcl/tcl8.3.5-src.tar.gz \
      > tcl8.3.5-src.tar.gz
```

Setelah kita download, kita perlu me-decompress source tarball nya. Kita
gunakan

```text
gunzip < tcl8.3.5-src.tar.gz | tar -xf -
```

1.  Configure dan install.

Jika kita bukan `root` kita konfigurasi tcl dengan menggunakan option
`--prefix` (kenyataannya root un bisa menggunakan option ini). ketika
bukan root, kita install tcl di HOME direktory kita sendiri. Hal ini si
maksudkan untuk menghindari installasi tcl di folder default:
`/usr/local/...` yang user biasa tidak mungkin menginstall
library/program di sana.

```text
$ ./configure --prefix=$HOME
$ make
$ make install
```

Tapi jika kita punya akses root, kita bisa menghilagkan option tadi,
sekali lagi meskipun kita bisa tetap mengguanakannya.

```text
# ./configure
# make
# make install
```

Setelah install tcl library, jangan lupa add `$HOME/lib` ke dalam `PATH`
kita, tapi biasanya gak perlu sih

1.  Install Eggdrop

Untuk install eggdrop, kita buka kembali catatan terdahulu mengenai
[installasi eggdrop](http://www.malam.or.id/blog/369.26.html).

Selamat membuat eggdrop~
