---
date: "2004-12-04T09:00:00+07:00"
title: "Selamat menjadi wanita"
tags:
    - Email
---

Dari pito:

Saya udah coba ngecilin hurufnya tapi gagal... semoga bisa di baca
deh... dan menjadi wanita yang baik.. (*buat yang bukan wanita saya
harapkan pengertian anda, ngertiin ni huruf kenapa besar besar begini
..:):):)*)

Seorang anak laki-laki kecil bertanya kepada ibunya "Mengapa engkau
menangis?","Karena aku seorang wanita", kata sang ibu kepadanya.

"Aku tidak mengerti", kata anak itu. Ibunya hanya memeluknya dan
berkata, "Dan kau tak akan pernah mengerti"

Kemudian anak laki-laki itu bertanya kepada ayahnya, "Mengapa ibu suka
menangis tanpa alasan?""Semua wanita menangis tanpa alasan", hanya itu
yang dapat dikatakan oleh ayahnya.

Anak laki-laki kecil itu pun lalu tumbuh menjadi seorang laki-laki
dewasa, tetap ingin tahu mengapa wanita menangis.

Akhirnya ia menghubungi Tuhan, dan ia bertanya, "Tuhan, mengapa wanita
begitu mudah menangis?"

Tuhan berkata:

"Ketika Aku menciptakan seorang wanita, ia diharuskan untuk menjadi
seorang yang istimewa. Aku membuat bahunya cukup kuat untuk menopang
dunia; namun, harus cukup lembut untuk memberikan kenyamanan "

"Aku memberikannya kekuatan dari dalam untuk mampu melahirkan anak dan
menerima penolakan yang seringkali datang dari anak-anaknya"

"Aku memberinya kekerasan untuk membuatnya tetap tegar ketika
orang-orang lain menyerah, dan mengasuh keluarganya dengan penderitaan
dan kelelahan tanpa mengeluh "

"Aku memberinya kepekaan untuk mencintai anak-anaknya dalam setiap
keadaan, bahkan ketika anaknya bersikap sangat menyakiti hatinya "

"Aku memberinya kekuatan untuk mendukung suaminya dalam kegagalannya dan
melengkapi dengan tulang rusuk suaminya untuk melindungi hatinya "

"Aku memberinya kebijaksanaan untuk mengetahui bahwa seorang suami yang
baik takkan pernah menyakiti isterinya, tetapi kadang menguji
kekuatannya dan ketetapan hatinya untuk berada disisi suaminya tanpa
ragu "

"Dan akhirnya, Aku memberinya air mata untuk diteteskan.

Ini adalah khusus miliknya untuk digunakan kapan pun ia butuhkan."

"Kau tahu: Kecantikan seorang wanita bukanlah dari pakaian yang
dikenakannya, sosok yang ia tampilkan, atau bagaimana ia menyisir
rambutnya."

"Kecantikan seorang wanita harus dilihat dari matanya, karena itulah
pintu hatinya tempat dimana cinta itu ada."
