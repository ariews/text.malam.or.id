---
title: "Kompi Yessy"
date: "2005-01-19T00:50:00+07:00"
---

Kemaren (tgl:17.01.04) ade gw kesini, dia bilang minta di check in CD-RW
nya, kata dia gak bisa di pake ngeburn lagi. Dia rencana datang ke net
sama temennya, Yessy (mudah2an gw bener ngeja namanya). Jam 1, kurang
lebih, siang dia dateng tapi koq sendirian, gak bareng Yessy. Kata dia:
Yessy ada kuliah jam 4.

Adek gw dateng ke sini sambil bawa CD-RW nya trus beberapa CD blank,
sama Harddisk (hdd). Dia bilang itu buat nge-backup file2 yang ada di
hdd nya yessy. Ada banyak yang mo di backup, terutama file mp3.

Emang keknya itu CD-RW dah rusak kali yaa, soalna gw otak-atik juga gak
bisa di pake lagi. Tus gmn dong sama backup file na yessy? Untungna ada
BENQ nya stanny, jadi pake itu dulu. Itu hdd katanya error, gak bisa
masuk windows di sana nya, jadi selain di backup filenya, juga di
install ulang.

Meskipun rada susah (restart2 terus), akhirnya berhasil juga install
XP.SP2. Tinggal Backup file, tapi gak jadi karena dah terlalu malem. si
adek sama temennya pun balik ke kostan. Gw nganterin sampe ke gandok,
mayan jauh lah.. kira2 1.5Kilo (jalan kaki) dan baliknya gw jalan juga,
soalna udah gak ada kendaraan umum. CAPEK!!

Baru sampe ke net, baru duduk, ada sms masuk dari adek gw, isinya:
*"iiie.. itu kompinya koq jadi gak nyala?"*. Mati gw, koq bisa? Gak tau
deh.. gw bilang: *"coba aja dulu, kalo masih gak bisa gw besok ke sana
sore hari"*. Gak ada sms masuk lagi berarti itu kompi emang jadi kagak
bisa nyala.

Hari ini, gw sms adek gw gini: *"ni, tadi test ngeburn, tapi ada error,
kesini aja deh bawa atm, tus beli CD-RW ke BEC"*. Tadi jam 3 sore, adek
gw dateng lagi (sekarang) sama yessy. Ya karena bentar lagi beres jaga,
gw bilang : *"tar aja deh bareng belinya, sekalian install kompi yessy
di sana, bentar lagi beres jaga."*

Sepulang dari BEC (yessy kagak ikut), gw langsung ke tempat makan :P
laper boq :D terus installing kompi yessy, ternyata emang error, gak
bsia masuk windows. Gwa coba install lagi gak bisa juga, di boot dari
CDROM tetep gak bisa (heheh CD Installer nya error pas gw bikin di net
kemaren), untung di sana ada yang punya CD XP, jadi ya pake punya
temennya, dengan susah payah (beberapa kali restart, mirip sama kek
kemaren pas di install di net) akhirnya bisa juga tuh XP di install,
lalu up ke XP.SP2 (lancar). Pas install Office2003 gw balik.

Mayan capek juga.. mana tadi keujanan pas berangkat ke BEC, sekarang
pala gw pusing banget nih. Adek gw ih senyum2 aja.. soalna sekarang dah
punya CD-RW baru.. gw yang keabisan duit.. hu hu hu..

enaknya jadi adek...
