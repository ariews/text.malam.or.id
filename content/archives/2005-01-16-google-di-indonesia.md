---
title: "Google di Indonesia?"
date: "2005-01-16T14:58:00+07:00"
---

![Google](/images/502963eebc13fLogo_60wht.gif)

[Google](http://www.google.com) sampe di Indonesia, hal ini memungkinkan
pencarian data dari Indonesia makin cepet!

Selain [Indonesia](http://www.google.co.id), juga beberapa negara
seperti: [Africa Selatan](http://www.google.co.za) dan
[Jamaica](http://www.google.com.jm) dan [beberapa negara
lainnya](http://www.google.com/googleblog/2005/01/domains-of-choice.html).
