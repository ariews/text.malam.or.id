---
title: "FWD Mengemis dan Memberi Pengemis Didenda Rp 20 Juta"
date: "2007-09-19T19:24:00+07:00"
---

fyi, spy ga kena denda

cheers :)

Kita mungkin selalu bingung dan prihatin, kenapa begitu banyak pengemis
di jalan-jalan di Jakarta. Tapi, mungkin gak kita pernah berpikir bahwa
kita juga yang membuka "lapangan pekerjaan" tersebut dengan selalu rajin
memberikan sedekah kepada mereka? Mungkin ini maksud dari peraturan di
ibukota yang sudah disahkan? Sila direnungkan

Mengemis dan Memberi Pengemis Didenda Rp 20 Juta

[http://www.kompas.com/ver1/Metropolitan/0709/11/045404.htm](https://web.archive.org/web/20090101044426/http://www2.kompas.com:80/ver1/Metropolitan/0709/11/045404.htm)

KEBON SIRIH, WARTA KOTA - Hati-hati jika Anda ingin bersedekah kepada
pengemis, baik ketika berada di kendaraan umum, atau perempatan jalan.
Alih-alih bermaksud berbuat baik, Anda bakal dikenai sanksi denda hingga
maksimal Rp 20 juta atau mendekam di tahanan paling lama 60 hari. Hal
itu merupakan konsekuensi pemberlakuan peraturan daerah (perda) tentang
Penyelenggaraan Ketertiban Umum yang disahkan dalam Rapat Paripurna DPRD
DKI, Senin (10/9). Perda baru itu merupakan pengganti Perda No 11 tahun
1988 tentang Ketertiban Umum yang dianggap tak lagi memadai menghadapi
perkembangan kondisi sosial Ibu Kota.

Larangan memberi sedekah kepada pengemis, maupun melakukan aktivitas
mengemis itu termuat dalam pasal 40 huruf b, dan c. Dalam pasal itu, tak
hanya mengemis saja yang dilarang melainkan juga mengamen, mengasongkan
dagangan, dan mengelap mobil di tempat umum. "Kalau ingin menyumbang dan
memberi sedekah, salurkan lewat lembaga resmi yang sudah ada, misalnya
lewat Bazis," ujar Ketua Fraksi PPP Achmad Suaedy, kepada wartawan usai
menghadiri rapat paripurna di Gedung DPRD DKI, kemarin.

Pemberlakuan larangan pun tak hanya berlaku pada pelaku, dan pemberi
sedekah bagi pengemis saja, melainkan juga terhadap pihak-pihak yang
mengorganisasi, atau memerintahkan aktivitas tersebut. Dan, sanksi bagi
mereka ini lebih berat, sesuai pasal 61 ayat 2, orang yang menyuruh
mengemis, mengasong, mengamen, atau mengelap kaca mobil dikenai sanksi
denda paling banyak Rp 30 juta, atau kurungan maksimal 90 hari. Gubernur
DKI Sutiyoso mengatakan, pemberlakuan aturan-aturan baru dalam perda
tersebut sebagai upaya meningkatkan budaya disiplin dan tertib di
kalangan warga Jakarta . Selain itu, juga untuk memperbaiki citra
Jakarta sebagai Ibu Kota Negara yang tertib dan nyaman. "Ketertiban umum
di kota mana pun harus ditegakkan karena ini untuk kepentingan bersama.
Perda ini harus kita lakukan secara konsekuen," ujar Sutiyoso usai
menghadiri rapat paripurna, kemarin.

Pemprov DKI akan melakukan sosialisasi mengenai isi dan konsekuensi
perda baru itu kepada masyarakat luas selama sekitar empat bulan,
sebelum secara efektif memberlakukan ketentuan tersebut. Sutiyoso
berjanji akan meningkatkan kinerja aparat pamong praja yang dimiliki
Pemprov untuk menjamin penegakan hukum atas perda itu. "Kalau soal
aparat yang tidak baik, itu masalah mentalnya, dan akan kita perbaiki.
Yang penting kesadaran masyarakat untuk disiplin, karena masalah
disiplin ini bukan hanya di Jakarta, secara nasional kita lemah di
bidang ini," ujar gubernur yang tinggal sebulan lagi menjabat itu. Perda
Penyelenggaraan Ketertiban Umum kemungkinan besar baru akan diberlakukan
efektif mulai tahun depan.

Kewajiban dan Larangan

Beberapa kewajiban dan larangan Perda Tibum, sebagai berikut:

-  Pejalan kaki wajib berjalan di tempat yang ditentukan.
-  Setiap orang wajib menyeberang di tempat penyeberangan yang
   disediakan.
-  Setiap penumpang wajib menunggu di halte atau pemberhentian yang
   ditetapkan (pelanggaran atas 3 aturan di atas, dikenai denda Rp
   100.000-Rp 20 juta, atau kurungan 10-60 hari).
-  Setiap pengemudi wajib menunggu, menaikkan, dan menurunkan penumpang
   di tempat pemberhentian yang ditentukan (pelanggaran didenda Rp
   500.000 - Rp 30 juta, atau sanksi kurungan 20-90 hari).
-  Setiap kendaraan bermotor dilarang memasuki jalur busway (pelanggaran
   didenda Rp 5juta-Rp 50 juta, atau sanksi kurungan 30-180 hari).
-  Ketentuan 3 in 1, dan larangan penggunaan joki (pelanggaran didenda
   Rp 500.000-Rp 30 juta, atau sanksi kurungan 20-90 hari).
-  Larangan menjadi joki 3 in 1 (pelanggaran didenda Rp 100.000-Rp 20
   juta, atau sanksi kurungan 10-60 hari).
-  Larangan menjadi penjaja seks atau memakai jasa penjaja seks
   komersial (pelanggaran didenda Rp 500.000-Rp 30 juta, atau sanksi
   kurungan 20-90 hari)
-  Larangan menyuruh, memfasilitasi, membujuk, memaksa orang untuk
   menjadi penjaja seks komersial (pelanggarannya dianggap sebagai
   tindak pidana kejahatan)
-  Larangan menyediakan bangunan sebagai tempat berbuat asusila (didenda
   Rp 5 juta-Rp 50 juta, atau sanksi kurungan 30-180 hari).

ah... gak tau deh gw, apa bener aturan itu bakal jalan sebagai mana mestinya
