---
title: "Perjalanan:Install Openkiosk di Xandros OC 3.0.1  #1"
slug: perjalanan_install_openkiosk_di_xandros_oc_3_0_1_1
date: "2005-06-21T11:01:00+07:00"
---

Sebenernya dah di coba berapa kali, tapi tetep gagal, kemaren ada si bos
yang minta ym-an sama gw, katanya mo forward imel, dia bialng gak bisa
send ke aryes ([lo tau kan? aryes dah kena suspend dari
yahoo](http://www.malam.or.id/blog/1629,2005,06,03/)) emang sempet di
tanyain sih sama si bos, 'knp gak bisa disuspend?' hehe gw jawab:
'karena kebanyakan exploit :P'

Btw, gw di minta buat ngetest 'Install Ulang OpenKiosk di Xandros OC
3.0.1' (gw sempet kasih tau kalo [gw pengen
tau](http://www.malam.or.id/blog/1626,2005,05,31/) cara install
openkiosk di [xandros](http://www.xandros.com)).

si bos kasih gw imel, isinya forward dari yang sudah berhasil install
openkiosk di **linux** (?? xandros juga linux, tapi menurut gw beda) gak
di sebutkan apakah di [Redhat](http://www.redhat.com),
[Fedora](http://fedora.redhat.com),
[Mandrake](http://www.mandriva.com/), [Debian](http://www.debian.org/)
(masih deket), [Slackware](http://www.slackware.org/) atau [yang
lainnya](http://www.linux.org/dist/index.html)?

Ok deh cuekin aja dulu, soalna gw kira emang sama aja, paling beda
setting

Lanjuttttt...

Berikut adalah imel yang diforward ke gw dari si bos.

```text
================================== Forwarded message ==================================
From: Harry Sufehmi <milis-2@...>
Date: Jun 20, 2005 11:43 AM
Subject: [asosiasi-warnet] Billing di Linux: OpenKiosk
To: asosiasi-warnet@...

Berikut ini adalah informasi cara instalasi salah satu program billing
di Linux, yaitu OpenKiosk : http://openkiosk.sourceforge.net
nb: program ini TIDAK bisa digunakan di LTSP.

Salah satu kelebihan OpenKiosk adalah bahwa client / workstation bisa
menggunakan sistim operasi Linux, FreebSD/OpenBSD, maupun Windows.
Program billing ini juga gratis.

SYARAT:
# Client (yang Linux) menggunakan KDE (bukan Gnome)

CARA INSTALASI:

1. Buat direktori khusus untuk mengumpulkan semua program yang
    dibutuhkan, ketik perintah2 berikut ini di console/terminal:

    mkdir /downloads
    cd /downloads

2. Download program-program berikut ini ke /downloads

   Nodeview :
   http://prdownloads.sourceforge.net/openkiosk/nodeview-0.8.3.tar.gz?download
   (di SERVER)

   kdex11client :
   http://prdownloads.sourceforge.net/openkiosk/kdex11client-0.3.1b.tar.gz?download
   (di CLIENT)

   Client for Windows :
   http://prdownloads.sourceforge.net/openkiosk/win32lock-0.8.exe?download

   Qt:
   http://sunsite.rediris.es/mirror/Qt/source/qt-x11-free-3.3.4.tar.bz2
   (di SERVER & CLIENT)

   BerkeleyDB:
   http://downloads.sleepycat.com/db-4.3.28.NC.tar.gz
   (di SERVER & CLIENT)

3. Install BerkeleyDB : (di SERVER & CLIENT)

   cd /downloads
   tar xzvf db-4.3.28.NC.tar.gz
   cd db-4.3.28.NC
   cd build_unix
   ../dist/configure
   make
   make install
   cd ..
   cd ..

4. Install Qt : (di SERVER & CLIENT)

   cd /downloads
   bzip2 -d qt-x11-free-3.3.4.tar.bz2
   tar xvf qt-x11-free-3.3.4.tar
   cd qt-x11-free-3.3.4
   ./configure

   Qt akan menanyakan apakah Anda setuju dengan isi lisensi-nya,
   ketik "yes" lalu tekan Enter

   make
   make install
   cd ..

 5. Install Nodeview (controller)  (di SERVER)

   cd /downloads
   tar xzvf nodeview-0.8.3.tar.gz
   cd nodeview-0.8.3
   ./configure
   make
   make install

   Nodeview kemudian bisa dijalankan dengan mengetikkan
   "/usr/local/bin/nodeview"

6. Install kdex11client (di CLIENT)

   cd /downloads
   tar xzvf kdex11client-0.3.1b.tar.gz
   cd kdex11client-0.3.1b
   ./configure
   make
   make install

7. Jalankan Nodeview

   /usr/local/bin/nodeview

   Nodeview akan meminta login untuk Administrator. Karena kita
   menjalankannya baru pertama kali, maka kita bisa mengetikkan
   apa saja sebagai passwordnya.

8. Setup Nodeview

   Pilih menu Settings - Configuration.
   Klik tab Workstation,
   Klik SETUP,
   masukkan nama workgroup (bebas saja, contoh: warnet),
   Klik ADD,
   Klik CLOSE,

   Catatkan komputer-komputer yang ada di warnet Anda :
   Di kolom "station name", masukkan nama komputer (contoh:
   klien1, klien2, dst)
   Di kolom "IP address", masukkan IP address dari komputer2 tsb
   Lalu klik "ADD/EDIT" untuk menambahkan

   Tutup semuanya, Nodeview akan meminta password untuk
   Administrator, masukkan terserah Anda.

   Lalu jalankan lagi Nodeview.

Kini program OpenKiosk sudah siap untuk Anda gunakan.

Semoga bermanfaat.

Salam,
Harry
```

yang saya lakukan (root/xandros oc 3.0.1)

SERVER

```text
1. mkdir -p ~/dl
2. cd ~/dl
3. download:

   http://ovh.dl.sourceforge.net/sourceforge/openkiosk/nodeview-0.8.3.tar.gz
   http://sunsite.rediris.es/mirror/Qt/source/qt-x11-free-3.3.4.tar.bz2
   http://downloads.sleepycat.com/db-4.3.28.NC.tar.gz

4. Install BerkeleyDB 4.3.28

   tar -zxf db-4.3.28.NC.tar.gz
   cd db-4.3.28.NC/build_unix
   ../dist/configure
   make
   make install

masukan '/usr/local/BerkeleyDB.4.3/lib' ke dalam file '/etc/ld.so.conf'
lalu jalankan:

   ldconfig

5. Install Qt 3.3.4

   cd ~/dl
   tar -jxf  qt-x11-free-3.3.4.tar.bz2
   cd qt-x11-free-3.3.4
   ./configure

==================================note:start==================================

pada awal baris tertulis:

   WARNING: /usr/X11R6/lib/libGLU.so is threaded!
   The Qt OpenGL module requires Qt to be configured with -thread.

   ==================================

pertengahan condigure:

      - Also available for Linux: linux-kcc linux-icc linux-cxx

   Configuration .......  nocrosscompiler minimal-config small-config medium-config
   large-config full-config styles tools kernel widgets dialogs iconview workspace
   network canvas table xml sql release dll largefile stl ipv6 png no-gif zlib nis
   cupsbigcodecs x11sm xshape xinerama xcursor xrandr xrender xftfreetype xkb
   STL support ......... yes
   PCH support ......... no
   IPv6 support ........ yes
   Thread support ...... no
   NIS support ......... yes
   CUPS support ........ yes

   Large File support .. partial
   GIF support ......... no
   MNG support ......... plugin (qt)
   JPEG support ........ plugin (qt)
   PNG support ......... yes (qt)
   zlib support ........ yes
   OpenGL support ...... no
   NAS sound support ... no
   Session management .. yes
   XShape support ...... yes
   Xinerama support .... yes
   Tablet support ...... no
   Xcursor support ..... yes
   XRandR support ...... yes
   XRender support ..... yes
   Xft support ......... yes
   XKB Support ......... yes

   ==================================

   make

==================================lama banget.. maklum deh p2 450,128, skr jam 7:57pm
padahal mulai dari jam 3.15pm *sigh*==================================

terjadi banyak warning
terjadi banyak warning

==================================lama banget.. maklum deh p2 450,128, skr jam 8:48pm
padahal mulai dari jam 3.15pm *sigh*==================================

terjadi banyak warning

jam 9 malem, belom beres make nya... ngantuk.. tidur....

bangun jam 2 pagi, gak beres juga! eh gak tau sih, tapi ilang konsole nya!

males, install ulang ntar aja pagi2.

==================================note:end==================================
```

jam 9:43am

```text
==================================

mkdir -p /qt;cd /qt
tar -jxf ~/src/qt-x11-free-3.3.4.tar.bz2
cd qt-x11-free-3.3.4

==================================note:start==================================

baca INSTALL

==================================note:end==================================

QTDIR=/usr/local/qt
PATH=$QTDIR/bin:$PATH
MANPATH=$QTDIR/doc/man:$MANPATH
LD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH
./configure -prefix /usr/local/qt -thread


==================================note:start==================================

di awal baris, tidak ada lagi tulisna warning:

WARNING: /usr/X11R6/lib/libGLU.so is threaded!
The Qt OpenGL module requires Qt to be configured with -thread.

tapi langsung configure.

ln -s ../qmake/qmake /qt/qt-x11-free-3.3.4/bin/qmake

     This target is using the GNU C++ compiler (linux-g++).

     Recent versions of this compiler automatically include code for
     exceptions, which increase both the size of the Qt library and the
     amount of memory taken by your applications.

     You may choose to re-run configure with the -no-exceptions
     option to compile Qt without exceptions. This is completely binary
     compatible, and existing applications should continue to work.


Build type:    linux-g++
Platform notes:

         - Also available for Linux: linux-kcc linux-icc linux-cxx

Configuration .......  nocrosscompiler minimal-config small-config medium-config
large-config full-config styles tools kernel widgets dialogs iconview workspace
network canvas table xml opengl sql release dll thread largefile stl ipv6 png
no-gifzlib nis cups bigcodecs x11sm xshape xinerama xcursor xrandr xrender
xftfreetype xkb
STL support ......... yes
PCH support ......... no
IPv6 support ........ yes
Thread support ...... yes
NIS support ......... yes
CUPS support ........ yes
Large File support .. partial
GIF support ......... no
MNG support ......... plugin (qt)
JPEG support ........ plugin (qt)
PNG support ......... yes (qt)
zlib support ........ yes
OpenGL support ...... yes
NAS sound support ... no
Session management .. yes
XShape support ...... yes
Xinerama support .... yes
Tablet support ...... no
Xcursor support ..... yes
XRandR support ...... yes
XRender support ..... yes
Xft support ......... yes
XKB Support ......... yes

==================================note:end==================================

/usr/bin/make
```

Skr blom kelar! masih `make`
