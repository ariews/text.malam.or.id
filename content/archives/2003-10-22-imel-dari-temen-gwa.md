---
title: "imel dari temen gwa..."
date: "2003-10-22T02:57:00+07:00"
tags:
    - Email
---

dari pito:

Pada hari pertama, diciptakan seekor sapi. Si Pencipta berkata kepada
sapi itu, "Sapi, hari ini Aku menciptakan kamu sebagaimana adanya kamu !
Kamu harus bekerja di ladang dengan petani sepanjang hari. Kamu harus
menguras tenaga untuk membajak sawahnya. Di samping itu kamu juga harus
menyediakan susu untuk diminum. Kamu akan bekerja sepanjang hari di
bawah terik matahari !Sebaliknya, kamu hanya bisa makan rumput !! Aku
akan memberi waktu untuk hidup selama 50 tahun."

Sapi protes. "Waduh?! Saya bekerja begitu keras dan hanya bisa makan
rumput saja ?! Dan hidup selama 50 tahun ?! Bolehkan saya hanya hidup 20
tahun saja ?? Dan 30 tahun saya berikan kepadamu, ok ? Moo ..... Moo."
akhirnya, Pencipta setuju.

Pada hari kedua, diciptakan seekor anjing. Si Pencipta berkata kepada
anjing itu, "Ajing, Kuciptakan kamu seperti apa adanya kamu, kamu harus
duduk sepanjang hari di depan pintu rumahmu ! Semua orang yang datang ke
rumahmu, kamu harus menggonggong menyambut mereka !! Sementara kamu akan
makan sisa sisa makanan tuanmu, dan kamu akan hidup selama 20 tahun !"

Ajing protes. "Waduh?! Sepanjang hari duduk di depan pintu ?! Harus

menggonggong orang ?! Jangan dong ... 10 tahun hidup, saya mau. Dan 10
tahun lainnya saya berikan kepadamu !?" Si Pencipta setuju.

Pada hari ketiga, diciptakan monyet. Ia berkata kepada monyet itu,

"Monyet, pekerjaanmu sebagai monyet adalah menghibur orang. Kamu harus
membuat mereka tertawa dengan gaya dan mimik mukamu. Kamu juga harus
bisa melakukan salto supaya mereka terkagum-kagum kepadamu. Sementara
itu, kamu hanya makan pisang, dan Aku akan memberimu hidup 20 tahun.

Monyet protes. "Waduh ?! Membuat mereka tertawa?! Melakukan atraksi
dengan gaya dan muka lucu ?! Apalagi, harus bisa salto ? Itu tidak
gampang ! Berikan saya 10 tahun dan 10 tahun lainnya saya berikan
kepadamu."

Pencipta pun setuju dengan tawaran Monyet.

Pada hari yang keempat, diciptakan manusia dan berkata kepadanya,

"Kamu! Manusia, kerjamu adalah tidur, makan, tidur, main, makan, tidur,
main dan tidak bekerja apa pun sepanjang hari. Kamu akan makan apa saja
yang ada di dunia ini dan bermain apa saja yang kamu senangi dalam
hidupmu. Semua yang kamu perlukan hanya menikmati, menikmati, menikmati
dan tidak bekerja apa pun. Itulah hidupmu. Dan aku berikan 20 tahun
untuk kamu."

Maunusia protes. "Yaah ?! Betapa enaknya hidup seperti itu ! Makan,

main, tidur, tidak bekerja, menikmati apa saja dan Engkau memberikan
waktu untuk Hidup 20 tahun ?! Jangan dong ...! Ok ! Bagaimana kalau kita
buat kesepakatan? Karena si Sapi mengembalikan 30 tahun, Ajing
mengembalikan 10 tahun dan Monyet mengembalikan 10 tahun, biar saya saja
yang mengambil waktu hidup mereka itu ?! Jadi, waktu hidup saya selama
70 tahun, kan?"

Pencipta pun setuju dengan tawaran si Manusia.

Akhirnya, itulah sebabnya di 20 tahun pertama, kita hanya makan,

tidur, main dan menikmati apa saja dan tidak bekerja. Kemudian selama 30
tahun kita bekerja sepanjang hari, bekerja keras dan harus menanggung
keluarga. Setelah itu, 10 tahun kemudian kita menjadi tua, capek dan
tinggal di rumah, duduk di depan pintu rumah dan menyambut tamu yang
datang. Dan 10 tahun terakhir, kita hanya bisa menghibur cucu - cucu
kita dengan gaya dan wajah lucu.

REFLEKSI

Di saat weekend esok, ada baiknya Anda merenungkan kembali kehidupan
Anda. Sudahkah Anda mewarnai hidup Anda dengan sesuatu yang berbeda dan
bermanfaat tidak hanya bagi diri Anda sendiri? Ataukah Anda menjalani
kehidupan ini rutin, apa adanya? Bukankah ada banyak hal (potensi,
bakat, ketrampilan) yang Anda miliki, dan terlalu sayang kalau Anda
biarkan "nganggur"?
