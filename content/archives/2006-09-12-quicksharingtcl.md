---
title: "quicksharing.tcl"
date: "2006-09-12T17:32:00+07:00"
tags:
    - Eggdrop
---

ah ah.. bukan gw gak demen sama rapidshare, tapi skr ribet huhuhu...

tadi gw nemu link quicksharing.com, mayan juga, gw bisa upload file,
poto atau mp3 tapi gw bisa langsung download dari shell (pake tcl). gak
bener2 tcl script sih, tapi pake wget juga buat downloadna, mayan lah
jadi lebih dikit.

```text
$ tclsh
% qd:usewget http://s16.quicksharing.com/v/4468634/111520.jpg.html
Download 111520.jpg (0.02 mb) OK!
% qd:usewget http://s22.quicksharing.com/v/4974876/RainForEsTipUU_007.JPG.html
Download RainForEsTipUU_007.JPG (0.10 mb) OK!
% ls -al
total 140
drwxr-xr-x  2 arie users   4096 2006-09-13 05:18 .
drwxr-xr-x  3 arie users   4096 2006-09-13 04:31 ..
-rw-r--r--  1 arie users  17588 2006-09-13 05:11 111520.jpg
-rw-r--r--  1 arie users 107062 2006-09-13 05:18 RainForEsTipUU_007.JPG
```

tus gmn cara download pake eggdrop na? pake bind aja.

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
