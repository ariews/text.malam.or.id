---
title: "Teruntuk Ibunda"
date: "2005-06-13T15:32:00+07:00"
---

Ibunda...

Maafkan ananda yang tidak bisa menulis dengan bagus,

yang belum bisa berbakti secara penuh,

Maafkan ananda yang sering berkata ketus di hadapan ibunda, tingkah laku
yang membuat ibunda sedih, marah dan tidak senang.

(meskipun bunda tidak pernah memperlihat kan wajah sedih di hadapan
ananda).

Ibu, tidak banyak yang bisa ananda lakukan di hari ini,

hanya bisa berdoa, memohon kepada Sang Khalik, Sang Esa untuk
keselamatan Ibunda di dunia dan Akhirat.

Selamat Ulang tahun Ibundaku tercinta.
