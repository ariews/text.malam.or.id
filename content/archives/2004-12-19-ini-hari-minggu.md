---
date: "2004-12-19T22:57:00+07:00"
title: "Ini Hari Minggu"
---

Malam sabtu gw emang tidur maleman (jam 12), gw tidur nyenyak.. malam
minggu yang menurut gw.. rada gmn yah, setiap malam minggu gw pengen di
samping ce gw sebenernya. cuman dia lagi gak bener koneksinya, bentar2
dc, atau mati lampu. belakangan dia sering mati lampu.

Pagi, gw tidur terlalu nyenyak! sampe gw gak sadar kalo bangun jam 11an,
gak sadar di jam 7 gw matiin alarm hp. Gwa bener2 tidur lelap. Jam 11 gw
bangun, liat hp ada sms, sama list miscall, sms dari ochie, rani (ade
gw). sama miscall dari rani, nyokap, kaka gw yang di sukabumi.

Kalo ade gw bilang: Kapan ke kostan? katanya mo benerin printer.

Bini gw bilang: 1. [sms 1] blom bangun yah? 2. [sms 2] hiks, di tefon
koq masih bobok.

Demi ngebaca sms dari ochie, gw langsung bangun, nanya anak2 apa tadi
ada tefon? mereka jawab iya. Rius yang ngomong.

> Kang tadi ada tefon dari yayangnya.

gw langsung ol (kagak mandi dulu). tus masuk cenel. di sana dah ada
ochie, begitu liat gw ol dia (kebetulan mau) brb, katanya mo cari makan,
selama dia brb, gw mandi, makan. Tus mulai ngobrol2, dari sms sampe
scramble. Terus terang gw ngerasa.. gmn yah.. ochie banyak diem. Tapi
dia bilang, gw yang banyak diem. Dan kesel sama gw. Banyakk gw salah
nya.

- Kagak Upload poto..
- Tidur terlalu malem.. dan
- kenapa Mereka (anak2 warnet) kagak bangunin gw pas dia tefon.

Gw coba jelasin sattu2.. tapi tetep aja kan chie, ochie na diem. kan gw
jadi ikut2an diem. sampe kita emang bener2 ribut masalah itu. "DIEM".

Ok, kita baekan lagi, dan gw bilang gw mo ke tempat adek jam 2. Babak 1
beres!

Seabis dari tempat Rani, nyatanya cuman berntar aja. gw balik lagi
ceting. Sekarang gw ceting di no 7. Ada si
[joko](http://ezie.spunge.org/?s=joko+pocong) di samping gw. Kita
tadinya ngobrol biasa aja, tapi nyatanya gw yang banyak diem (gw sambil
ngobrol sama joko), nyadar kalo gw banyak diem, gw minta joko na pindah.
dan emang bener, ochie bilang: lagi ngapain? koq diem? 3 menit baru di
jawab.

Gw jelasin kalo tadi ada joko, dan gw ngobrol sama dia, sengaja dia gw
minta pindah biar gw bisa lebih enak ngobrol sama ochie. Ochie bilang:
terusin dulu ngobrolnya.

Loh, kan dah gw suruh pindah si joko na, biar enak ngobrol. Terus
terang, pengen rasanya marah, gw tahan, sampey tiba2 dia bilang: kenapa
sih selalu ada alasan buat badmood?

kenapa sayang, kenapa ngomong gitu? terus terang gw marah. GW MARAH! dah
gw OFF.

> I've got a bike. You can ride it if you like. It's got a basket, a 
> bell that rings and Things to make it look good. I'd give it to you 
> if I could, but I borrowed it. You're the kind of girl that fits in 
> with my world. I'll give you anything, ev'rything if you want 
> things.
> 
> _Bike by Pink Floyd_

Sebait lirik lagu dari [pink floyd](http://www.pinkfloyd.co.uk)
keknya kedengeran jelasss banget, bener koq
[chie](http://ezie.spunge.org/?s=ochie). Terus pas ke warungna pak
moes.. ada peterpan yang lagi nyanyi.

> Sudah maafkan aku segala salahku, Dan bila kau tetap bisu ungkapkan 
> salahmu, Dan aku sifatku dan aku khilafku, Dan aku cintaku dan aku 
> rinduku
>  
> Sudah lupakan semua segala berubah, Dan kita terlupa dan kita 
> terluka, Dan aku sifatku dan aku khilafku, Dan aku cintaku dan aku 
> rinduku
>  
> Kutanya malam, Dapatkah kau lihatnya perbedaan, Yang tak 
> terungkapkan, Tapi mengapa kau tlah berubah, Ada apa denganmu

Kadang, sangat susah buat ngerti. Chie.. kamu bukan pandora's box kan?
yang biar kamu ngomong itu mesti pake kunci (yang mungkin gw blom tau).

Berarti bener apa kata [pito](http://ezie.spunge.org/?s=pito), kalo
ternyata gw itu kurang peka.

> ...tuz lo juga ga peka sama skali...

apapun.. apapun itu.. gw ini emang bego.. gw tolol! gw gak peka :( dan
satu lagi... APAPUN YANG ADA, KELAKUAN GW SELALU SALAH!
