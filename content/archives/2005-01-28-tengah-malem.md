---
title: "tengah malem"
date: "2005-01-28T03:14:00+07:00"
tags:
    - Tools
---
![bloggar0](http://x3.freeserverhost.net/seepicts.php?img=/wbloggar2.png)

Gw baru bangun tidur neh, gak tau kenapa tumben aja kebangun, biasanya
nyenyak, blom bisa tidur lagi. enaknya ngapain yah? cari2 hostingan buat
jess, cuman (hehehhe) lagi rada2 males, bukan apa2, disini gw lagi
kegerahan, padahal ini malem loh! sekalian ini gw posting pake
[w.bloggar](http://wbloggar.com). yah… kenapa gal ngebahas w.bloggar
aja..

Ok deh.. kita mulai

![bloggar1](http://x3.freeserverhost.net/seepicts.php?img=/_wbloggar.jpg)

w.bloggar bisa kita [download](http://wbloggar.com/download/) gratis
dari webnya, dengan tools ini kita bisa ngeposting blog tanpa harus
login ke web blog. w.bloggar support banyak blog, kek
[blogger](http://www.blogger.com/), [wordpress](http://wordpress.org),
[b2](http://www.cafelog.com/), [MT](http://www.movabletype.org/) dan
banyak lagi.

Sangat mudah menggunakan software buatan brazil ini. yang perlu kita
lakukan hanya:

1.  run w.bloggar
2.  masukan ID kita
3.  ketik artikel, jurnal atau curhatan kita :P
4.  post!

Simple kan? Tapi sebelomna kita download dulu lah, dan nyatanya ada
beberapa yang mesti kita setting dulu pada saat pertama kali add user.

1.  Kita akan disuruh memilih jenis blog yang kita gunakan, apakah itu
    blogger, movabletype, b2 atau yang lainnya.
2.  Isikan ID kita (sesuai dengan ID login).
3.  Isikan host (buat yang blognya dari blogger, bigblogtool, blogalia,
    Upsaid, TheBlog, EraBlog, BlogCity, LiveJournal atau TypePad skip
    langkah ini)
4.  OK

Oh yah.. postingan untuk wordpress, gw pake setting b2, karena wordpress
sendiri pengembangan dari b2. jadi gw pilih b2 dan tinggal setting nama
host na aja. cuman postingan ntar masuk ke postingan default aja, gak
bisa di pilih2.

BTW, koq gw perasaan panas banget yah.. padahal orang2 di sekitar gw
pada pake baju anget.. huhuhu gak kerasa dah jam 3.14 pagi! Gambar di
buat pake [Gimp](http://www.gimp.org) versi windows
