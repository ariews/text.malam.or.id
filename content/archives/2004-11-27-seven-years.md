---
date: "2004-11-27T02:41:00+07:00"
title: "Seven Years"
tags:
    - Lyrics
---

![Tigerlily](/images/NatalieMerchantTigerlily.jpeg)

How did I love you?  
There was no measuring  
Far above this dirty world  
Far above everything  
In your tower over it  
You were clean  

So warm & insightful  
Were you in my eyes  
I was sure the rightful  
Guardian of my life  
Damn you betrayer  
How you lied  

But for seven years  
You were loved  
I laid golden orchid crowns  
Around your feet  

For seven years  
I bowed down  
To touch the ground  
So wholly your devotee  

You were  
All I could see  

I've got my sight now  
I see everything you hid  
So don't you try to right now  
All the wrong you did  
I might forget you  
But not forgive  

But for seven years  
You were loved  
I laid golden orchid crowns  
Around your feet  

For seven years  
I bowed down  
To touch the ground  
So wholly your devotee  

For seven years  
You were so revered  
I made offerings of  
Anything and everything I had  

You were  
All I could see  

_by Natalie Merchant, Tigerlily_
