---
title: "Cikaracak"
date: "2005-01-28T17:19:00+07:00"
---
Ti internet:

> kailhaman ku kang oman, perkara revitalisasi budaya.
>
> kuring jadi ras inget kana paribasa “cikaracak ninggang batu,
> laun-laun jadi legok”. ku mj mah pernah diplesetkeun, jadi “lila-lila
> caina beah” hehe. duka pedah mindeng pagiling gisik jeung perkara
> itung-itungan, model-modelan, jeung gambar-gambaran kitu, aya jorojoy
> inget kana paribasa cikaracak tea.
>
> diimpleng-impleng, sabenerna eta paribasa ngandung ajen ilmiah, sarta
> bisa dijadikeun ilham pikeun panalungtikan. paling heunteu aya dua
> widang anu bisa ngabahas eta paribasa teh. kahiji widang elmu ngitung
> alias matematika. kadua widang model-modelan dina komputer grafik.
>
> dina widang model-modelan komputer grafik, eta cikaracak teh bisa
> domodelkeun jadi lengkungan bezier, b-spline, jeung sabangsana. pikeun
> ngagambarkeun kumaha ngarambatna cai ti luhur ka handap. ngayarayap
> sapanjang lengkungan tea. dina widang itung-itungan baris ngahasilkeun
> model “persamaan differensial”.
>
> jigana ku hiji paribasa “cikaracak” bakal ngahasilkeun sababaraha
> sarjana matematika jeung komputer. malah bisa ngahasilkeun doktor
> sagala.
>
> salam,
>
> mh —\| tina milis
> [kisunda](http://www.mail-archive.com/kisunda@yahoogroups.com/msg00282.html)

sing sabar arie… cikaracak maneh teh !! sing tabah…
