--- 
date: "2014-12-17T17:45:00+07:00"
title: "Kenapa pake VPS murah?"
tags:
    - VPS
---

![Gambar Server](/images/vps-server.jpg)

Ada temen gw nanya

> Kenapa rie pake VPS murah? kan banyak hosting lain yg lebih bagus dan
> lumayan murah?

Salah satu jawabannya sih, sudah pasti duit ya, untuk
[VPS](http://en.wikipedia.org/wiki/Virtual_private_server) yg gw pake
skr, gw pake INIZ, bisa lo cari di [LEB](http://lowendbox.com/). Gak
perlu yg bagus juga apalagi yg mahal, bukan berarti gw gak mau ya. Tapi
karena ini cuman blog pribadi, pengunjungnya juga cuman bot dari `search
engine`, itu pun cuman beberapa aja :).

```text
# awk '{print $1}' /var/www/arie.malam.or.id/access.log | \
  wc -l
79
```

See, cuman 79 pengunjung sejak 6 hari lalu :D.

Terus kenapa harus VPS? Ya soalnya gw butuh beuat belajar aja sih,
seting server, config ini-itu, kalo gagal ya install ulang aja. Di
tempat hostingan kan gak bisa gitu :)
