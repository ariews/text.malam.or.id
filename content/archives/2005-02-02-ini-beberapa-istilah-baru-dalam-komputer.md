---
title: "Ini beberapa istilah baru dalam komputer... ;)"
date: "2005-02-02T12:09:00+07:00"
---

Dari pito via email:

-   Ejakulasi Dini:

    Peristiwa dimana komputer terlalu cepat keluar dari programnya.
    Ketika komputer sedang diaktifkan, tiba-tiba sudah exit dan tidak
    bisa melanjutkan programnya.

    Peristiwa ini sangat dirisaukan oleh pengguna komputer. Biasanya
    pengguna komputer akan mencari orang yang pakar dalam mengatasi
    masalah ini.

-   Menopause:

    Komputer generasi lama WS dan Lotus yang sudah tidak bisa diaktifkan
    lagi. Biasanya komputer jenis ini sudah ditinggalkan penggunanya.
    Jika dipaksakan komputer jenis ini, tidak akan menghasilkan produk.

-   Orgasme:

    Adalah rasa senang yang dialami pengguna komputer karena terlalu
    asyik dan larut dalam menggunakan komputer. Perasaan ini bisa
    membuat pengguna komputer lupa daratan dan tentunya dapat mengurangi
    produktifitas dan efektifitas etos kerja.

-   Penetrasi:

    Adalah istilah yang populer ketika seseorang memasukan disket ke
    dalam drive A atau drive B. Setelah d isket dimasukan, maka akan
    terjadi interaksi didalam CPU. Disket akan mengeluarkan program
    sesudah dimasukan ke drive A atau B.

-   Masturbasi:

    Adalah kelainan komputer dalam mengeluarkan program karena menyalahi
    prosedur yang lazim. Biasanya ini terjadi pada komputer yang jarang
    sekali dipakai oleh pengguna komputer. Untuk menghindarkan hal ini,
    sebaiknya komputer sering diaktifkan.
