---
title: "NEW target!!"
date: "2005-05-31T15:27:00+07:00"
---

huhu, ternyata ada yg laen selain target 1000 besar (target bulan skr),
gw pengen target gw minggu ini (minggu ini) adalah karma mencapai 750.

Karma yg tinggi memungkinkan kita untuk menggunakan artefak 2x dalam
24jam, berbeda dengan skr, gw hanya bisa mengguakan 1x aja, jadi kalo gw
pake [European Court of
Justice](http://blogshares.com/industries.php?id=231) atau gw pake
[Concerto Grosso](http://blogshares.com/industries.php?id=100)
([artefak](http://blogshares.com/user.php?id=28645&view=artefacts)
termahal gw saat ini :P) bisa masing2 2x. berarti sehari dapat
7-8milyaran lah :P (maximal).

Oh ya, mengenai warnet, dah berapa hari ini xandros gw sering error,
[eddie](http://ediotz.name) sampe 2x/3x install. makanya skr ampir
setiap pagi kalo gw mo chatting, mesti install xchat dolo.

```text
jz@Planet-0:~$ su
password:
root@Planet-0:/home/jz# apt-get install xchat
...
root@Planet-0:/home/jz# logout
jz@Planet-0:~$
```

huhu berabe2... apalagi ntar pas bagian ada user, billingnya masih
manual! kalo ada print lebih "asik", gmn gak? kalo mo print, printerna
nyalain dulu baru bisa, kalo gak? error tuh.

> **note**: ada yang dah sukses install
> [OpenKiosk](http://openkiosk.sf.net/) di Debian/xandros? pls
> [contact](http://ezie.spunge.org/contact/) gw atau [msg di
> thread](http://ezie.spunge.org/1626,2005,05,31/#respond) ini.
