---
title: "Untuk kita renungkan bersama....."
date: "2003-10-31T10:45:00+07:00"
tags:
    - Email
    - Friends
---

sebuah email dari temanku (pito): menyambut ramadhan ni... slamad
menikmati...

Siang menjelang dzuhur...salah satu Iblis ada di Masjid.

Kebetulan hari itu hari Jum'at, saat berkumpulnya orang. Iblis sudah ada
dalam Masjid. Ia tampak begitu khusyuk. Orang mulai berdatangan. Iblis
menjelma menjadi ratusan bentuk & masuk dari segala penjuru, lewat
jendela, pintu, ventilasi, atau masuk lewat lubang pembuangan air. Pada
setiap orang, Iblis juga masuk lewat telinga, ke dalam syaraf mata, ke
dalam urat nadi, lalu menggerakkan denyut jantung setiap para jamaah
yang hadir. Iblis juga menempel di setiap sajadah. "Hai, Blis!", panggil
Kiai, ketika baru masuk ke Masjid itu. Iblis merasa terusik : "Kau
kerjakan saja tugasmu, Kiai. Tidak perlu kau larang-larang saya. Ini hak
saya untuk mengganggu setiap orang dalam Masjid ini!", jawab iblis
ketus.

"Ini rumah Tuhan, Blis! Tempat yang suci,Kalau kau mau ganggu, kau bisa
diluar nanti!", Kiai mencoba mengusir.

"Kiai, hari ini, adalah hari uji coba sistem baru". Kiai tercenung.
"Saya sedang menerapkan cara baru, untuk menjerat kaummu". "Dengan apa?"
"Dengan sajadah!" "Apa yang bisa kau lakukan dengan sajadah, Blis?"
"Pertama, saya akan masuk ke setiap pemilik saham industri sajadah.
Mereka akan saya jebak dengan mimpi untung besar. Sehingga, mereka akan
tega memeras buruh untuk bekerja dengan upah di bawah UMR, demi
keuntungan besar!" "Ah, itu kan memang cara lama yang sering kau pakai.
Tidak ada yang baru,Blis?" "Bukan itu saja Kiai..." "Lalu?" "Saya juga
akan masuk pada setiap desainer sajadah. Saya akan menumbuhkan gagasan,
agar para desainer itu membuat sajadah yang lebar-lebar" "Untuk apa?"

"Supaya, saya lebih berpeluang untuk menanamkan rasa egois di setiap
kaum yang Kau pimpin, Kiai! Selain itu, Saya akan lebih leluasa, masuk
dalam barisan sholat. Dengan sajadah yang lebar maka barisan shaf akan
renggang. Dan saya ada dalam kerenganggan itu. Di situ Saya bisa ikut
membentangkan sajadah".

Dialog Iblis dan Kiai sesaat terputus. Dua orang datang, dan keduanya
membentangkan sajadah. Keduanya berdampingan. Salah satunya, memiliki
sajadah yang lebar. Sementara, satu lagi, sajadahnya lebih kecil. Orang
yang punya sajadah lebar seenaknya saja membentangkan sajadahnya, tanpa
melihat kanan-kirinya. Sementara, orang yang punya sajadah lebih kecil,
tidak enak hati jika harus mendesak jamaah lain yang sudah lebih dulu
datang. Tanpa berpikir panjang, pemilik sajadah kecil membentangkan saja
sajadahnya, sehingga sebagian sajadah yang lebar tertutupi sepertiganya.
Keduanya masih melakukan sholat sunnah.

"Nah, lihat itu Kiai!", Iblis memulai dialog lagi. "Yang mana?" "Ada dua
orang yang sedang sholat sunnah itu. Mereka punya sajadah yang berbeda
ukuran. Lihat sekarang, aku akan masuk diantara mereka".

Iblis lenyap. Ia sudah masuk ke dalam barisan shaf. Kiai hanya
memperhatikan kedua orang yang sedang melakukan sholat sunah. Kiai akan
melihat kebenaran rencana yang dikatakan Iblis sebelumnya. Pemilik
sajadah lebar, rukuk. Kemudian sujud. Tetapi, sembari bangun dari sujud,
ia membuka sajadahya yang tertumpuk, lalu meletakkan sajadahnya di atas
sajadah yang kecil. Hingga sajadah yang kecil kembali berada di
bawahnya. Ia kemudian berdiri. Sementara, pemilik sajadah yang lebih
kecil, melakukan hal serupa. Ia juga membuka sajadahnya, karena
sajadahnya ditumpuk oleh sajadah yang lebar. Itu berjalan sampai akhir
sholat.

Bahkan, pada saat sholat wajib juga, kejadian-kejadian itu beberapa kali
terihat di beberapa masjid. Orang lebih memilih menjadi di atas,
ketimbang menerima di bawah. Di atas sajadah, orang sudah berebut
kekuasaan atas lainnya. Siapa yang memiliki sajadah lebar, maka, ia akan
meletakkan sajadahnya diatas sajadah yang kecil. Sajadah sudah dijadikan
Iblis sebagai pembedaan kelas. Pemilik sajadah lebar, diindentikan
sebagai para pemilik kekayaan, yang setiap saat harus lebih di atas dari
pada yang lain. Dan pemilik sajadah kecil, adalah kelas bawah yang
setiap saat akan selalu menjadi sub-ordinat dari orang yang berkuasa. Di
atas sajadah, Iblis telah mengajari orang supaya selalu menguasai orang
lain.

"Astaghfirullahal adziiiim ", ujar sang Kiai pelan.
