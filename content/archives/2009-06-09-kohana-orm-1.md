--- 
date: "2009-06-09T22:54:00+07:00"
slug: "kohana-orm-1"
title: "Kohana ORM #1"
tags:
    - Kohana
    - Tutorial
    - ORM
---

Penggunaan ORM di Kohana relative mudah. Misalkan untuk table `users`.
kita tinggal bikin model dengan nama `User_Model`, dan model ini extends
ke ORM class. Save dengan nama `user.php`.

```php
class User_Model extends ORM {}
```

untuk menambahkan entry baru, kita pake:

```php
$user = new User_Model();
$user->name = 'arie';
$user->addr = 'bandung';
$user->save();
```

`$user = new User_Model();` bisa juga kita ganti dengan: `$user = ORM::factory('user');`

untuk load dan lalu edit juga mudah banget.

```php
$user = new User_Model(ID); //
$user->name = 'omgphp';
$user->save();
```

Untuk delete, kita tinggal pake:

```php
$user = new User_Model(ID);
$user->delete();
```

Mudah bukan?
