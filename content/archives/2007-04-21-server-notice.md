---
title: "Server notice"
date: "2007-04-21T04:31:00+07:00"
tags:
    - Tutorial
    - IRC
    - BitchX
---

*Mode* `+s` (`snotice`) di
[irc](http://en.wikipedia.org/wiki/Internet_Relay_Chat) sangat
berguna untuk mengetahui 'apa yg terjadi di server', tapi untuk yg gak
memerlukannya gak usah pake, karena bisa banyak *notice*. Jika tidak
dipisahkan pada window tertentu, hal ini lumayan mengganggu.

Pada [mIRC](http://mirc.com), kita bisa menggunakan *script*
sederhana seperti ini:

```text
On ^*:SNOTICE:*:{
  if ($window(@snotice) == $null) { window @snotice }
  if ($network) {
    var %snot.srv $network
    var %snot.color $calc( $len($network) % 16 )
  }
  else {
    var %snot.srv $server
    var %snot.color $calc( $len($server) % 16 )
  }
  echo @snotice [ $+ %snot.color $+ %snot.srv $+ ]: $1-
  haltdef
}
```

*script* di atas akan membuat semua `snotice` ditampilan pada *window*
`@snotice`.

Beda lagi dengan [BitchX](http://bitchx.org), kita bisa menggunakan
``operview`` (``/toggle operview``). Sedanglan pada
[X-Chat](http://xchat.org), *notice* dari server udah dipisahin pada
*window* tersendiri.

[![BitchX Operview 1](/images/5029643f632b1bitchxoperview1th.png)](/uploads/bitchxoperview1.png)

[![BitchX Operview 2](/images/502964400ed63bitchxoperview2th.png)](/uploads/bitchxoperview2.png)
