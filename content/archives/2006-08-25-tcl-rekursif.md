---
title: "tcl rekursif"
date: "2006-08-25T13:03:00+07:00"
tags:
    - Eggdrop
---

kadang kita pengen bikin list dari sebuah direktori dan sub-direktori di
bawahnya, ini ada script kasar, mungkin ada yg bisa mempersingkat....

```tcl
proc mydir {dirs} {
    foreach a [glob -nocomplain "$dirs/*"] {
        if { [file isdirectory $a] } {
            mydir $a
        } else {
            puts $a
        }
    }
}
proc scanalldir { alldirs } {
    foreach dir $alldirs {
        mydir $dir
    }
}
```

pemakaian:

`scanalldir /home/gwcowok/mp3`

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
