---
title: "SmileyXtra"
date: "2005-01-26T21:25:00+07:00"
tags:
    - Friends
    - Tools
---

[jeZz](http://itzjuztme.blogspot.com) u should read this!

gaya bener ya atasnya :P tapi emang bener koq, ini cuman buat jess doang
*\[tentu sama orang laen juga yang butuh smiley di blog na, terutama
yang pake \`firefox\`\_\]* jess kan pengen ada smile di blog nya.. bener
kan?? nah ini mungkin bisa ngebantu.

1.  Run Firefox
2.  Brows ke [Firefox
    Extention](https://addons.update.mozilla.org/extensions/?os=Windows&application=firefox).
3.  cari [SmileyXtra](http://smile.studio-network.co.uk/), atau buka aja
    langsung ke [tempat
    installnya](https://addons.update.mozilla.org/extensions/moreinfo.php?id=375).
4.  pilih install
5.  Restart Firefox *\[tutup firefox, dan run kembali\]*
6.  pilih menubar Tools -&gt; Extentions
7.  klik kanan pada SmileyXtra, dan pilih Option.
8.  klik Update Database.
9.  pilih menubar View -&gt; Toolbars -&gt; Costumize
10. drag icon SmileXtra ke toolbars. close costumize.
11. klik icon SmileXtra tadi, dan di sana dah banyak smile yang kamu
    mau.

**TEST**

![Ninja](/images/502963f025e00numchuks.gif)
![image1](/images/502963f139e96botando.gif)
![image2](/images/502963f212598animal20.gif)
![image3](/images/502963f28167f209069.gif)

**SELAMAT MENCOBA!!!**
