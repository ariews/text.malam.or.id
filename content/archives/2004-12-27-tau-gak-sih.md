---
date: "2004-12-27T09:32:00+07:00"
title: "tau gak sih?"
---

Eh kalian tau gak sih? kalo gw sekarang lagi mendaki? gw kek yang jadi
[bahan lagu]({{< ref path="2004-11-27-seven-years.md">}}) si
natalie merchant.

> How did I love you?  
> There was no measuring  
> Far above this dirty world  
> Far above everything  
> In your tower over it  
> You were clean

Perasaan gw, gw cukup hati2.. dan gw *bener2*.. gw *bener2* chi.. gw
ngerasa.. kalo jalan yang gw tempuh udah clean.. bersih.. Gw bangun pagi
ini sebenernya dengan perasaan seneng, berharap banyak dapat ngeliat dia
senyum buat gw.. tapi apa yang ku liat?

> So warm & insightful  
> Were you in my eyes  
> I was sure the rightful  
> Guardian of my life  
> Damn you betrayer  
> How you lied

> But for seven years  
> You were loved  
> I laid golden orchid crowns  
> Around your feet
