--- 
date: "2013-03-11T16:23:00+07:00"
title: "Pindah server lagi :)"
tags:
    - VPS
    - Pindah Hosting
    - CopperEgg
---

Woii udah lama neh gak ngepost.. hehe kemana aja sihh?

Gak kemana-mana juga, cuman lagi persiapan buat lahiran anak kedua :)
doain aja yaa...

Btw, hostingan pindah lagi, HAH!! hehe iyaaa.. gw kira sekarang
pagodabox udah cukup buat jadi bahan percobaan-percobaan :)

Saat make *service*nya pagodabox, gw juga *signup* di
[CopperEgg](http://copperegg.com), dan setiap hari gw selalu dapat email
dari si copperegg ini, ngasih report kalo web gw ini berjalan
laaaaaaammmmbbattt. Sebodo amat sih, namanya juga gratisan :D

Tapi lama-lama jenuh juga.. bosen kalo dapat imel yang isinya itu-itu
juga. lambat lah, pingnya di atas 4500ms lah, kadang sampe 6000ms, ya
akhirnya gw pindahin aja ke VPS cadangan.

Hasilnya seperti berikut:

![image0](/images/513da2436a63fLogChannel_restart.jpg)

ya masih gede sih.. tapi kliatan banget kan turunnya drastis :D, baru
pindah server nehh...
