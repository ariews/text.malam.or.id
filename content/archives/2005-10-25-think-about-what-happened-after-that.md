---
title: "Think about what happened after that"
date: "2005-10-25T04:57:00+07:00"
tags:
  - Maenan
  - Games
---

![Demon](/images/5029643557394demon_red-evil.gif)

Dalam dunia mafia, pikir dulu sebelum bertindak, temen gw bilang: 'think
about what happened after that'.

Pertama gw gak sempet mikir, apa artinya. Tentu kita melakukan sesuatu
dengan sebuah ambisi dan perencanaan tertentu, ternyata ada yg perlu di
pikir selain perencanaan dan kemenangan.

Kenyataannya, gw abis nyerang seorang mafioso berinisial B. gak banyak
yg gw dapat, hanya ada beberapa juta dollar dan sebuah jet. Dia anggota
kluarga illuminati (banyak kluarga di mulai dengan 'illuminati'). Apakah
dia mengadu pada anggota kluarga lain atau kepala rumah tangga ngeliat
bahwa si B ini gw serang? gw di serang balik habis2an sama yg laen.

> Gw sekarat!

Lalu gw buka mailbox, sebelom nyerang ada yg invite masuk kluarga
'illuminati' dan berharap setelah kejadian ini gw di terima. Thank God,
gw masih di terima. Masalah pun diluruskan, sebuah pembelaan 'hey br0,
im n00b!'.

Siapa sih yg gw serang? 2 orang kepala 'illuminati' lainnya.

Sebuah pelajaran berharga buat gw. Bukan cuman di dalam game The Mafia
Boss, tapi juga di kehidupan real.

# Allies. All Illuminati Familys. {#allies.-all-illuminati-familys.}
