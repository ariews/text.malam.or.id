---
title: "Autoslaps"
date: "2005-01-14T19:59:00+07:00"
tags:
    - Friends
    - mIRC
---

si HaZaMi (@irc.nullus.net) nanya soal autoslaps, gak tau buat apaan,
mungkin buat iseng, gak tau juga. Gw minta waktu beberapa menit buat
bikin remote script (mIRC) nya, Jadi ini dia:

```text
On ^*:ACTION:*:#:{
  ; check apakah kita ada salah (text) action nya?
  ; dan pastikan bukan kita yang melakukan action
  if (($me isin $1-) && ($nick != $me)) {

    ; ini di pake buat nahan flooding
    if (%sl. $+ $nick != 1) {

      ; lakukan describe (action) ke cenel
      ; ganti nama kita (dalam text) menjadi nick dia
      ; biar gak kena CAPSLOCK, sapa tau kan kepslok semua
      ; kita set ke $lower()
      describe $chan $lower($replace($1- ,$me ,$nick))

      ; si HaZaMi pengen ngekick juga kalo dia kena slaps
      .kick $chan $nick auto slaps

      ; kasih timer, biar gak jadi flood
      .timerrsl $+ $nick 1 20 { .unset %sl. $+ $nick }

      ; kasih tanda kalo kita udah ngelakuin autoslaps
      ; di pake buat autoslaps berikutnya
      set %sl. $+ $nick 1
    }
  }
}
```

selain itu gw juga ngebikin alias buat ngebalikin kata2, misal kata `hello` jadi `olleh`

```text
alias balik {
  ; pertama kita definisikan dulu beberapa variable
  var %text = $1-, %i = 0, %j = $len(%text)

  ; unset %balik (kalo ada)
  unset %balik

  ; loop! di pake buat ambil satu persatu karakter dari text
  ; yang akan kita balikan
  while (%i < %j) {

    ; jika karakter spasi: kita ganti karakter spasi ( $char(32) )
    ; dengan pengganti sementara
    if ($mid(%text,$calc( %j - %i ),1) == $chr(32)) {
      %_balik = Ã‚Â¬?Ã‚Â¬
    }

    else {
      set %_balik $mid(%text,$calc( %j - %i ),1)
    }

    ; satukan variable %_balik ke %balik
    set %balik %balik $+ %_balik
    inc %i
  }

  ; variable %balik sudah utuh
  var %_msg = $replace(%balik, Ã‚Â¬?Ã‚Â¬, $chr(32))

  ; send output ke windows active, atau echo jika kita tidak konek
  ; ke server
  $iif($server,say %_msg,echo -s %_msg)
  unset %balik
}
```
