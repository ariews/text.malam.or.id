--- 
date: "2009-06-09T20:36:00+07:00"
title: "Spam count"
tags:
    - Spam
---

Jumlah comment spam skr ada 38, halah!! padahal masih terbilang baru ini
blog nya _(humm.. maksudnya baru relaunch :P)_

Untuk mengatasi comment spam itu, kemaren gw pake PHP5
[Akismet](http://akismet.com/) (bisa di liat di:
[sini](http://www.achingbrain.net/stuff/php/akismet)).

Cara pake nya mudah ya.. ini gw copy paste dari halamannya:

```php
$WordPressAPIKey = 'aoeu1aoue';
$MyBlogURL = 'http://www.example.com/blog/';

$akismet = new Akismet($MyBlogURL, $WordPressAPIKey);
$akismet->setCommentAuthor($name);
$akismet->setCommentAuthorEmail($email);
$akismet->setCommentAuthorURL($url);
$akismet->setCommentContent($comment);
$akismet->setPermalink('http://www.example.com/blog/alex/someurl/');
if($akismet->isCommentSpam())
    // store the comment but mark it as spam (in case of a mis-diagnosis)
else
    // store the comment normally
```

Untuk mendapatkan API Key, bisa kita dapat saat daftar ke
[wordpress.com](http://wordpress.com)
