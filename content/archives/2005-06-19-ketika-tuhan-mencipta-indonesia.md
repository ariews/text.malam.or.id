---
title: "Ketika Tuhan mencipta indonesia...."
date: "2005-06-19T13:52:00+07:00"
tags:
  - Humor
---

Suatu hari Tuhan tersenyum puas melihat sebuah planet yang baru saja
diciptakan-Nya. Malaikat pun bertanya, "Apa yang baru saja Engkau
ciptakan, Tuhan?"

"Lihatlah, Aku baru saja menciptakan sebuah planet biru yang bernama
Bumi," kata Tuhan sambil menambahkan beberapa awan di atas daerah hutan
hujan Amazon.

Tuhan melanjutkan, "Ini akan menjadi planet yang luar biasa dari yang
pernah Aku ciptakan. Di planet baru ini, segalanya akan terjadi secara
seimbang.

Lalu Tuhan menjelaskan kepada malaikat tentang Benua Eropa.

Di Eropa sebelah utara, Tuhan menciptakan tanah yang penuh peluang dan
menyenangkan seperti Inggris, Skotlandia dan Perancis. Tetapi di daerah
itu, Tuhan juga menciptakan hawa dingin yang menusuk tulang.

Di Eropa bagian selatan, Tuhan menciptakan masyarakat yang agak miskin,
seperti Spanyol dan Partugal, tetapi banyak sinar matahari dan hangat
serta pemandangan eksotis di Selat Gibraltar.

Lalu malaikat menunjuk sebuah kepulauan sambil berseru, "Lalu daerah
apakah itu Tuhan?"

"O, itu," kata Tuhan, "itu Indonesia. Negara yang sangat kaya dan sangat
cantik di planet bumi. Ada jutaan flora dan fauna yang telah Aku
ciptakan di sana. Ada jutaan ikan segar di laut yang siap panen. Banyak
sinar matahari dan hujan. Penduduknya Ku ciptakan ramah tamah, suka
menolong dan berkebudayaan yang beraneka warna. Mereka pekerja keras,
siap hidup sederhana dan bersahaja serta mencintai seni."

Dengan terheran-heran, malaikat pun protes, "Lho, katanya tadi setiap
negara akan diciptakan dengan keseimbangan. Kok Indonesia baik-baik
semua. Lalu dimana letak keseimbangannya?"

Tuhan pun menjawab dalam bahasa Inggris, "Wait, until you see the idiots
I put in the government."

dari milis alumnismasukabumi
