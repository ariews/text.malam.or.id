---
title: "tentang wortel, kopi & telur"
date: "2005-05-27T22:26:00+07:00"
---

Terus terang saja, mungkin gw blom begitu bersemangat buat nulis,
mungkin karena beberapa hal:

1.  Gw baru sembuh dari sakit.
2.  Banyak pekerjaan yang menyita waktu.
3.  Masih bingung dan masih mencari2 billing warnet buat OS linux
4.  Bagaimana menerangkan kepada user warnet bahwa linux lebih mudah
    dari dulu.
5.  Kenapa sih gw gagal terus dalam install
    [OpenKiosk](http://openkiosk.sf.net/)?
6.  Susahnya menyusun kata2 (padahal sebelom gw sakit, gw banyak bahan
    untuk di tulis)

Gpp!! gw coba ceritain...

Tapi mungkin gak banyak yang akan di ceritakan. Salah satunya adalah
[komentar](http://ezie.spunge.org/1619,2005,05,21/#comment-1534)
dari [wandi](http://samuderacinta.blogspot.com) tentang [tulisan
dari ochie](http://ezie.spunge.org/1619,2005,05,21/).

Gw sendiri blom tau ya, ochie dapat menulis sendiri atau dari
[airputih](http://www.airputih.tk)
(http://groups.yahoo.com/group/airputih/). Tepat sama dengan
[postingan](http://groups.yahoo.com/group/airputih/message/535) dari
[alfin_baby](http://profiles.yahoo.com/alfin_baby) (tgl. May 18,
2005 7:09 am) . Ochie bilang ke gw katanya dia dapat dari testimonial
LC, gw sendiri blom tau LC (live connector) yang mana, karena google
banyak sekali mempunyai kata [live connector](http://www.google.co.id/search?q=%22live+connector%22&start=0&start=0&ie=utf-8&oe=utf-8&client=firefox-a&rls=org.mozilla:en-US:official).
Tentu gak akan enak di hati jika itu benar hasil karya tulis dari
alfin baby.

Tapi ternyata, gw gak bisa bilang apa2 lagi ketika gw coba2 nyari di
google dengan key: [Seorang anak mengeluh pada](http://www.google.co.id/search?q=%22Seorang+anak+mengeluh+pada%22&start=0&start=0&ie=utf-8&oe=utf-8&client=firefox-a&rls=org.mozilla:en-US:official)
hasilnya lebih dari yang kita duga. terdapat banyak!!

Bahkan salah satunya dari ILUNI GP FTUI
([link](http://www.gp.iluni.org/renungan.php?id=29)) tertulis di
sana tgl 02-08-2002. jelas jauh banget dengan yang di tulis ochie atau
alfin baby. Atau dari <http://www.husna.org> ([google
cache](http://66.102.7.104/search?q=cache:fIhOz7yVelcJ:www.technorati.com/tag/isnaini+%22Seorang+anak+mengeluh+pada%22&hl=id&client=firefox-a),
link post: <http://www.husna.org/archives/2005/04/25/air-mendidih/>,
skr sudah gak ada lagi)
![image0](http://x3.freeserverhost.net/seepicts.php?img=/snps.png) atau
dari milis anak [Teknik Kimia ITB
98](http://groups.yahoo.com/group/tk98/message/1069) tertanggal 9
oktober 2002, dan banyak lagi yang lain. Jadi bukan hanya postingan
ochie saja yng sama/mirip dengan airputih, tapi banyak... bahkan sebelum
ada airputih tulisan itu sudah ada.

Jujur aja, koq gw jadi penasaran ya (logika)? wajar jika postingan ochie
di bilang mirip sama airputih (alfin baby lebih dlu ngeposting) tapi
kenapa wandi gak mengatakan hal serupa tentang "koq tulisan alfin mirip
ya sama yang ada di milis Teknik Kimia 98 ITB" --yang tadi gw sebut--.
Atau bahwa alfin baby adalah
[arif036](http://profiles.yahoo.com/arif036) yang menulis di milis
tersebut? atau dia adalah yang posting di [ILUNI
GP](http://www.gp.iluni.org/renungan.php?id=29)? atau
[Deedat](http://www.activeboard.com/forum.spark?forumID=5184&subForumID=5179&action=viewTopic&commentID=664220&subForumID=5179)
mungkin? atau memang alfin baby adalah benar yang menulis pertama kali
tulisan tersebut dan di post ulang di airputih, *who know*?

Dunia itu aneh!

Maaf, mungkin ochie melakukan kesalahan dalam postingannya (tidak di
cantumkan dari mana dia mendapat bahan tulisannya itu).

Exodus masal para pemilik warnet keknya bisa jadi sebuah topik yang
hangat di bandung ini, gmn gak? hampir semua (gw yakin semua, kecuali
nekat :p) warnet tutup beberapa hari di waktu lalu, dan itu termasuk
warnet ini. exodus dari windows ke linux ini banyak di lakukan dan bukan
di bandung saja, juga di jakarta, bogor, jawa tengah, yoga, dan daerah
lainnya. Issue yang di dapat dari milis Awari.

Sekarang warnet pake linux.

*BTW*, berawal dari blognya [kusaeni](http://kusaeni.com), skr gw
ubah ukuran dari
[gravatar](http://www.gravatar.com) nya
menjadi 50pixel, karena kalo di liat koq jadi berantakan yah? Jadilah
ukurannya di ganti.

Agak rapihan dikit kan?
