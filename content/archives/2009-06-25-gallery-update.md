--- 
date: "2009-06-25T01:08:00+07:00"
title: "Gallery Update"
---

Akhirnyaa.............

Keinginan gw yg pengen pasang carousel di gallery kesampean juga,
setelah bersemedi sambil di temenin sama mbah gugel, kelar juga.

Gw pake [jQuery Carousel](http://sorgalla.com/jcarousel/), sempet
kebingungan dengan `next`, `prev` nya, soalnya gw pake
[picasaweb](http://code.google.com/apis/picasaweb/overview.html) yg
cuman ada 1 parameter `start-index` aja, beda dengan sample yang dikasih
sama jquery carousel.

Tapi pikir2, next/prev juga sama aja seperti pake pagging, ya udah
akhirnya kelar deh :)

Tinggal gw tambahin di album method: `request::is_ajax()`, kelaar deh.
