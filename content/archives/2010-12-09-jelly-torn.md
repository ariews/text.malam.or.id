--- 
date: "2010-12-09T06:00:00+07:00"
title: "Jelly Torn"
tags:
    - Kohana
    - Jelly
    - Kohana modules
    - Formo Jelly
    - Jelly Torn
    - Kohana Jelly
---

Gw baru tau (sebenernya hari sabtu kemaren taunya) ada [Jelly
Torn](https://github.com/TdroL/kohana-jelly-torn), itu pun ga sengaja
nemunya, ketemu dari [Kohana Modules](http://kohana-modules.com).

Katanya sih Form generator Khusus buat
[Jelly](http://github.com/jonathangeiger/kohana-jelly). Kliatanya bagus,
lom sempet coba sih, ntar gw coba2 dulu, dan gw pengen tau, apa bedanya
jengan [Formo-Jelly](https://github.com/bmidget/kohana-formo-jelly).
