---
title: "BloGTK"
date: "2005-07-10T02:36:00+07:00"
tags:
  - Tools
---

Gw lagi test ngepost pake [BloGTK](http://blogtk.sourceforge.net/)
apakah bisa atau gak? spoalna ini masih versi yang lumayan lama (v1.0).

Untuk Xandros ada yang harus di lakukan jika blom di update :P:

Update Source.

Add baris berikut pada file `/etc/apt/source.list`

```text
deb http://xnv3.xandros.com/3.0/pkg unsupported3.0-xn main contrib non-free
```

Dengan root bisa di tik pada konsole:

```text
echo "deb http://xnv3.xandros.com/3.0/pkg unsupported3.0-xn main contrib non-free" >> /etc/apt/source.list
apt-get update
```

Bisa juga melalui XN, dari menu pilih edit--> Set Application Sources--> centang--> Debian unsupported site.

Install BloGTK

Setelah di update source-nya, dengan cara yang sama dengan kedua cara di
atas (melalui konsole atau XN).

1.  melalui konsole--> `apt-get install blogtk` lalu jawab "y" untuk
    mulai installasi.
2.  melalui XN memang cara yang mudah, ada search bar, masukan kata
    kunci "blogtk" (tanpa tanda petik) lalu pilih install.

Untuk yang lain, mungkin bisa install sendiri, dengan cara tradisional
(silahkan baca [FAQ](http://blogtk.sourceforge.net/faq.php)).

Setelah installasi kita bisa mulai menjalakan applikasi ini dengan
perintah: `BloGTK` pada menu RUN, atau biar gampang kita bisa
menyimpannya di Application Menu (gunakan Menu Editor).

Sekarang kita lihat, apakah postingan pertama dengan BloGTK ini
berhasil?
