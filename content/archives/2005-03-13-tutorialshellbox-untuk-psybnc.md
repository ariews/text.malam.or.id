---
title: "tutorial:shellbox untuk psybnc"
date: "2005-03-13T04:31:00+07:00"
tags:
  - Tutorial
---

Gw admin di salah satu server. Gw pegang root, kadang kalo kita
cetingan, ada aja yang bilang "*Eh, add user dong di shell*" Ini
mengartikan bahwa pada server (baca:*box*), memungkinkan kita untuk
mempunyai banyak user.

Berikut adalah contoh user dalam box gw.

```text
router# cat /etc/passwd
root:*:0:0:Charlie &:/root:/bin/csh
daemon:*:1:1:The devil himself:/root:/sbin/nologin
operator:*:2:5:System &:/operator:/sbin/nologin
bin:*:3:7:Binaries Commands and Source,,,:/:/sbin/nologin
...
...
nobody:*:32767:32767:Unprivileged user:/nonexistent:/sbin/nologin
orok:*:72:72:orok:/home/orok:/bin/sh
opiq:*:1001:1001:opiq:/home/opiq:/bin/sh
prince:*:1003:1003:Unprivileged Princessa:/home/prince:/bin/sh
misae:*:1000:998:Misae arigato:/home/misae:/bin/sh
ochie:*:1002:997:ochie:/home/ochie:/bin/sh
pocong:*:1004:1004:pocong:/home/pocong:/bin/sh
mysql:*:1005:0::/home/mysql:/bin/csh
router#
```

Tapi ada juga yang beranggapan, bahwa pada box tidak ada user lain
(???). Anggapan ini adalah salah. Di OS Windows, kita juga bisa
mempunyai banyak user, hanya saja tidak bisa login sekaligus, berbeda
dengan system operasi yang seperti unix (UNIX like). Bahkan kabarnya
bisa mempunyai lebih dari 50000 user!!.

Perintah (command) untuk add user adalah

1.  `adduser -d /path/to/home/dir USER`
2.  `adduser`

gak sama sih, ada yang mesti langsung add sama `$HOME` nya (seperti
RedHat) ada juga yang gak (seperti Slackware, dan typical BSD). Setelah
adduser, kita buka koneksi baru ke box dengan user_baru, sebut aja yang
di atas kita pake user `orok`, atau karena kita masih pake user root
(user dengan hak tertinggi dalam box) kita bisa menggunakan `su` command
(**ingat!!** user `orok` itu berbeda dengan user `pocong`. Meskipun
masih dalam satu box).

Setelah login dengan user `orok`, kita download
[psybnc](http://www.psychoid.lam3rz.de/psyBNC2.3.2-4.tar.gz). Cara
downloadnya sama kek download eggdrop (baca [bikin
eggdrop](http://ezie.spunge.org/369/2004/12/26/bikin-eggdrop/)).

Setelah tarbal diextract (`tar -zxf psyBNC2.3.2-4.tar.gz`) akan di buat
folder dengan nama: `psybnc`, lalu masuk ke dalam foldernya
(`cd psybnc`) dan lakukan `make`.

setelah selesai, kita (optional) edit file `psybnc.conf`, kalo dah
kelar, kita run (`./psybnc`). Kalo nanya crontab, keknya gw [dah
posting](http://ezie.spunge.org/425/2005/02/02/crontab-buat-psybnc/)
deh.

Setelah psybnc kita run. kita login ke psybnc, caranya:

1.  buka irc client (mirc, bitchx, pirch, atau apa aja)
2.  konek ke box dengan port yang telah di tentukan di psybnc.conf.
3.  setting password (ingat! sapa yang duluan konek ke psybnc, dia
    adalah psybnc admin).
4.  addserver & konek ke irc.

> **Catatan**: mungkin kita akan melihat user orok dan user pocong
> di irc dengan host yang sama. tapi kita jangan beranggapan itu
> adalah orang yang sama. apalagi kalo identd *nyala* atau di
> aktifkan. akan jelas terlihat bahwa "siapa yang running psybnc?"
