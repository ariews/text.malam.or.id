--- 
date: "2014-11-07T02:17:00+07:00"
title: "Jadi static HTML"
tags:
    - Pelican
    - Static HTML
---

Oh hai!! udah sangat-sangat lama gak pernah ngisi blog lagi. Buat para
pembaca, yang kebanyakan adalah robot :D, maapin yaa.. lo-lo semua suka,
dan banyak nemuin error 404. Berarti ntar gw kudu re-link untuk
tiap-tiap postingan. Tunggu aja deh tanggal mainnya.

Sabar ya Google Webmaster, gpp sekarang lo report-report banyak error di
web ini, tapi ntar ga ada lagi kok *ngareeeppp*.

Banyak alesannya kenapa jadi jarang nulis, dan keknya gak perlu ditulis
juga sih di sini :)

Jadi, kenapa pindah ke static HTML? yahh karena salah satunya udah
jarang banget update, dan kemungkinan kedepannya juga gitu, terus gw
sekarang cuman punya Low VPS, pastinya RAM jadi makin minim. Yahhh
karena udah jarang banget update, daripada dipake buat mysql+php+nginx,
kenapa ga jadi nginx+html aja?

Lalu dipilihlah Pelican, lumayan sederhana sih, gampang pula installnya.

Okeyy, gak banyak cerita yg bakal ditulis hari ini, thanks dah balik
lagi ke sini, meski cuman ngeindex halaman ini aja.

Sampe ketemu di postingan selanjutnya ^^
