---
date: "2004-12-15T02:16:00+07:00"
title: "tcl"
---

Ngomong2 soal scramble.. gw jadi rada aneh juga :) kenapa sih skr orang
banyak2 maen scramble atau family 100?.. gw liat di beberapa cenel gede
(#bandung, #indonesia,#surabaya) semua pake scramble. Kalo soal ripper..
antara yakin sama gak.. semua na juga jago ripperna :P

Yang parah di bandung.. itu bener2 deh.. tcl bisa sampe ke bandung.. ck
ck ck.. KLIATAN GAK BERMODAL TAU GAK SIH LO! katanya @bandung kreatif,
katanya @bandung kren, katanya @bandung bermodal... TAPI MALES...

Keren apa kalo pake tcl oang laen? kreatif apaan kalo tcl orang laen?
bermodal.. hum.. iye kali ya.. demi buat sebuah tcl.. heuahuea di
bayarin :P

sample code:

```tcl
proc MonthTopTen {nick uhost hand chan args} {
  global AlreadyGoing Question AnyonePlaying WonBefore WonTimes HintAlreadyGiven Hint Answer stoppedflag Score filelength QNumber AlreadyGuessed
  global actch
  if {$chan != $actch} {return}
  putserv "PRIVMSG $nick :Top 10 Scores :"
  set Winners ""
  set f [open "MonthlyScores.txt" r]
  for { set s 0 } { $s < 10 } { incr s } {
    gets $f Tally
    set Placement [expr $s +1]
    if {$Placement == 1} {
      set Placement " 1st Place: "
      set HeadHoncho [lindex $Tally 0]
    }
    if {$Placement == 2} {set Placement " 2nd Place: "}
    if {$Placement == 3} {set Placement " 3rd Place: "}
    if {$Placement == 4} {set Placement " 4th Place: "}
    if {$Placement == 5} {set Placement " 5th Place: "}
    if {$Placement == 6} {set Placement " 6th Place: "}
    if {$Placement == 7} {set Placement " 7th Place: "}
    if {$Placement == 8} {set Placement " 8th Place: "}
    if {$Placement == 9} {set Placement " 9th Place: "}
    if {$Placement == 10} {set Placement " 10th Place: "}
    append Winners $Placement
    append Winners [lindex $Tally 0]
    append Winners " with "
    append Winners [lindex $Tally 1]
    if {[lindex $Tally 1] > 1} {
      append Winners " wins! "
    } else {
      append Winners " win! "
    }
  }
  putserv "PRIVMSG $nick :$Winners"
  putserv "PRIVMSG $nick :Selamat untuk $HeadHoncho !"
  close $f
  return
}
```
