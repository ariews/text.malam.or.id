---
title: "Download eggdrop dari shell"
date: "2006-03-29T07:28:00+07:00"
tags:
    - Eggdrop
---

'Ada banyak jalan menuju roma' itu kata pepatah, begitu juga dengan
eggdrop, ada banyak cara untuk download eggdrop dalam shell *nix*.

Misalnya saja dengan [wget](http://www.gnu.org/software/wget/wget.html),
softaew yg cukup terkenal. Sepertinya menjadi pilihan no. 1 untuk
kebanyakan linuxer :)

Tapi sebelumnya mungkin akan disebutkan beberapa source eggdrop yang
siap di download.

-   Dari ftp eggheads sendiri.
    [ftp://ftp.eggheads.org/pub/eggdrop](ftp://ftp.eggheads.org/pub/eggdrop/source).
    Di sana banyak tersedia eggdrop dari berbagai versi (lama sampe
    baru)
-   Dari CVS server, adalah versi eggdrop paling fresh di setiap
    versinya. Dari sini, kita tidak menggunakan wget atau program
    download lainnya, tapi menggunakan software yang namanya
    [cvs](http://www.nongnu.org/cvs/). Biasanya cvs sudah terinstall.
-   Selain dari CVS, kita pun masih bisa mendownload eggdrop versi
    terakhir (release stable) dari eggheads melalui
    <http://geteggdrop.com>
-   Dan ada beberapa source lainnya, seperti
    [johoho](http://johoho.eggheads.org) yang menyediakan versi
    [eggdrop-precompiled](http://johoho.eggheads.org/eggdrop/index.shtml).

Ok!, skr kita mulai dengan `wget`, mudahnya kita ketik: wget
shource_url, misal `wget http://geteggdrop.com` untuk download eggdrop
versi terbaru. Atau bisa bisa juga dengan option `-O <output>`,
misalnya: `wget http://geteggdrop.com -O egg-baru.tgz`.

Kalo dengan [lynx](http://lynx.browser.org/), kita bisa menggunakan
`lynx -source source_url > output`, misalnya
`lynx -source http://geteggdrop.com > egg-baru.tgz`.

Lalu gmn cara download dengan cvs? gak susah koq, dengan catatan cvs nya
sudah terinstall pada system.

`cvs -d :pserver:anoncvs:anoncvs@cvs.eggheads.org:/usr/local/cvsroot -z9 co eggdrop1.x`

x disini adalah kode eggdrop.

Sebenernya ada banyak cara loh, mungkin ada yg apke
[perl](http://www.perl.com/)? atau pake [php](http://www.php.net).

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
