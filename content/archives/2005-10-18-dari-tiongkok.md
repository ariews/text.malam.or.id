---
title: "Dari Tiongkok"
date: "2005-10-18T18:08:00+07:00"
tags:
  - Humor
---

![Sapi](/images/5029643277919sapi-cartoon.gif)

Dahulu kala di Indonesia ada seorang Cina peranakan yang berasal dari
Propinsi Hokkian, RRC dan ia mempunyai seorang anak laki-laki bernama Ta
Niu.

Ta Niu adalah seorang yang sangat berbakat, cerdas, rajin dan memiliki
fisik yang sangat kuat. Oleh karena itulah dia diberi nama Ta Niu (yang
dalam bahasa Mandarin artinya adalah Sapi Besar).

Sayangnya, Ta Niu mempunyai satu sifat jelek yaitu dia sangat malas.
Ketika sudah tiba usianya untuk menikah, dia sama sekali tidak berusaha
untuk mencari calon istri. Akhirnya ayahnya memutuskan untuk
membantunya. Dia meletakkan sebuah papan di depan rumahnya dan
mengumumkan bahwa anaknya mencari calon istri. Dicantumkannya juga
segala sifat baik dan bakat yang dimiliki Ta Niu dengan tulisan yang
mencolok.

Setelah sekian lama tidak ada seorangpun yang merespon pengumumannya
itu. Setelah mencoba beberapa kali akhirnya ayah Ta Niu memutuskan untuk
mencari calon istri untuk anaknya dikampung leluhurnya yaitu di Hokkian.
Di sana dia juga memasang papan nama yang serupa. Akan tetapi kali ini
juga tidak ada yang meresponnnya. Ini berlangsung hingga beberapa
minggu.

Akhirnya ayahnya menyimpulkan bahwa mungkin karena dia hanya
mencantumkan segala kebaikan yang dimiliki Ta Niu sehingga orang lain
kurang mempercayainya. Akhirnya dia menambahkan sebuah kalimat dalam
bahasa Mandarin yang menjelaskan kejelekan Ta Niu yaitu "TA NIU PI CIAO
LAN" (Yang berarti "Ta Niu agak sedikit malas").

Tak disangka keesokan harinya ratusan gadis berdatangan untuk menemui Ta
Niu sampai-sampai ayah Ta Niu tidak bisa menentukan mana gadis yang
terbaik untuk anaknya.

Dalam keterkejutannya, dia menanyakan kenapa respon mereka begitu bagus
begitu dicantumkan anaknya "sedikit malas". Gadis-gadis ini tampaknya
kurang mengerti pertanyaan ayah Ta Niu. Mereka membaca keras-keras apa
yang tertulis di papan tersebut: "LAN CIAO PI NIU TA" (Tulisan Cina bisa
juga dibaca dari kanan ke kiri).

> **TA NIU PI CIAO LAN** Ta Niu agak sedikit malas, sedangkan **LAN CIAO
> PI NIU TA** Anunya lebih besar dari sapi

<sup>gambar di ambl dari: [strangerinparadise.com](http://www.strangerinparadise.com/Nov-01/Security%20in%20The%20Region.html)
