---
date: "2004-12-30T20:15:00+07:00"
title: "PS (pocongserv)"
tags:
    - Games
---

Sekarang dah mulai masuk versi RC0.2. apa sih yang ada pada versi ini?
dan kenapa?

Hehehhe.. gara2 si ^piE^ bilang kalo program scramble cheat dah di
update sama yang punya nya. Anehnya cuman di berlakukan buat di #allindo
(@nullus.net -- `irc.nullus.net`) aja.

Disini yang gwa ubah cuman 1 aja (dari dulu juga satu), yaitu ngeganti
karakter spasi (space, 032) menjadi karakter normal yang di beri warna
item dan background item.

```tcl
#
# udah banyak program yang ngedetek antara belakang dan depan, sebagai indikator suatu jawaban
# sebuah ide muncul... bagaimana jika kita ilangin space nya?
# hingga program tidak akan membaca space tapi suatu garis lurus yang gak ada space.

set j_zigi_char {
  "a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "o"
  "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z" "" "a " "b "
  "c " "d " "e " "f " "g " "h " "i " "j " "k " "l " "m "
  "o " "p " "q " "r " "s " "t " "u " "v " "w " "x " "y "
  "z " " " " a" " b" " c" " d" " e" " f" " g" " h" " i"
  " j" " k" " l" " m" " o" " p" " q" " r" " s" " t" " u"
  " v" " w" " x" " y" " z" "!" "@" "#" "$" "%" "^" "&"
  "*" "(" ")" "_" "+" "=" "-" "! " "@ " "# " "$ " "% "
  "^ " "& " "* " "( " ") " "_ " "+ " "= " "- " " !" " @"
  " #" " $" " %" " ^" " &" " *" " (" " )" " _" " +" " =" " -"
}

#
# OK, itu kita pake buat nge-FAKE indikator pertanyaan dan jawaban. sekarang kita butuh
# 'sesuatu' buat ngilangin space.

set j_lost_in_space {
  "a" "b" "c" "d" "e" "f" "g" "h" "i" "j" "k" "l" "m" "o" "p" "q" "r" "s" "t" "u" "v" "w" "x" "y" "z" "!" "@" "#" "$" "%" "^" "&" "*" "(" ")" "_" "+" "=" "-"
}

#
# 1.
sc___gamemsg "032,2Hint:034Q030,2uestion 034n030,2o.038 $jz_question_number 032,2Hint:03"

if {$sc___alwaysshowq == 1 || $sc___hintnum == 0} {
  #
  # 2.
  # ok, sekarang kita ganti space dengan karakter.
  set j_gspace "031,1[jz_rand_msgs ${j_lost_in_space}]030,1"
  regsub -all " " ${sc___currentquestion} ${j_gspace} sc___currentquestion_
  sc___gamemsg "038,102H02int:0301[jz_rand_msgs ${j_zigi_char}]0300$sc___currentquestion_0301[jz_rand_msgs ${j_zigi_char}]0301]03"
}

#
# 3.
# kita ulangin langkah di atas.
set j_gspace "031,1[jz_rand_msgs ${j_lost_in_space}]030,1"
regsub -all " " ${test_jawabannya} ${j_gspace} test_jawabannya_
sc___gamemsg "038,102W02ord:0301[jz_rand_msgs ${j_zigi_char}]0300$test_jawabannya_0301[jz_rand_msgs ${j_zigi_char}][jz_rand_msgs ${j_zigi_char}]0301]03"
```
