---
title: "autodelete"
date: "2006-05-25T02:40:00+07:00"
tags:
    - Eggdrop
---

Ini cuman percobaan.

```tcl
#!/usr/bin/tclsh
# folder (array)
# tambah / di akhir folder
set myfolder {
  "/home/arie/mp3/"
  "/home/arie/mpeg/"
  "/home/arie/arsip/avi/"
}

# outdate (hari)
set outdated 7

proc chkfile {} {
  set outdated [expr $::outdated * 86400]
  set waktu [clock seconds]
  set delfile 0
  set keepfile 0
  foreach mydir $::myfolder {
    set hasil [glob "$mydir*"]
    foreach files [lsort $hasil] {
      if { [expr $waktu-[file mtime $files]] > $outdated } {
        file delete -force $files
        incr delfile
      } else {
        incr keepfile
      }
    }
  }
}
```

scritp ini buat ngedelete file yang udah lama di upload, dari script di
atas, anggap file yang udah lebih dari 7 hari akan di delet.

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
