--- 
date: "2014-12-02T11:18:00+07:00"
title: "Monyet Galau!"
tags:
    - Nginx
    - Monkey
---

Baru kemaren gw posting soal si monyet, ya.. si monyet!

Dia butuh memory yang lebih kecil dibanding nginx, cuma ~1Mb. Gw pikir
gw gak bakal butuh nginx lagi, VPS gw kan yg low memory, jadi keknya
udah cukup pake monyet. Udah setting autoblogged juga, ya udah enak lah,
udah ngepas.

Tapi ternyata kadang ada kasus dimana si Pelican harus diset clear
directory.

```dotenv
DELETE_OUTPUT_DIRECTORY = True
```

Ini bakal ngakibatin semua files sama semua directories bakalan dihapus
dulu, sebelum static filenya dibuat. Dan ini termasuk file cgi yg gw
bikin buat autoblogged.

Kalo aja si monyet ini support aliasing (atau emang udah support ya?
cuman gw aja yg belom tau), baru deh nyaman.

Strukture folder gw kek gini:

```text
/public
  |- cgi-bin     <-- tempat narok script buat autoblogged
  |- index.html
  |- ...
  |- generated.file
```

Dengan option `DELETE_OUTPUT_DIRECTORY` maka folder
`cgi-bin` bakalan ikut ke hapus, hal ini bisa kita cegah kalo aja si
monyet support folder aliasing.

Hm.. atau gw downgrade version nya aja kali ya? di versi sebelomnya ada
tuh aliasing, kek gini:

```text
# Server_ScriptAlias :
# ==================================
# If you which to have CGI support (Common Gateway Interface), you should
# define the directory where the executive scripts will be found,for that,
# you should define an alias directory towards the original of the
# following form:
#
#       Server_ScriptAlias /name_of_alias/ original_path
#

Server_ScriptAlias /cgi-bin/ /usr/share/monkey/cgi-bin
```

Atau... gw balik lagi pake nginx?
