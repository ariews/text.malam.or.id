--- 
date: "2017-05-18T09:22:16+07:00"
title: "Nambahin description ke postingan"
tags:
    - Hexo
---

Setelah berhasil [deploy ke git pake plugin]({{< ref path="2017-05-17-deploying-hexo-to-github.md">}}) dan udah bisa diliat
[hasilnya](http://arie.malam.or.id/).

Cuman kok pas liat _html source_, kok authornya orang laennn.

![scshoot](/images/html-source.png)

Setelah ditelusuri, ternyata dari [template](https://github.com/probberechts/cactus-dark) 
yang dipake [manggil](https://github.com/probberechts/cactus-dark/blob/master/layout/_partial/head.ejs#L7) `open_graph` _helper_. Emang sih, gw gak set site description di `_config.yml`, tapi teteuppp ntah kenapa kok yg muncul nama itu.

Ya sudah lah ya, sekarang gimana caranya biar nama itu gak muncul. Tambahin meta description **di setiap post**.

Karena terlalu banyak file yang kudu diupdate, akhirnya bikin script kecil, karena gak ngerti `nodejs`, scriptnya pake php.

Pertama kumpulkan semua files yang mau kita tambahin `description`nya.

```php
$pattern   = '/^.+_(posts|drafts).+\.rst$/i';
$files     = [];
$path      = realpath('/path/to/source/dir');

$directory = new \RecursiveDirectoryIterator($path);
$iterator  = new \RecursiveIteratorIterator($directory);
$regex     = new \RegexIterator($iterator, $pattern, \RecursiveRegexIterator::GET_MATCH);

foreach ($regex as $file) $files[] = $file[0];
```

Kenapa `$file[0]`? gak tau :D, mungkin hasil dari `preg_match($pattern, $path)`

```text
>>> preg_match($pattern, '/path/to/source/dir/_drafts/2004-12-04-open-only-if-you-have-time-for-god.rst', $file);
=> 1
>>> $file
=> [
     "/path/to/source/dir/_drafts/2004-12-04-open-only-if-you-have-time-for-god.rst",
     "drafts",
   ]
```

Nah, mungkin yg direturn itu adalah hasil dari `preg_match` tadi.

Setelah semua filesnya udah kumpul, tinggal update satu per satu.

Ambil meta (paling atas)

```php
$content = file_get_contents($file);

$add_description = function(& $content) {
    /*
     * hanya tambah description kalo blom ada
     * samain aja descriptionnya dengan title
     */
    if (! preg_match('!^description:!mi', $content)) {
        if (preg_match("!^(title: (?<title>.+))$!mi", $entry, $match)) {
            $entry .= "\ndescription: {$match['title']}";
        }
    }
};

if (preg_match('!^(?<fl>-{2,})$\n(?<c>.*)\n^(?<sl>-{2,})$!sim', $content, $match)) {
    $match     = ['fl' => $match['fl'], 'c' => $match['c'], 'sl' => $match['sl']];
    $original  = join("\n", $match);

    $add_description($match['c']);

    $new_meta  = join("\n", $match);
    $content   = str_replace($original, $new_meta, $content);

    file_put_contents($file, $content);
}
```

nah kira-kira seperti [itu](https://gist.github.com/ariews/1ff5e5b212853b6d2da6d12e12eb423c).
