---
title: "Tahun-Tahun Penting Perkembangan Virus"
date: "2003-11-22T12:40:00+07:00"
---

Sampai saat ini ada puluhan ribu virus yang menyebar pada sistem-sistem
komputer di seluruh dunia. Jumlahnya diperkirakan mencapai lebih dari
**70.000**.

Sebenarnya, dari mana virus-virus tersebut bermuasal, mengapa mereka
menyebar dan sampai kapan mereka menghentikan kegilaannya ? Banyak
pertanyaan yang harus dijawab. Disarikan dari PCWorld.com, berikut
adalah sejumlah potongan kisah virus yang selama ini menghantui dunia
komputer.

**1982**. "Elk Cloner", diyakini sebagai virus pertama yang muncul.
Menyebar melalui floppy-floppy komputer Apple II dan menampilkan pesan
pada layar yang berbunyi: It will get on all your disks It will
infiltrate your chips Yes it's Cloner!

**1983**. Seorang lulusan USC, Fred Cohen, adalah orang pertama yang
menggunakan istilah virus untuk menggambarkan program komputer perusak
dan dapat menyebar sendiri.

**1986**. Brain, virus pertama yang hinggap pada PC IBM. Muncul di
floppy berukuran 360KB. File teks yang ditemukan di dalamnya berisikan
nama dan alamat si pembuat virus. Mereka adalah warga Pakistan
bersaudara, Basit dan Amjad Farooq Alvi. Karya mereka tidak menimbulkan
kerusakan berarti. Sebagai penjual software, mereka menganggap tindakan
tersebut bukanlah hal berbahaya. Mereka lakukannya untuk mengukur
tingkat pembajakan software di negara mereka.

**1987**. Sebuah virus percobaan lolos dari sebuah laboratorium komputer
di Israel. Dikenal sebagai Jerusalem, virus ini menyerang setiap hari
Jumat yang jatuh pada tanggal 13 dan menghapus program-program yang
beroperasi pada hari itu.

**1988**. Seorang lulusan Robert Morris, Jr., Cornell, membuat worm
pertama yang menyebar via internet. Worm ini mematikan 6000 komputer
bersistem operasi Unix dan menyebabkan kerugian material sebesar US$10
juta sampai US$100 juta.

**1990**. Muncul virus pertama karya pembuat virus asal Bulgaria "Dark
Avenger". Bulgaria juga merupakan kawasan dimana buletin elektronik
untuk para penulis virus muncul pertama kali. Buletin ini digunakan
sebagai media pertukaran kode program virus. Eropa bagian timur kemudian
menjadi kawasan subur tumbuhnya program-program pengganggu.

Ada **300** virus yang diketahui sampai tahun ini.

**1991**. Tequila, adalah virus polymorphic (memiliki banyak bentuk)
pertama. Menyebar melalui hardisk yang dipakai beramai-ramai. Virus
Polymorphic dapat merubah bentuknya untuk menipu software anti-virus.
Seiring dengan berakhirnya tahun, lusinan virus polymorphic terdeteksi.

**1992**. Michelangelo menjadi virus pertama yang berhasil menarik
perhatian media masa. Terdeteksi pertama kali pada 6 Maret (tanggal
kelahiran sang seniman yang dijadikan nama virus) dan menghinggapi
hardisk komputer para korban. Michelangelo berhasil menginfeksi
kurang-lebih 5.000 sampai 10.000 komputer -- jauh lebih sedikit dari
yang diperkirakan.

**1994**. Ancaman yang muncul lewat e-mail, pertama kali disebarkan
virus Good Time di seluruh internet, merupakan virus pertama yang
menjadi cikal bakal dari sekian banyak virus sejenis yang kemudian
muncul.

**1995-1997**. Concept, virus yang menyerang macro pada Microsoft Word.
Ini merupakan virus pertama yang dapat menginfeksi komputer bersistem
operasi Windows dan Macintosh.

Hingga tahun 1997, jumlah virus yang terdeteksi mencapai 10.000 lebih.

**1998**. Kelompok hacker, Cult of the Dead Cow, mengeluarkan Back
Orifice, seperangkat tool yang digunakan untuk membuat program-program
Trojan horse yang memungkinkan para hacker menginfeksi PC-PC yang tak
terproteksi untuk kemudian melakukan kontrol dari jarak jauh.

**1999**. Tahun ini adalah tahun kemunculan Melissa. Ini adalah virus
pertama yang memanfaatkan kumpulan alamat yang terdapat pada komputer
korban. Alamat-alamat tersebut digunakan untuk mengirimkan dirinya
melalui e-mail ke user-user lain. Virus ini menyebar ke seluruh dunia
hanya dalam hitungan jam.

**2000**. Tahun ini dibanjiri serangan berupa denial-of-service yang
melumpuhkan Amazon.com, CNN, Yahoo dan situs-situs besar lainnya hanya
dalam beberapa hari.

Virus LoveLetter (surat cinta) menyebar ke jutaan komputer di malam
hari. Mencuri user name dan password dari komputer korban.

**2001**. Virus Anna Kournikova muncul dalam bentuk lampiran
(attachment) pada e-mail. Pengirimnya mengiming-imingi korban dengan
foto sang atlet tenis asal Rusia tersebut. Para analis menilai virus ini
sebagai virus pertama yang sukses dibuat oleh seorang "script kiddie",
istilah yang diberikan kepada seseorang yang menulis virus dengan
tool-tool yang di-download di internet.

**2002**. Worm Klez, muncul pertama kali. Hinggap di sejumlah server
e-mail dan melumpuhkan software-software anti virus.

**2003**. Tahun ini dijuluki sebagai "Tahunnya Virus". Serangan yang
terus-menerus dan memangsa sistem-sistem komputer di seluruh dunia
secara silih berganti. Slammer, Blaster dan Sobig. Melumpuhkan sejumlah
server e-mail dan menimbulkan kerugian material sampai jutaan dolar
akibat kehilangan produktivitas. Para analis sepakat: kita belum pernah
melihat yang seperti ini sebelumnya.
