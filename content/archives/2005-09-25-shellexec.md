---
title: "shellexec"
date: "2005-09-25T10:05:00+07:00"
---

nyobain [tcl](http://www.tcl.tk) script buat
[eggdrop](http://eggheads.org). gw tau banyak error, dan ini juga simple
banget. gak ada batasan.

```tcl
bind dcc n shellexec dcc:shellexec
proc dcc:shellexec {host idx arg} {
    if {$arg == ""} { return 0 }
    set shellcmd [lindex [split $arg] 0]
    set cmdopt [lrange $arg 1 end]
    putlog "#$host# $shellcmd $cmdopt"
    set cmdresult [exec $shellcmd $cmdopt]
    putdcc $idx "$cmdresult"
}
```
