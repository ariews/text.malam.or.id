--- 
date: "2013-03-18T15:58:00+07:00"
title: "Test Varnish+NginX+PHP-FPM"
tags:
    - Kerjaan
    - Nginx
    - Varnish
    - PHP-FPM
---

Setelah sekian lama berbengong-bengong dengan banyak artikel tentang
[varnish](https://www.varnish-cache.org/).

Hasilnya masih gak terlalu jauh sih dengan
[NginX](http://nginx.org/)+[PHP-FPM](http://php-fpm.org/), mungkin
karena ukuran webyang masih kecil kali ya, jadi gak terlalu signifikan.
