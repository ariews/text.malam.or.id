--- 
date: "2010-11-09T07:05:00+07:00"
title: "Blog update"
tags:
    - Kohana
    - Kohana 3.2
---

Setelah beberapa hari ini gw migrasi blog ini dari Kohana 2 ke Kohana 3
(tapi masih blom beres semua). Hari ini gw nambahin
[feed](http://arie.malam.or.id/feed/posts.atom) dalam bentuk
[atom](http://en.wikipedia.org/wiki/Atom_(standard)) 1.0, sampe
[valid](http://feedvalidator.org/check.cgi?url=http%3A//arie.malam.or.id/feed/posts.atom)
:). Biar lebih mudah, gw pake module
[Kohana_XML](https://github.com/ccazette/Kohana_XML). Sample untuk pake
Kohana_XML ini ada di halaman
[Wiki](https://github.com/ccazette/Kohana_XML/wiki)nya.

Pas baru pindahan kemaren, gw masih disable komentar, sekarang udah
bisa. Tapi sementara ga pake Honeypot dulu, gw coba pake [TypePad
Antispam](http://antispam.typepad.com/) dulu aja.

Ada banyak yg mau gw tulis, soal mudahnya pake Kohana Jelly dibaningkan
dengan default ORM, tapi besok aja, soalnya skr udah rada ngantuk.

Oh ya, saat ini source code blog ini ada di
[Bitbucket](http://bitbucket.org/), kalo ntar blognya beres, gw mau
publish, skr sih masih malu... kodenya berantakan.
