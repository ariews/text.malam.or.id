---
title: "dl dari eggdrop"
date: "2006-09-12T14:30:00+07:00"
tags:
    - Eggdrop
---

set getchan "#namachannel"

```tcl
set savedir "save_folder"
bind pubm p "$getchan !DL *" usewget
proc usewget {n u h c r} {
  set r [lindex $r 1]
  puthelp "PRIVMSG $c: $r"
  set res "Download [lindex [split $r /] end] "
  if {[catch {[exec wget --output-document=$::savedir/[lindex [split $r /] end] $r]}]} {
    append res " 02OK! 02"
  } else {
    append res " 02ERROR! 02"
  }
  puthelp "PRIVMSG $c :$res"
}
```

buat download pake eggdrop

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
