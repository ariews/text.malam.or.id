---
title: "The Piper at the Gates of Dawn"
date: "2005-09-25T00:05:00+07:00"
tags:
 -  Lyrics
---

7 Agustus 1967. cover ada [di web
sebelah.](http://images.google.com/images?q=%22The%20Piper%20at%20the%20Gates%20of%20Dawn%22)

## Astronomy Domine

Lime and limpid green, a second scene  
A fight between the blue you once knew.  
Floating down, the sound resounds  
Around the icy waters underground.  
Jupiter and Saturn, Oberon, Miranda  
And Titania, Neptune, Titan.  
Stars can frighten.

Blinding signs flap,  
Flicker, flicker, flicker blam. Pow, pow.  
Stairway scare Dan Dare who's there?  
Lime and limpid green  
The sounds surrounds the icy waters underground  
Lime and limpid green  
The sounds surrounds the icy waters underground.

## Lucifer Sam

Lucifer Sam, siam cat.  
Always sitting by your side  
Always by your side.  
That cat's something I can't explain.

Ginger, ginger you're a witch.  
You're the left side  
He's the right side.  
Oh, no!  
That cat's something I can't explain.

Lucifer go to sea.  
Be a hip cat  
Be a ship's cat.  
Somewhere, anywhere.  
That cat's something I can't explain.

At night prowling sifting sand.  
Hiding around on the ground.  
He'll be found when you're around.  
That cat's something I can't explain.

## Matilda Mother

There was a king who ruled the land.  
His majesty was in command.  
With silver eyes the scarlet eagle  
Showers silver on the people.  
Oh Mother, tell me more.

Why'd'ya have to leave me there  
Hanging in my infant air  
Waiting?  
You only have to read the lines  
They're scribbly black and everything shines.

Across the stream with wooden shoes  
With bells to tell the king the news  
A thousand misty riders climb up  
Higher once upon a time.

Wandering and dreaming  
The words have different meaning.  
Yes they did.

For all the time spent in that room  
The doll's house, darkness, old perfume  
And fairy stories held me high on  
Clouds of sunlight floating by.  
Oh Mother, tell me more  
Tell me more.  
Aaaaaaaah  
Aaaaaaaah  
Aaaaaaaah

## Pow R. Toc H

instrument

## Flaming

Alone in the clouds all blue  
Lying on an eiderdown.  
Yippee! You can't see me  
But I can you.

Lazing in the foggy dew  
Sitting on a unicorn.  
No fair, you can't hear me  
But I can you.

Watching buttercups cup the light  
Sleeping on a dandelion.  
Too much, I won't touch you  
But then I might.

Screaming through the starlit sky  
Traveling by telephone.  
Hey ho, here we go  
Ever so high.

Alone in the clouds all blue  
Lying on an eiderdown.  
Yippee! You can't see me  
But I can you.

## Take Up Thy Stethoscope and Walk

Doctor doctor!  
I'm in bed  
Achin' head  
Gold is lead  
Choke on bread  
Underfed  
Gold is lead  
Jesus bled  
Pain is red  
Are goon  
Grow go  
Greasy spoon  
You swoon  
June bloom

Music seems to help the pain  
Seems to cultivate the brain.  
Doctor kindly tell your wife that  
I'm alive - flowers thrive - realize - realize  
Realize.

## Interstellar Overdrive

instrument

## The Gnome

I want to tell you a story  
About a little man  
If I can.  
A gnome named Grimble Crumble.  
And little gnomes stay in their homes.  
Eating, sleeping, drinking their wine.

He wore a scarlet tunic,  
A blue green hood,  
It looked quite good.  
He had a big adventure  
Amidst the grass  
Fresh air at last.  
Wining, dining, biding his time.  
And then one day - hooray!  
Another way for gnomes to say  
Oooooooooomray.

Look at the sky, look at the river  
Isn't it good?  
Look at the sky, look at the river  
Isn't it good?  
Winding, finding places to go.  
And then one day - hooray!  
Another way for gnomes to say  
Oooooooooomray.  
Ooooooooooooooomray.

## Chapter 24

A movement is accomplished in six stages  
And the seventh brings return.  
The seven is the number of the young light  
It forms when darkness is increased by one.  
Change returns success  
Going and coming without error.  
Action brings good fortune.  
Sunset.

The time is with the month of winter solstice  
When the change is due to come.  
Thunder in the other course of heaven.  
Things cannot be destroyed once and for all.  
Change returns success  
Going and coming without error.  
Action brings good fortune.  
Sunset, sunrise.

A movement is accomplished in six stages  
And the seventh brings return.  
The seven is the number of the young light  
It forms when darkness is increased by one.  
Change returns success  
Going and coming without error.  
Action brings good fortune.  
Sunset, sunrise.

## The Scarecrow

The black and green scarecrow as everyone knows  
Stood with a bird on his hat and straw everywhere.  
He didn't care.  
He stood in a field where barley grows.

His head did no thinking  
His arms didn't move except when the wind cut up  
Rough and mice ran around on the ground  
He stood in a field where barley grows.

The black and green scarecrow is sadder than me  
But now he's resigned to his fate  
'Cause life's not unkind - he doesn't mind.  
He stood in a field where barley grows.

Bike

I've got a bike, you can ride it if you like.  
It's got a basket, a bell that rings  
And things to make it look good.  
I'd give it to you if I could, but I borrowed it.

You're the kind of girl that fits in with my world.  
I'll give you anything, everything if you want things.

I've got a cloak it's a bit of a joke.  
There's a tear up the front. It's red and black.  
I've had it for months.  
If you think it could look good, then I guess it should.

You're the kind of girl that fits in with my world.  
I'll give you anything, everything if you want things.

I know a mouse, and he hasn't got a house.  
I don't know why I call him Gerald.  
He's getting rather old, but he's a good mouse.

You're the kind of girl that fits in with my world.  
I'll give you anything, everything if you want things.

I've got a clan of gingerbread men.  
Here a man, there a man, lots of gingerbread men.  
Take a couple if you wish. They're on the dish.

You're the kind of girl that fits in with my world.  
I'll give you anything, everything if you want things.

I know a room full of musical tunes.  
Some rhyme, some ching, most of them are clockwork.  
Let's go into the other room and make them work.
