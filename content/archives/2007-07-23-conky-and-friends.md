---
title: "Conky and friends"
date: "2007-07-23T21:30:00+07:00"
tags:
    - Myself
    - Tools
    - Tutorial
---

Sapa dia? gak tau...loh?

Yg gw omongin bukan nama orang, tapi sebuah aplikasi yang running di kompi gw 
skr. [conky](<http://conky.sf.net>) yang ada di [screenshoot postingan 
sebelomnya](<http://www.malam.or.id/blog/1780.20.html>). banyak ngebantu kita 
tau beberapa informasi yang ada tentang kompi kita.

Cuma itu? gak juga, conky support inputan dari aplikasi laennya.

Misalnya, gw pengen masukin judul lagi mp3 yang lagi gw pasang pake 
[Rhythmbox](<http://www.gnome.org/projects/rhythmbox/>), banyak beberapa cara, 
tapi gw pake yang simple aja.

pertama, gw bikin script sederhana di file (sebut aja `rhytmstatus`), gw save 
dalam `$PATH` gw biar bisa dengan mudah kita akses.

```sh
#!/bin/sh`

TEXT=rhythmbox-client --no-start --print-playing 

if [ "${TEXT}" != "" ]; then 
    TEXT=${TEXT} 
else 
    TEXT='Not Active!' 
fi 
echo "${TEXT}"
```

jangan lupa `chmod +x rhytmstatus`. balik ke `.conkyrc` tambahin di `TEXT` seperti ini:

`Rhythmbox: ${execi 5 rhytmstatus}`

![Conky](/images/5029644451a33070723_2851_1280x1024.jpg)
