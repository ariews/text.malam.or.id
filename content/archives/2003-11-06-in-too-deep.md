---
title: "In Too Deep"
date: "2003-11-06T03:06:00+07:00"
tags:
    - Lyrics
---

![image0](http://img51.exs.cx/img51/3232/it2.jpg)

All that time i was searching, nowhere to run to, it started me
Thinking, Wondering what i could make of my life, and who’d be waiting,
Asking all kinds of questions, to myself, but never finding the answers,
Crying at the top of my voice, and no one listening, All this time, i
still remember everything you said

There’s so much you promised, how could i ever forget.

Listen, you know i love you, but i just can’t take this, You know i love
you, but i’m playing for keeps, Although i need you, i’m not gonna make
this,

You know i want to, but i’m in too deep.

So listen, listen to me, Ooh you must believe me, I can feel your eyes
go thru me,

But i don’t know why.

Ooh i know you’re going, but i can’t believe It’s the way that you’re
leaving, It’s like we never knew each other at all, it may be my fault,
I gave you too many reasons, being alone, when i didn’t want to I
thought you’d always be there, i almost believed you, All this time, i
still remember everything you said, oh

There’s so much you promised, how could i ever forget.

Listen, you know i love you, but i just can’t take this, You know i love
you, but i’m playing for keeps, Although i need you, i’m not gonna make
this,

You know i want to, but i’m in too deep.

So listen, listen to me,

I can feel your eyes go thru me

It seems i’ve spent too long Only thinking about myself - oh Now i want
to spend my life

Just caring bout somebody else.

Listen, you know i love you, but i just can’t take this, You know i love
you, but i’m playing for keeps, Although i need you, i’m not gonna make
this,

You know i want to, but i’m in too deep.

You know i love you, but i just can’t take this, You know i love you,
but i’m playing for keeps, Although i need you, i’m not gonna make this,

You know i want to, but i’m in too deep…

– Genesis - Invisible Touch - 1986 –
