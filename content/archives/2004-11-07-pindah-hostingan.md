---
title: "Pindah Hostingan"
date: "2004-11-07T13:03:00+07:00"
---

setelah beberapa minggu ngehosting di
[100webspace](http://www.100webspace.com), akhirnya
[blog](http://www.spunge.org/~ezie/jz) di pindahin lagi ke yang
[lama](http://www.spunge.org/~ezie/)

Sebenernya di 100webspace bagus, beneran! cuman kadang kaang sql gak
jalan, padahal bener2 suka. di sana dah di sediain banyak
[fasilitas](http://100webspace.com/freeplan.html),
[configurasi](http://www.somse.info/conf.php) phpnya pas banget buat gw.

Tapi ya itu tadi.. kadang suka error, kadang bisa di akses, kadang error
500 (script error), kadang mysql na jeblok. tapi itu gak ngurangin nilai
100webspace koq, dia tetep aja free. lagian gw juga ngerti deh kalo
error, namanya juga free services, cuma-cuma.

Jadi makin komplit aja deh:

1.  pertama pake [Spunge](http://www.spunge.org),
    [Blogger](http://www.blogger.com), pake
    [HaloScan](http://www.haloscan.com).
2.  tus ke [Yahoo](http://webhosting.yahoo.com) sama masih pake Blogger,
    pake [StatCounter](http://www.statcounter.com) buat statistik,
    HaloScan dah di tinggalin soalna [dah bisa pake
    comentar](http://www.blogger.com/knowledge/2004/05/great-blogger-relaunch.pyra)
    Blogger na, kalo
    [trackback](http://en.wikipedia.org/wiki/Trackback)? blom ngerti
    makenya, jadi ya sekalian di tinggalin aja.
3.  tus pake [wp](http://wordpress.org) (masih yahoo), database:
    [mysql](http://www.mysql.com)
4.  tus pindah ke 100webspace, wp, mysql
5.  mulai muncul masalah koneksi pada [mysql](http://www.mysql.com),
    pake deh [code kecil2an](#mini-kode), database di double di
    db.host.sk sama di freeserverhost.net.

yah.. karena dua-duanya free services (db.host.sk & freeserverhost.net)
kadang error juga :(, tapi buat ngurangin rasa kekecewaan, hostingnya
aku balikin ke Spunge, karena cepet banget!

**UPDATE**

Setelah di daftarkan dengan dengan domain baru:
[malam.or.id](http://malam.or.id) di [tadulako](http://tadulako.co.id),
blog ini mulai2 rada asik di baca, gmn gak? selain
[wp](http://wordpress.org) baru juga ada beberapa kemajuan yang bikin gw
demen. Salah satunya adalah pengertian tentang
[CSS](http://www.w3.org/Style/CSS/). Selain itu, ada beberapa member
baru disini yang mulai ngeramein blog, salah satunya adalah
[banyubening](/author/banyubening/).

Beberapa waktu lalu, malam.or.id ini sering susah di akses webnya, hal
ini udah gw konfirmasikan sama pihak hosting, dan emang jawabannya
memuaskan. Tapi gw kadang ngerasa aneh, kalo pake proxy bisa, gak pake
proxy malah gak bisa. Atau sebaliknya. hal ini juga pernah di alamin
sama [jezz](http://itzjuztme.blogspot.com), dia akses web malam.or.id
kagak bisa2, tapi begitu pake proxy baru bisa. Keknya ada masalah. Ya
udah.. gw balikin lagi ke [spunge](http://www.spunge.org) aja, meski
[masalah](http://ezie.spunge.org/350/2004/12/13/beuh/) timbul lagi,
yaitu di batesinnya koneksi [mysql](http://www.mysql.com) ke
[freesql.org](http://freesql.org).

Pikir2.. kenapa gak sekalian aja mysql na taro di malam.or.id, tinggal
adduser, terus kasih `%` buat allowed host. Setelah beberapa kali di
coba, baru deh bisa connect dari spunge ke malam.or.id.

**UPDATE**

Sekarang dah [pindah database
server](http://ezie.spunge.org/2005/03/18/hup-hup/1542).

**UPDATE**: [links](http://www.malam.or.id/blog/1635,2005,06,09/)
