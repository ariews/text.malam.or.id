---
title: "search engine"
date: "2004-11-12T06:16:00+07:00"
---

![MSN Search Beta](http://img105.exs.cx/img105/7491/msn-beta-fp.gif)

[microsoft](http://www.microsoft.com) dengan [msn](http://www.msn.com/)
nya ngetest2 mesin [pencari baru](http://beta.search.msn.com/), cuman
kliatannya gak beda jauh sama yang lama..

berikut hasil dari beberapa search engine yang gede2 dengan search
pattern **host**:

[Alltheweb.com](http://www.alltheweb.com)

![Alltheweb.com](http://img3.exs.cx/img3/5531/alltheweb.gif)

[Altavista.com](http://www.altavista.com)

![Altavista.com](http://img3.exs.cx/img3/1743/altavista.gif)

[Askjeeves.com](http://www.ask.com)

![Askjeeves.com](http://img105.exs.cx/img105/9963/askjeeves.gif)

[MSN Search Beta](http://beta.search.msn.com)

![MSN Beta](http://img105.exs.cx/img105/6299/beta-msn.gif)

[Google.com](http://www.google.com)

![Google.com](http://img105.exs.cx/img105/5702/google.gif)

[Hotbot.com](http://www.hotbot.com)

![HotBot](http://img105.exs.cx/img105/781/hotbot.gif)

[MSN Search](http://search.msn.com)

![MSN](http://img105.exs.cx/img105/2843/msn.gif)

[Yahoo.com](http://search.yahoo.com)

![Yahoo Search](http://img28.exs.cx/img28/7262/search-yahoo.gif)

banyak [komentar](http://slashdot.org/article.pl?sid=04/11/11/1724221)
soal MSN Search Beta ini
