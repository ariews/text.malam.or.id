--- 
date: "2015-02-10T17:15:01+07:00"
title: "Migrasi ke October CMS"
tags:
    - Kerjaan
    - OctoberCMS
    - Laravel
---

![OctoberCMS Logo](/images/octobercms-logo.png)

Tahun baru udah lewat, gak kerasa udah februari lagi, jadi keknya udah
basi kalo ngucapin selamat tahun baru ya :D

Belakangan ini, pekerjaan gw akan (sedang) dipindah ke CMS baru yg
namanya [OctoberCMS](http://octobercms.com/), banyak hal yg bisa
dipelajari di CMS baru ini.

Perbedaannya mencolok banget kalo dibandingkan dengan Kohana, OK untuk
yg ini gw ketinggalan banget.

Ternyata OctoberCMS ini menggunakan dari [Laravel](http://laravel.com/).

Udah gitu ajah :P
