---
date: "2004-12-28T12:20:00+07:00"
title: "Fwd Something Nice To Read"
tags:
    - Friends
    - Email
---

Tuhan yang Mahabaik memberi kita ikan,

tetapi kita harus mengail untuk mendapatkannya.

Demikian juga Jika kamu terus menunggu waktu yang tepat,

mungkin kamu tidak akan pernah mulai.

Mulailah sekarang...

mulailah di mana kamu berada sekarang dengan apa adanya.

Jangan pernah pikirkan kenapa kita memilih seseorang untuk dicintai,

tapi sadarilah bahwa cintalah yang memilih kita untuk mencintainya.

Perkawinan memang memiliki banyak kesusahan,

tetapi kehidupan lajang tidak memiliki kesenangan.

Buka mata kamu lebar-lebar sebelum menikah,

dan biarkan mata kamu setengah terpejam sesudahnya.

Menikahi wanita atau pria karena kecantikannya atau ketampanannya

sama seperti membeli rumah karena lapisan catnya.

Harta milik yang paling berharga bagi seorang pria di dunia ini adalah
..

hati seorang wanita.

Begitu juga Persahabatan, persahabatan adalah 1 jiwa dalam 2 raga

Persahabatan sejati layaknya kesehatan,

nilainya baru kita sadari setelah kita kehilanganNya.

Seorang sahabat adalah yang dapat mendengarkan lagu didalam hatiMu

dan akan menyanyikan kembali tatkala kau lupa akan bait-baitnya.

Sahabat adalah tangan Tuhan untuk menjaga Kita.

Rasa hormat tidak selalu membawa kepada persahabatan, tapi Jangan pernah
menyesal untuk bertemu dengan orang lain...

tapi menyesal-lah jika orang itu menyesal bertemu dengan kamu.

Bertemanlah dengan orang yang suka membela kebenaran.

Dialah hiasan dikala kamu senang dan perisai diwaktu kamu susah.

Namun kamu tidak akan pernah memiliki seorang teman,

jika kamu mengharapkan seseorang tanpa kesalahan.

Karena semua manusia itu baik kalau kamu bisa melihat kebaikannya dan
menyenangkan kalau kamu bisa melihat keunikannya tapi semua manusia itu
akan buruk dan membosankan

kalau kamu tidak bisa melihat keduanya.

Begitu juga Kebijakan, Kebijakan itu seperti cairan, kegunaannya
terletak pada penerapan yang benar, orang pintar bisa gagal karena ia
memikirkan terlalu banyak hal,

sedangkan orang bodoh sering kali berhasil dengan melakukan tindakan
tepat.

Dan Kebijakan sejati tidak datang dari pikiran kita saja,

tetapi juga berdasarkan pada perasaan dan fakta.

Tak seorang pun sempurna.

Mereka yang mau belajar dari kesalahan adalah bijak.

Menyedihkan melihat orang berkeras bahwa mereka benar meskipun terbukti
salah.

Apa yang berada di belakang kita dan apa yang berada di depan kita

adalah perkara kecil berbanding dengan apa yang berada di dalam kita.

Kamu tak bisa mengubah masa lalu....

tetapi dapat menghancurkan masa kini dengan mengkhawatirkan masa depan.

Bila Kamu mengisi hati kamu ..

dengan penyesalan untuk masa lalu dan kekhawatiran untuk masa depan,

Kamu tak memiliki hari ini untuk kamu syukuri.

Jika kamu berpikir tentang hari kemarin tanpa rasa penyesalan dan hari
esok tanpa rasa takut,

berarti kamu sudah berada dijalan yang benar menuju sukses.

Thanks & Salam
