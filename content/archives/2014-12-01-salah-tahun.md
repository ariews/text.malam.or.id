--- 
date: "2014-12-01T19:03:00+07:00"
title: "Salah tahun"
tags:
    - Pelican
    - reStructuredText
---

Setelah pindah pake Pelican, semua data kan gw ubah ke dalam bentuk file
biasa dengan format reStructuredText. Terus kan gw ada bikin 4 buah
postingan baru (selama bulan November).

Ternyata semuanya salah tahun! hehehe.. gak tau kenapa, yg harusnya 2014
kok jadi 2015. Baru gw ubah tadi :)

Gw belum biasa ternyata nulis dalam bentuk kek ginian, karena kebiasaan
pake CRUD, yg tanggalnya itu otomatis, sampe salah isiin tanggal.

Oh ya, selamat datang Desember, bentar lagi tahun baru nih, ambil cuti
yg lama, beres2 rumah (mau pindahan akhir Desember ini), dan bersantai
:)
