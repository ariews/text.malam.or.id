--- 
date: "2013-04-25T13:23:00+07:00"
title: "Ganti format ke Markdown"
tags:
    - Markdown
---

Yep! sekarang blog inih pake format markdown lagi :)

Sebelumnya pake markdown juga, cumann.. pas lagi ngoprek-ngoprek dan
nemu [redactor](http://redactorjs.com) gw reformat lagi ke dalam bentuk
HTML.

Makanya banyak postingan lama yang ada (banyak!) keterangan diupdate.

Setelah direformat lagi kedalam markdown, semua postingan jadi-terupdate
:D

Hahaha, dan postingan ini gw tulis manual dari mysql client, dengan
peritntah: `INSERT ...`.

Selain pake markdown lagih, juga ada pergantian pemain dari MySQL
menjadi [MariaDB](https://mariadb.org/).

Babay MySQL, tengkiew yah udah nemenin gw :)
