---
title: "Saat saat yang indah"
date: "2005-04-10T18:59:00+07:00"
---

Ochie send imel ke gw... keknya imel dari orang laen :)

![image001.gif](http://x3.freeserverhost.net/seepicts.php?img=/image001.gif)

Terkadang ada saat-saat dalam hidup, ketika engkau merindukan seseorang
begitu dalam, hingga engkau ingin mengambilnya dari angan-anganmu, lalu
memeluknya erat-erat!

![image003.gif](http://x3.freeserverhost.net/seepicts.php?img=/image003.gif)

Ketika pintu kebahagiaan tertutup, ada pintu yang lain yang terbuka;
tetapi, seringkali kita memandang terlalu lama pada pintu yang tertutup
hingga kita tidak melihat pintu yang lain, yang telah terbuka bagi kita.

![image004.gif](http://x3.freeserverhost.net/seepicts.php?img=/image004.gif)

Jangan percaya pada penglihatan; penglihatan dapat menipu.

Jangan percaya pada kekayaan; kekayaan dapat sirna.

Percayalah pada Dia yang dapat membuatmu tersenyum, sebab hanya
senyumlah yang dibutuhkan untuk mengubah hari gelap menjadi terang.

Carilah Dia, yang dapat membuat hatimu tersenyum.

![image005.gif](http://x3.freeserverhost.net/seepicts.php?img=/image005.gif)

Angankan apa yang engkau ingin angankan; pergilah kemana engkau ingin
pergi; jadilah seperti yang engkau kehendaki, sebab hidup hanya satu
kali dan engkau hanya memiliki satu kali kesempatan untuk melakukan
segala hal yang engkau (Mungkin) ingin lakukan.

Semoga engkau punya cukup kebahagiaan untuk membuatmu terus tersenyum,
cukup pencobaan untuk membuatmu kuat, cukup penderitaan untuk tetap
menjadikanmu manusiawi, dan cukup pengharapan untuk menjadikanmu
bahagia.

![image006.gif](http://x3.freeserverhost.net/seepicts.php?img=/image006.gif)

Mereka yang paling berbahagia tidaklah harus memiliki yang terbaik dari
segala sesuatunya; mereka hanya mengoptimalkan segala sesuatu yang
datang dalam perjalanan hidup mereka.

Masa depan yang paling gemilang akan selalu dapat diraih dengan
melupakan masa lalu yang kelabu; engkau tidak akan dapat maju dalam
hidup hingga engkau melepaskan segala kegagalan dan sakit hatimu.
dibelakang

![image007.gif](http://x3.freeserverhost.net/seepicts.php?img=/image007.gif)

Ketika engkau dilahirkan, engkau menangis sementara semua orang di
sekelilingmu tersenyum. Jalani hidupmu sedemikian rupa, hingga pada
akhirnya engkaulah satu-satunya yang tersenyum sementara semua orang di
sekelilingmu menangis.

Kirimkan pesan ini kepada mereka yang berarti bagimu (dan aku baru saja
melakukannya); kepada mereka yang telah menyentuh hidupmu dengan sesuatu
yang indah atau lain cara; kepada mereka yang membuatmu terus tersenyum
ketika engkau sungguh membutuhkannya; kepada mereka yang membuatmu
melihat sisi baik dari segala hal ketika engkau terjatuh; kepada mereka
yang persahabatannya engkau hargai; kepada mereka yang begitu berarti
dalam hidupmu.

![image008.gif](http://x3.freeserverhost.net/seepicts.php?img=/image008.gif)

Jika engkau tidak mengirimkannya, janganlah khawatir, tak ada hal buruk
yang akan menimpamu; hanya saja engkau kehilangan satu kesempatan untuk
menyemarakkan hari seseorang dengan pesan ini...

Jangan hitung tahun-tahun yang telah terlewatkan, hitunglah saat-saat
yang indah Hidup tidak diukur dengan berapa banyaknya napas yang kita
hirup;

Melainkan dengan saat-saat di mana kita menarik napas bahagia!
