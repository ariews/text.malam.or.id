---
date: 2017-05-17T03:34:43Z
tags:
    - Hexo
    - Github
title: "Deploying Hexo to Github"
---
Setelah menulis, biasanya kita melakukan generate html dengan `hexo generate`, lalu hasilnya kita push ke static repo, di kasus saya adalah github.

Prosesnya kira-kira seperti ini:

```text
$ hexo new post "deploying hexo to github"
INFO  Created: ~/path/to/source/_posts/deploying-hexo-to-github.md
```

Terusss... beres nulis, dan `generate` kita `push` deh.

```text
$ cd public
$ git add . && git push
```

Oh ya, folder output punya gw ini adalah `submodule` ke static repo, jadi bisa melakukan `git` di sana.

Nah beres, kita berhasil posting output hasil _generate_ ke Github.

Cara lainnya adalah pake plugin [hexo-deployer-git](https://github.com/hexojs/hexo-deployer-git).

Ngikutin apa yg ada di README:

```text
$ npm install \
    git+git@github.com:hexojs/hexo-deployer-git.git \
    --save
```

Lalu kita aktifkan pluginnya di `_config.yml`

```yaml
plugins:
  hexo-deployer-git
```

Jangan lupa, _update_ `_config.yml`-nya ngikutin yg ada dipetunjuk.

```yaml
deploy:
  type: git
  repo: <repository url>
  branch: [branch]
```

Misalnya untuk `log channel`:

```yaml
deploy:
  type: git
  repo: git@github.com:username/pages.github.io.git
  branch: master
```

Lalu, kita clone static repo kita di `.deploy_git`

```text
$ git clone \
    git@github.com:username/pages.github.io.git \
    .deploy_git
```

Sip... sekarang, setiap kita _generate_ udah bisa langsung otomatis deploy ke github.

```text
$ hexo generate -d # generate dan langsung deploy
```

Nah, selamat mencoba ya!!
