---
date: 2014-11-11T14:33:00Z
title: "Auto Blogged"
tags:
    - Static HTML
    - Pelican
    - Git
---
Setelah seting sana-sini di VPS, akhirnya gw pasang git hook.
Sederhananya ketika kita push content baru, atau edit, ke repositori (gw
pake Bitbucket), bakalan ngetrigger hook yang ditentukan.

Postingan ini merupakann test, apakah hook tersebut berhasil atau tidak.

**Update (2019-11-12T14:51:19+06:30)**

Lebih pas disebut auto deploy kali yeee
