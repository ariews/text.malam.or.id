---
title: "Yahoo dan AntiSpam"
date: "2004-11-17T15:41:00+07:00"
---

![Yahoo! mail](http://img62.exs.cx/img62/1829/yahoo-mail.gif)

[Yahoo](http://www.yahoo.com) ngebuat [antispam
baru](http://antispam.yahoo.com/domainkeys), kalo di liat cara kerjanya
sih gampang aja.

Email yang di kirim akan di buatkan digital-signature-nya dan di simpan
untuk keperluan vrifikasi pada penerimaan email itu.

[Open Source-na](http://domainkeys.sourceforge.net/) udah [di terapkan
pada Sendmail. inc](http://sendmail.net/dk-milter/)
