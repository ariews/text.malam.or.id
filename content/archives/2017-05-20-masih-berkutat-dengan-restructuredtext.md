--- 
date: "2017-05-20T16:15:12+07:00"
title: "Masih berkutat dengan reStructuredText di Hexo"
tags:
    - Hexo
    - reStructuredText
---

Masih berkaitan dengan postingan yg lalu, soal [munculnya nama
lain]({{< ref path="2017-05-18-nambahin-description-ke-postingan.md">}}) sebagai author
blog, ternyata itu bukan karena description, tapi lebih ke
[plugin](https://www.npmjs.com/package/hexo-renderer-restructuredtext)
yang dipake.

Plugin ini pake [rst2html](http://docutils.sourceforge.net/rst.html)
buat ngerender reStructuredText ke HTML.

Setiap kita panggil si `rst2html`, hasilnya selalu full `HTML`, dari
mulai `<!DOCTYPE...`, `<head>`, sampe `<html>`, jadi komplit banget,
malahan termasuk `stylesheet`-nya juga ada.

Nah, dibagian stylesheetnya ini yg ada nama David itu.

```html
<style type="text/css">
/*
:Author: David Goodger
...
*/
</style>
```

Padahal kita bisa ngilangin semua itu, termasuk stylesheetnya, karena
kan kita gak perlu, yang kita mau cuma tulisan dalam format HTML.

Caranya adalah dengan menambahkan option `--template=my-template.txt`,
kenapa? karena isi dari default _template_ nya seperti ini:

```text
%(head_prefix)s
%(head)s
%(stylesheet)s
%(body_prefix)s
%(body_pre_docinfo)s
%(docinfo)s
%(body)s
%(body_suffix)s
```

Kita ga perlu yang lain-lain lagi, cuman perlu `%(body)s` aja.

Sekarang balik lagi ke plugin yg dipake, kita tambah option tadi ke
file `index.js`

```javascript
// original:
    var args = ['--math-output="MathJax"',
    '--smart-quotes="yes"', '--traceback'];
```

Ubah menjadi:

```javascript
// mod:
    var args = ['--math-output="MathJax"',
    '--smart-quotes="yes"', '--traceback',
    '--template=/path/to/my-template.txt'];
```

Seperti disebut di atas, isi dari `/path/to/my-template.txt` hanya
`%(body)s` aja.

Setelah itu, kita coba _regenerate_

```text
$ hexo clean
$ hexo generate
```

Hasilnya jadi lebih bersih, coba deh.. ;)
