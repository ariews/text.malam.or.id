---
title: "Obscured By Clouds"
date: "2005-09-18T08:58:00+07:00"
tags:
  - Lyrics
---

Lirik Lagu dari Album Obscured By Cloud (1972). Pink Floyd!

Cover... ada di
[Google](<http://images.google.co.id/images?svnum=10&hl=id&lr=&client=firefox&rls=org.mozilla%3Aen-US%3Aunofficial&q=%22wot%27s+uh+the+deal%22&btnG=Cari>)!

## BURNING BRIDGES

Bridges burning gladly, melting with the shadow Flickering between the
lines Stolen mormons floating softly on the air

Gone on wings of fire and climbing higher

Ancient bulbs are breaking, leaving all and changing sides Dreaming of a
new day, cast aside the other way Magic vision, staring, kindled by the
burning flames

Lies in her eyes

The door it stands a jar It was, it once was high beyond the guilded
cage Beyond the reach of time, the moment is at hand

She breaks the golden band

## CHILDHOOD'S END

You shout in your sleep That's the price, it's just too steep Is your
conscience at rest It was put to the test You awake with a start Just
the beating of your heart Just one man beneath the sky

Just two ears, just two eyes

You said, sail across the sea Long past thoughts and memories
Childhood's end: your fantasies Merging harsh realities And then as the
sail is hoist You'll find your eyes are growing moist And all the fears
never voiced

Say you have to make the final choice

Who are you and who am I To say we know the reason why Some are born,
some men die Beneath one infinite sky There'll be war and there'll be
peace But anything one day will cease All the iron turn to rust All the
proud men turn to dust So all things time will mend

So this song will end

## FREE FOUR

The memories of a man in his old age Are the deeds of a man in his
prime. You shuffle in gloom of the sickroom

And talk to yourself as you die.

Life is a short, warm moment And death is a long cold rest. You get your
chance to try in the twinkling of an eye:

Eighty years, with luck, or even less.

So all aboard for the American tour, And maybe you'll make it to the
top. And mind how you go, and I can tell you, 'cause I know

You may find it hard to get off.

You are the angel of death And I am the dead man's son. And he was
buried like a mole in a fox hole.

And everyone is still in the run.

And who is the master of fox hounds? And who says the hunt has begun?
And who calls the tune in the courtroom?

And who beats the funeral drum?

The memories of a man in his old age Are the deeds of a man in his
prime. You shuffle in gloom in the sickroom

And talk to yourself till you die.

## STAY

Stay and help me to end the day And if you don't mind, we'll break a
bottle of wine Stick around and maybe we'll get one down

'cause I want to find what lies behind those eyes

Midnight blue, burning gold

A yellow moon is growing cold

I rise, looking through my morning eyes Surpised to find you by my side
Light my brain, don't try to remember your name

To find the words to tell you goodbye

Morning dews, newborn day Midnight blue turned to grey Midnight blue,
burning gold

A yellow moon is growing cold.

## THE GOLD IT'S IN THE...

Come on my friend, let's make for the hills They say there's gold and
I'm looking for thrills You can get your hands on whatever we find

'cause I've only come along for the ride

Well you go your way and I'll go mine I don't care if we get there on
time Everybody's searching for something, they say

I'll get my kicks on the way

Over the mountains, across the sea Who knows what may be waiting for me
I could sail forever to strange sounding names

Faces of people and places don't change

All I have to do is just close my eyes To see the seagulls wheeling in
the far distant skies All I wanna tell you, all I wanna say

Is count me in on the journey, don't expect me to stay

## WOT'S... UH THE DEAL

Heaven sent the promised land  
Looks alright from where I stand  
'cause I'm the man on the outside looking in  
Waiting on the first step  
Show me where the key is kept  
Point me down the right line 'cause it's time

To let me in from the cold, turn my lead into gold  
'cause there's a chill wind blowing in my soul  
And I think I'm growing old

Flash... the red is... wot's... uh deal  
Got to make it to the next meal  
Try to keep up with the turning of the wheel  
Mile after mile, stone after stone  
You turn to speak but you're alone  
Million miles from home you're on your own

Fly kite by candle light  
With her by my side  
Wish she prefers we never stir again  
Someone sent the promised land  
Well I grabbed it with both hands  
Now I'm the man on the inside looking out

Hear me shout, come on in  
What's the news, where you've been  
'cause there's no wind left in my soul and I've grown old
