--- 
date: "2009-04-15T23:49:00+07:00"
title: "Wish You Were Here"
tags:
    - Lyrics
---

So, so you think you can tell Heaven from Hell, Blue skys from pain. Can
you tell a green field From a cold steel rail? A smile from a veil?

Do you think you can tell?

And did they get you to trade Your heroes for ghosts? Hot ashes for
trees? Hot air for a cool breeze? Cold comfort for change? And did you
exchange A walk on part in the war

For a lead role in a cage?

How I wish, how I wish you were here. We're just two lost souls
Swimming in a fish bowl, Year after year, Running over the same old
ground. What have we found? The same old fears.

Wish you were here.

[Pink Floyd, Wish You Were Here](http://www.google.com/search?q=Pink+Floyd+Wish+You+Were+Here+lyrics)
