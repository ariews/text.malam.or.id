---
title: "more wortel & lilypie!"
date: "2005-05-28T23:00:00+07:00"
tags:
  - Friends
  - Archie
---

Kemaren sore gw sempet ngerasa pusing banget, pala nyut2an, ngantuk, dan
punggung gw terasa panas, gak tau karena apa. Seabis jaga net gw maen di
no 3, gak seperti biasanya, sore ini gw gak mandi, keknya badan terasa
lemes dan mo sakit ... lagi :(

Maen di no 3 tadinya cuman mo chat doang sama ochie, tapi nyatanya malah
sempet tidur depan kompi. gw minta maaf sama ochie soalna jadi lama pas
jawab. Tapi lo tau? ochie bukannya marah karena "gw cuekin" tapi dia
malah nyuruh gw tidur: "papana istirahat aja ya...", mwahh.

Gw kebangun jam 10an, haus, badan panas dan pala masih nyut2an, waktu
liat hp terlihat ada tanda sms masuk, wah... sampe gak tau kalo ochie
ada sms gw sekitar jam 9an. Balik lagi ke kamar, maunya bobok lagi, tapi
gak bisa tidur, "rie... ada dodi tuh di no 6" si
[joko](http://www.jaringcrew.com) bilang dari billing.

Wah ngobrol dulu deh sama dia, Oh ya dodi ini dah tua, dia dah punya
anak anak sini \[baca: ciumbuleuit\] asli tp skr ikut istrinya di tasik.
Kita ngobrol2 sana sini, maklum lama gak ketemu, tapi obrolannya koq
lambat laun makin pudar, gak konsen ngobrol... lama2 jadi pada diem. gmn
gak? dodi ini ngobrol di depan komputer sambil buka situs bokep: "nah
ini rie yang kemaren di protes masyarakat, orang indo yang ikutan miss
univers", "oh ya?" gw gak tau ya apa bener apa gak, bener gitu? dan gw
juga duduk di depan kompi, gak mau maen tadinya tapi gak ada tempat
duduk laen selain itu. Lama2 karena nunggu dodi ngomong, gw sambil buka
konqueror, tus buka blog.

![love u ochie](/images/5029641802649200492722216.gif) Masih penasaran
sama [postingan ochie kemaren](http://ezie.spunge.org/1619,2005,05,21/),
skr gw nyari2 di [technorati](http://www.technorati.com/) dan nemu [link
laennya](http://www.yusufbottiegaos.com/index.php?p=105), ah keknya gw
makin gak yakin kalo itu **BUKAN** hasil karya dari [alfin
baby](http://profiles.yahoo.com/alfin_baby) dan juga sudah pasti
**BUKAN** hasil karya dari ochie, sorry
[wandi](http://samuderacinta.blogspot.com/), gw bukan pengen hal ini
lebih jauh, ini cuman sekedar rasa penasaran gw aja. Pengen tau sapa
sebenernya yang nulis pertama kali. Meski mungkin gw gak akan tau siapa
dia.

Lain itu, dari [blognya](http://www.yusufbottiegaos.com/) gw blom
kenal: hai kenalan dong :) ada link yang menurut gw lucu banget, very
funny! di bagian bawah blognya ada semacam meteran :P bayi! (apa sih
namanya?), namanya [lilypie](http://lilypie.com/). Gwa jadi inget, kalo
gw pernah dan sering bilang kalo gw pengen punya beby dari ochie.

love u ochie.
