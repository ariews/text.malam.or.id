---
date: "2004-12-04T08:34:00+07:00"
title: "THE TWO WOLVES"
tags:
    - Email
---

dr pito:

One evening an old Cherokee told his grandson about a battle that was
going on inside himself.

He said, "My son, it is between 2 wolves. One is evil: Anger, envy,
sorrow, regret, greed, arrogance, self-pity, guilt, resentment,
inferiority, lies, false pride, superiority and ego... The other is
good: Joy, peace, love, hope, serenity, humility, kindness, benevolence,
empathy, generosity, truth, compassion and faith..."

The grandson thought about it for a minute and then asked his
grandfather, "Which wolf wins?"

The old Cherokee simply replied, "The one I feed."

_Author Unknown_
