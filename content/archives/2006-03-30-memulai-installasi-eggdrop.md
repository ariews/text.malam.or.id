---
title: "memulai installasi eggdrop"
date: "2006-03-30T07:41:00+07:00"
tags:
    - Eggdrop
---

Setelah di download tentu saja kita install, masa kita delete lagi :P

Cara installnya pun termasuk mudah.

1.  decompress dulu file source dengan perintah:
    `tar -zxf file-eggdrop.tar.gz` <sup>1</sup>
2.  lalu masuk ke folder yang di hasilkan, misalnya `cd eggdropx.x`
    <sup>2</sup>
3.  kita configure dengan mengetikan perintah: `./configure`
    <sup>3</sup>
4.  lalu kita mulai compile, tik: `make config` <sup>4</sup>
5.  setelah itu, `make` <sup>5</sup>
6.  dan yang terakhir `make install` <sup>6</sup>

Ada beberapa keterangan dari setiap point, yaitu sebagai berikut:

1.  bisa juga dengan: `gzip -dc file-eggdrop.tar.gz | tar xvf -`
2.  `x.x` disini berarti versi, misal `1.6.7` atau `1.4.4`.
3.  secara default, eggdrop akan terinstall di $HOME/eggdrop, nah agar
    kita bisa menginstall ke dir yg lain, kita tambahkan perintah
    berikut pada configurenya, `--prefix=/path/baru`.
4.  di beberapa server, kita tidak di ijinkan menggunakan `make`,
    sebagai gantinya kita gunakan `gmake`
5.  mudahnya, kita memang tinggal ketik: `make` saja, tapi ada beberapa
    option yang bisa di gunakan, yaitu: sdebug, debug dan static.
6.  jika pada point 3 kita tidak memberikan option `--prefix=`, kita
    masih bisa menginstall eggdrop di direktori yg kita inginkan dengan
    menambahkan option berikut: `DEST=/path/baru`
7.  sebagai tambahan, untuk configurasi lebih lanjut kita bisa melihat
    dengan option help pada configure: `./configure --help`.

Setelah proses installasi selesai, kita akan di minta untuk memedit file
`eggdrop.conf`, edit file tersebut sesuai dengan kebutuhan
masing<sup>2</sup>, dan eggdrop siap di jalankan.

Link:

1.  [eggheads](http://eggheads.org/)
2.  [Gzip](http://www.gzip.org/)
3.  [GNU make](http://www.gnu.org/software/make/make.html)
4.  [TAR](http://www.gnu.org/software/tar/)

*[note: di-import dari [http://bot.to.md](http://bot.to.md/)]*
