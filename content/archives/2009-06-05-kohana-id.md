--- 
date: "2009-06-05T03:37:00+07:00"
title: "kohana-id"
tags:
    - Kohana
    - Milis
---

Untuk menambah pengetahuan soal php, selain membaca artikel2 tentang
php, gw juga ada ikut beberapa mailing list (milis), seperti id-php,
phpug, dan yang lainnya.

Baru2 ini gw ikutan
[kohana-id](http://groups.google.com/group/kohana-id) berharap dapat
pengetahuan banyak khususnya soal kohana, dan php umumnya.

Sayangnya, masih sedikit membernya, dan aktifitasnya masih low. Berbeda
dengan
[codeigniter-id](http://tech.groups.yahoo.com/group/codeigniter-id/).
