---
title: "link eggdrop"
date: "2005-05-20T16:31:00+07:00"
tags:
  - Friends
  - Tutorial
---

temen gw nih si tomz minta di ajarin ngelink bot (eggdrop), dulu sih dah
pernah gw ajarin, tapi katanya catatannya ilang.

> 16:18:54 &lt;@tomz&gt; rie 16:19:02 &lt;@tomz&gt; catetan notepad cara
> link bot kemaren ilang 16:19:05 &lt;@tomz&gt; wekekekeke
>
> 16:19:09 &lt;@tomz&gt; dudul

payah.. koq bisa bisa nya ilang :evil:

Anggap kita punya 2 bot, sebut aja BOT1 sama BOT2. BOT1 kita jadikan
hub.

Cara mudah:

di BOT1\*

```text
.+bot BOT2 IP-BOT2:PORT-BOT2
```

di BOT2\*

```text
.+bot BOT1 IP-BOT1:PORT-BOT1
.botattr BOT1 +h
.link BOT1
```

**note: *) dalam ctcp chat**
