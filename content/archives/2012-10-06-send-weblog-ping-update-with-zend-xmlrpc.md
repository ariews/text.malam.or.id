--- 
date: "2012-10-06T00:30:00+07:00"
title: "Send weblog ping update with Zend XmlRpc"
tags: 
    - Weblog
    - Zend Framework
    - XmlRpc
---

![image0](/images/506f18bec52adping-blog-services.jpg)

Di [postingan
sebelumnya](http://arie.malam.or.id/posts/read/168/ping.php), gw ada
tulis beberapa ping server, nah kali ini, kebetulan gw ada project yang
mesti ngasih update ke mereka. Hanya saja kalo dulu gw masih pake
Wordpress, sekarang kagak.

Yahh daripada gw mesti nulis lagi library buat ngeping, kenapa ga pake
yang udah ada aja... bener gak?

Library yang gw pilih adalah componen dari Zend Framework, yaitu XmlRpc
Client. Cara pakenya gak ribet kok, malah sangat gampang bin mudah.

Misalnya gw mau ngeping untuk update blog gw ini.

Sangat mudah kan :)

Tapi beberapa server mengharuskan kita menyertakan alamat feed kita,
bisa dalam format RSS, Atom atau RDF. Untuk menyertakan alamat feed
kita, maka method yang kita panggil juga beda.

Sepertinya gampang ya? Ya.. gampang sih, kan kita tinggal make component
nya aja, gak perlu ngebikin dari nol.
