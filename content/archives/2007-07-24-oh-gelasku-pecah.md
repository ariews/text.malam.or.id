---
title: "Oh gelasku pecah"
date: "2007-07-24T19:54:00+07:00"
tags:
    - Myself
    - Archie
---

Sebuah gelas kaca yang indah, akan terasa lebih indah jika kita beri cahaya yang berkilauan. Dan cahaya matahari akan memberikannya, indah dan hangat. Sangat memikau!

Keindahan lain adalah ketika kita memandang dunia ini dari atas, dari atas gedung tingkat 100 misanya. Kita bisa malayangkan pandangan sejauh-jauhnya.

Dari dua keindahan itu, gw bermaksud menggabungkan keduanya. Gelas yang berkilauan dan pemandangan yang sangai indah.

Akan tetapi, ada hal2 lain yang tidak terduga, gelas itu pecah dan jatuh dari ketinggian.

![Gelas pecah](/images/5029644557fbebroken_glass.gif)

Itulah keadaan gw saat ini.

SELAMAT JALAN OCHIE CINTAKU
