---
title: "MediaWiki"
date: "2005-07-08T15:29:00+07:00"
---

[MediaWiki](http://wikipedia.sourceforge.net/) Move Template Cross-Site
Scripting Vulnerability.

**DESCRIPTION:**

A vulnerability has been reported in MediaWiki, which can be exploited
by malicious people to conduct cross-site scripting attacks.

Input passed to an unspecified parameter in the page move template isn't
properly sanitised before being returned to the user. This can be
exploited to execute arbitrary HTML and script code in a user's browser
session in context of an affected site.

The vulnerability has been reported in version 1.4beta6 through 1.4.5
and versions prior to 1.5beta3 for the 1.5.x releases.

**SOLUTION:**

Update to version 1.4.6.

<http://sourceforge.net/project/showfiles.php?group_id=34373>

via: [secunia](http://secunia.com)
