---
title: "Backup Template!"
date: "2004-11-26T13:27:00+07:00"
---

Backup template [wp](http://wordpress.org) lo.. soalna udah ada
[wordpress
v1.3-alpha-5](http://wordpress.org/nightly/wordpress-2004-11-23.zip).

kalo lo mo update, ikuti langkah berikut: 1. sebelum update, BACKUP
database dan file2 penting laininya (termasuk template), kalo perlu
sepolder wp di backup.

1.  [download](http://wordpress.org/download/) wp baru. cara ini bisa
    dengan 2 cara.
2.  kalo lo ada [CVS](https://www.cvshome.org/) client, pake itu, lebih
    cepet, gampang koq [caranya](#cvs).
3.  kalo gak ada.. download aja dari “[nightly
    builds](http://wordpress.org/nightly/)”.
4.  upload.
5.  lalu buka script update nya, ada di:
    `http://domain-kamu.com/folder-blog/wp-admin/upgrade.php`
6.  ikuti petungjuknya di sana.

CVS melalui cvs

`cvs -d:pserver:anonymous@cvs.sourceforge.net:/cvsroot/cafelog -z3 co wordpress`
