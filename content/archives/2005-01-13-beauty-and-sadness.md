---
title: "Beauty and Sadness"
date: "2005-01-13T10:22:00+07:00"
---

***Yasunari Kawabata*** dalam novelnya yang berjudul *Utsukushisa To
Kanashimi To (Beauty and Sadness)* melukiskan cinta sebagai sesuatu yang
indah, yang bersemi dalam hati, yang membuat segalanya (sekeliling kita)
jadi indah pula.Cinta membuat orang menjadi penyayang, murah hati,
tersenyum2, gembira, kangen yang gak ada hentinya, dan kadang juga
cemburu.

Dibalik keindahan dan kesempurnaan penjelasan tadi, Kawabata juga
menceritakan tentang sisi lain dari cinta. Adalah benci dan dendam, jika
cinta di kecewakan. Cinta yang indah akhirnya menyebabkan kesedihan.

Gw jadi inget sama quit msg nya si bang riki tempo hari, *jangan
berlebihan mencintai wanita, karena cinta itu akan menjadi rasa benci*,
ya kira2 seperti itu deh.
