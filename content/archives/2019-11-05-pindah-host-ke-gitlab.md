---
date: 2019-11-05T01:03:06Z
tags:
    - Pindah Hosting
    - Gitlab
title: "Pindah ke Gitlab"

---
Hei, maaf ya, blognya lagi berantakan, lagi coba beres-beres.

## Dari Github ke Gitlab.

Udah lama juga jarang pake Github, dari dulu sebelum dibeli sama Microsoft, hal ini dikarenakan private repo, akhirnya pilih Bitbucket. Tapi di Bitbucket pun kurang puas, karena beberapa hal, dan pindahlah ke Gitlab.

## Dari Hexo ke Hugo.

Blog ini ya, udah berpindah-pindah dari Wordpress, Drupal, Wordpress, Kohana, Pelican , Hexo dan akhirnya sekarang pake Hugo.

## Dari reStructuredText ke Markdown

Sebelumnya, format blog ini berupa reStructuredText, tapi karena jarang banget pake format RST, akhirnya balik lagi ke Markdown.

Jadi ya sekarang mau dipindah satu-persatu kontentnya, mudah-mudahan kekejar ya.

## Blog lama

Update (2019-11-12T06:18:02+06:30): Gak bakalan pindahin semua blog kesini, yang lama masih bisa diakses di [arie.malam.or.id](http://arie.malam.or.id/archives/ "arie.malam.or.id")
