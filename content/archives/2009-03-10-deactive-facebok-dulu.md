--- 
date: "2009-03-10T14:04:00+07:00"
title: "Deactive Facebok dulu"
tags:
    - Facebook
---

Dari Facebook:

> Hi Arie,
>
> You have deactivated your Facebook account. You can reactivate your
> account at any time by logging into Facebook using your old login
> email and password. You will be able to use the site like you used to.

YA, buat sementara, gw deactive dulu, ada hal yg butuh konsentrasi dulu,
daripada cuma mikirin fb (apalagi liat2in status orang).

Ntar kalo dah santai, gw balik lagi deh ;)
